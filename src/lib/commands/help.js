var fs = require("fs");
var db = require("../db.js");
var util = require("../util.js");
var config = util.config;

var cd = require("../commands.js");

var help = module.exports = new Map();

help.function = function(m,ihs){
	// get commands that correspond to the roles you have
	var mod_rids = config.roles["mod"];
	var admin_rids = config.roles["admin"];

	// show everything if admin or mod
	if(util.has_role(m.member, admin_rids) || util.has_role(m.member, mod_rids)){
		var categories = util.generate_category_pages(cd,"admin");
		m.author.send({embed:{
			author: {
				name: "" + ihs.client.user.username + " Commands by kel#3382 🐐",
				icon_url: ihs.client.user.avatarURL()
			},
			title: categories[0][0],
			description: categories[0][1],
			footer: {
				text: "Page 1 of " + String(categories.length),
				icon_url: m.author.avatarURL()
			},
			timestamp : new Date()
		}}).then(function(m){
			m.react("⬅").then(function(){
				m.react("➡").then().catch(console.error);	
			}).catch(console.error);
		}).catch(console.error);
	}
	// show commands that all people can use
	else {
		var command_descriptions = util.generate_command_descriptions(cd, "all");
		var all_commands_string = command_descriptions.all.join("");

		m.author.send({embed:{
			author: {
				name: "" + ihs.client.user.username + " Commands by kel#3382 🐐",
				icon_url: ihs.client.user.avatarURL()
			},
			description: all_commands_string,
			footer: {
				text: m.author.username,
				icon_url: m.author.avatarURL()
			},
			timestamp : new Date()
		}});
	}
}

help.name = "helplfg";
help.arg = "";
help.description = "lists all available commands sorted by category";
help.perms = "all";
help.category = "info";
help.dm = false;
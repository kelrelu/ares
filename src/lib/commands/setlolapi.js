var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var setlolapi = module.exports = {};

setlolapi.function = function(m,ihs){
	var arg = util.parse_arg(m.content, "setlolapi");
	
	m.channel.send(util.cm_embed("Successfully changed League API Key from \n\n**" + config.api.lol + "**\n\t\t\t\t\t\t\t\t\t\tto\n**" + arg[0] + "**.")).then(function(em){
		em.delete({ timeout: config.bot.delete_wait_time_seconds * 1000 });
	});

	m.delete({ timeout: config.bot.delete_wait_time_seconds * 1000 });

	config.api.lol = arg[0];

	util.write_config_to_file(config);
}

setlolapi.name = "setlolapi";
setlolapi.arg = "<generated api key>";
setlolapi.description = "changes lol api while there's no permanent key, message will delete itself to protect key";
setlolapi.perms = "admin";
setlolapi.category = "settings";
setlolapi.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var stats = module.exports = {};

stats.function = function(m,ihs){
    var ranked_players = db.dbquery("select * from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + " order by p.elo desc,p.mmr desc, p.wins desc,p.losses asc,p.ign asc;");
    var ranked_players_igns = ranked_players.map(function(p){return p.ign;});
    var ranked_players_discord_ids = ranked_players.map(function(t){return t.discord_id;});

    var players = db.dbquery("select * from players p where p.wins > 0 or p.losses > 0;");
    var players_igns = players.map(function(t){return t.ign;});
    var players_discord_ids = players.map(function(p){return p.discord_id;});

    var args = util.parse_arg(m.content,"stats");

    if(util.is_user_mention(args[0])){
        var uid = util.get_did(args[0]);
        // if ranked
        if(ranked_players_discord_ids.includes(uid)){
            var player = ranked_players[ranked_players_discord_ids.indexOf(uid)];
            var rank = ranked_players_discord_ids.indexOf(uid) + 1;
            // send message
            var stats_message = "```fix\n   RANK   " + "ELO   MMR   W   L ``````py\n";
            stats_message += " ".repeat(4) + String(rank) + " ".repeat(6-String(rank).length) + String(player.elo) + "  " + String(player.mmr) + "  " + String(player.wins) + " ".repeat(4-String(player.wins).length) + String(player.losses);
            stats_message += " ```\n";

            var awards = "";
            if(rank <= config.ih.max_leaderboard ){ awards += config.ih.emoji_leaderboard; }
            if(db.is_veteran_ign(player.ign)){ awards += config.ih.emoji_vet; }

            var guild = ihs.client.guilds.find("id",config.server.server_id);
            var member = guild.members.find("id",player.discord_id);
            if(member){
                m.channel.send({ embed:{
                    // color:3447003,
                    footer:{
                        text: member.user.username,
                        icon_url:member.user.avatarURL()
                    },
                    timestamp : new Date(),
                    title: "**" + player.ign + " " + awards + "**",
                    description: stats_message
                }}).then().catch(console.error);
            } else {
                m.channel.send({embed:{
                    footer:{
                        text: "(left server)"
                    },
                    timestamp : new Date(),
                    title: "**" + player.ign + " " + awards + "**",
                    description: stats_message
                }}).then().catch(console.error);
            }
        // if unranked
        } else if(players_discord_ids.includes(uid)){
            var player = players[players_discord_ids.indexOf(uid)];
            var stats_message = "```fix\n   RANK   " + "ELO   MMR   W   L ``````py\n";
            stats_message += "   UR     " +  String(player.elo) + "  " + String(player.mmr) + "  " + String(player.wins) + " ".repeat(4-String(player.wins).length) + String(player.losses);
            stats_message += " ```\n";

            var guild = ihs.client.guilds.find("id",config.server.server_id);
            var member = guild.members.find("id",player.discord_id);
            if(member){
                m.channel.send({ embed:{
                    // color:3447003,
                    footer:{
                        text: member.user.username,
                        icon_url: member.user.avatarURL()
                    },
                    timestamp : new Date(),
                    title: "**" + player.ign + "**",
                    description: stats_message
                }}).then().catch(console.error);
            } else {
                m.channel.send({embed:{
                    footer:{
                        text: "(left server)"
                    },
                    timestamp : new Date(),
                    title: "**" + player.ign + "**",
                    description: stats_message
                }}).then().catch(console.error);
            }  
        } else {
            m.channel.send({embed:{
                color: 3447003,
                description: "**" + args[0] + "** does not have any recorded " + config.ih.region.toUpperCase() + " in-house games this season! No stats to report."
            }});
        }
    } else {
        var ign = args[0];

        if(ranked_players_igns.includes(ign.toUpperCase())){
            var player = ranked_players[ranked_players_igns.indexOf(ign.toUpperCase())];
            var rank = ranked_players_igns.indexOf(ign.toUpperCase()) + 1;

            // send message
            var stats_message = "```fix\n   RANK   " + "ELO   MMR   W   L ``````py\n";
            stats_message += " ".repeat(4) + String(rank) + " ".repeat(6-String(rank).length) + String(player.elo) + "  " + String(player.mmr) + "  " + String(player.wins) + " ".repeat(4-String(player.wins).length) + String(player.losses);
            stats_message += " ```\n";

            var awards = "";
            if(rank <= config.ih.max_leaderboard ){ awards += config.ih.emoji_leaderboard; }
            if(db.is_veteran_ign(player.ign)){ awards += config.ih.emoji_vet; }

            var guild = ihs.client.guilds.find("id",config.server.server_id);
            var member = guild.members.find("id",player.discord_id);
            if(member){
                m.channel.send({ embed:{
                    footer: {
                        text: member.user.username,
                        icon_url: member.user.avatarURL()
                    },
                    timestamp: new Date(),
                    title: "**" + player.ign + " " + awards + "**",
                    description: stats_message
                }});
            } else {
                m.channel.send({ embed:{
                    footer: {
                        text: "( left server )",
                    },
                    timestamp: new Date(),
                    title: "**" + player.ign + " " + awards + "**",
                    description: stats_message
                }});
            }

        } else if(players_igns.includes(ign.toUpperCase())){
            
            var player = players[players_igns.indexOf(ign.toUpperCase())];

            var stats_message = "```fix\n   RANK   " + "ELO   MMR   W   L ``````py\n";
            stats_message += "   UR     " +  String(player.elo) + "  " + String(player.mmr) + "  " + String(player.wins) + " ".repeat(4-String(player.wins).length) + String(player.losses);
            stats_message += " ```\n";

            var guild = ihs.client.guilds.find("id",config.server.server_id);
            var member = guild.members.find("id",player.discord_id);
            if(member){
                m.channel.send({ embed:{
                    footer: {
                        text: member.user.username,
                        icon_url: member.user.avatarURL()
                    },
                    timestamp: new Date(),
                    title: "**" + player.ign + "**",
                    description: stats_message
                }});
            } else {
                m.channel.send({ embed:{
                    footer: {
                        text: "( left server )",
                        icon_url: member.user.avatarURL()
                    },
                    timestamp: new Date(),
                    title: "**" + player.ign + "**",
                    description: stats_message
                }});
            }

        } else { 
            m.channel.send({embed:{
                color: 3447003,
                description: "**" + ign + "** has not played a " + config.ih.region.toUpperCase() + " in-house game this season! No stats to report."
            }});
        }
    }
}

stats.name = "stats";
stats.arg = "";
stats.description = "shows stats (elo, mmr, win, loss) for player on ladder";
stats.perms = "all";
stats.category = "info";
stats.dm = true;
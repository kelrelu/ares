var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var quit = module.exports = {};

quit.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_open_lobby();
			if( g!=null ){
				if(g.is_signed_up_user(m.author)){
					var ign = g.players[m.author.id];
					delete g.players[m.author.id];
					g.player_count -=1;
					m.channel.send(util.cm_embed(" Deleted " + m.author + " (**" + ign + "**) from the sign up sheet."));
					g.show_players(m);
				}
			}
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

quit.name = "quit";
quit.arg = "";
quit.description = "removes player from in-house sign up sheet";
quit.perms = "all";
quit.category = "in-house";
quit.dm = false;
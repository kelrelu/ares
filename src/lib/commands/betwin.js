var util = require("../util.js");
var db = require("../db.js");
var config = util.config;
var rt = require("rand-token");

var betwin = module.exports = new Map();
var shorthand = new Map();

shorthand["A1"] = ["Lobby A", 1];
shorthand["A2"] = ["Lobby A", 2];

shorthand["B1"] = ["Lobby B", 1];
shorthand["B2"] = ["Lobby B", 2];

shorthand["C1"] = ["Lobby C", 1];
shorthand["C2"] = ["Lobby C", 2];

shorthand["D1"] = ["Lobby D", 1];
shorthand["D2"] = ["Lobby D", 2];

shorthand["E1"] = ["Lobby E", 1];
shorthand["E2"] = ["Lobby E", 2];

shorthand["F1"] = ["Lobby F", 1];
shorthand["F2"] = ["Lobby F", 2];

// make sure bet minimum is reached
// make sure bet is on active lobby
// bet must be within 10 minutes of draft finish
// you can't retract a bet
// typing .betwin multiple times will updaet your  bet, but only if it is greater than the existing bet
// if you win, you get 5% of what you bet + losing pool/among all people who bet with you
// if you lose, you lose the amount you bet


betwin.function = function(m,ihs){
	var args = util.parse_args(m.content, "betwin");
	console.log(args);
	if(args.length==2){
		if(util.is_integer(args[0])){
			var valid_abbrv = Object.keys(shorthand);
			if(valid_abbrv.includes(args[1].toUpperCase())){
				var amount = args[0];
				var lobbyteam = shorthand[args[1].toUpperCase()];

				// MAKE SURE GAME THEY'RE BETTING ON IS IN SESSION
					// check if they've already bet. if they have increase the bet
					var lobby_bets = db.dbquery("select * from bets where lobby_name='" + lobbyteam[0] + "';");
					var lobby_bets_uids = lobby_bets.map(function(t){return t.uid});
					if(lobby_bets_uids.includes(m.author.id)){
						// update bet

					} else {
					// 	// hasn't bet before, insert it
					// 	var bet_id = rt.suid(config.misc.token_length);
					// 	var game_id = ihs.lobbies[shorthand[args[1].toUpperCase()][0]].id;
					// 	db.dbquery("insert into bets (bet_id,uid,amount,game_id,lobby_name,team) values(" + bet_id + "," + m.author.id + "," + args[0] + "," + game_id + "," + shorthand[args[1].toUpperCase()][0] + "," + shorthand[args[1].toUpperCase()][1] + ");")
					}

				

			} else {m.channel.send(util.redx_embed("Invalid LobbyTeam combination.\n\nValid combinations include:\n " + valid_abbrv.join(", ") + ".\n\n" + "**" + config.bot.prefix + this.name + " " + this.arg + "**"));}
		} else {m.channel.send(util.redx_embed("Invalid amount.\n\n**" + config.bot.prefix + this.name + " " + this.arg + "**"))}
	} else {m.channel.send(util.error_format(this));}

}

betwin.name = "betwin";
betwin.arg = "<amount> <lobbyteam>";
betwin.description = "bets specified amount of flowers on specified inhouse team's winning their next game";
betwin.perms = "all";
betwin.category = "betting";
betwin.dm = false;
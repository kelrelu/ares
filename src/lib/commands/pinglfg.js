var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var pinglfg = module.exports = {};

pinglfg.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_open_lobby();
			if( g!=null ){
				if(g.player_count < config.ih.max_team_size * 2){
					m.channel.send("<@&" + config.roles.ih_player.join("> <@&") + ">, Need " + String((config.ih.max_team_size * 2) - g.player_count) + " more player(s)!");
				} else { m.channel.send(util.redx_embed(" Not pinging! Current lobby is full. ")); }
			}
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

pinglfg.name = "pinglfg";
pinglfg.arg = "";
pinglfg.description = "pings @ in-houses, lists number of players needed";
pinglfg.perms = "mod";
pinglfg.category = "in-house";
pinglfg.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var flush = module.exports = {};

flush.function = function(m,ihs){

    console.log("Current LoL Verification Before Flush")
    console.log(ihs.lol_verification)

    var unverified_uids = Object.keys(ihs.lol_verification).map(function(t){return t});

    var guild = ihs.client.guilds.find("id",config.server.server_id);
    for(var i = 0; i < unverified_uids.length;i++){
        var member = guild.members.find("id,",unverified_uids[i]);
        if(member){
            member.user.send({embed:{
                author: {
                    icon_url: guild.iconURL(),
                    name: "Verification Expired"
                },
                description: "Hi! Your verification has expired (flushed on a daily basis).\nIf you still want to receive a ranked tag, you must renew your verification by typing **" + config.bot.prefix + "verify <ign> <region>** again to receive a new summoner icon." 
            }});
        }
    }

    ihs.lol_verification = null;
    ihs.lol_verification = new Map();
}

flush.name = "flush";
flush.arg = "";
flush.description = "clears lol verification map";
flush.perms = "admin";
flush.category = "secret";
flush.dm = true;
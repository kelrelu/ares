var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var setgenerateinvite = module.exports = {};

setgenerateinvite.function = function(m,ihs){
	var arg = util.parse_arg(m.content, "setgenerateinvite");
	if(arg.length == 1){
		if(arg[0].toLowerCase() === "on" || arg[0].toLowerCase() === "off"){
			config.misc.generate_lobby_invite = arg[0].toLowerCase();
			m.channel.send(util.cm_embed("Invite generation is now turned **" + arg[0].toLowerCase() + "**."));
		} else { m.channel.send(util.redx_embed("Please set invite generation to **on** or **off**.")); }
	}

	util.write_config_to_file(config);
}

setgenerateinvite.name = "setgenerateinvite";
setgenerateinvite.arg = "<on or off>";
setgenerateinvite.description = "sets elocap";
setgenerateinvite.perms = "admin";
setgenerateinvite.category = "settings";
setgenerateinvite.dm = false;
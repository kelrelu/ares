var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var cancel = module.exports = {};

cancel.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		// only allow people to host in in-house channel
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if(g != null){

				// create ur variables for cancel
				if(ihs.ur["cancel"] == null){
					ihs.ur["cancel"] = new Map();
					ihs.ur["cancel"]["awaiting"] = true;
					ihs.ur["cancel"]["timeout"] = null;
					ihs.ur["cancel"]["message"] = [m].map(function(t){return t;})[0]
				}
				// send cancel confirmation
				m.channel.send({ embed:{
					color:0xff050c,
					author:{
						name: g.lobby,
						icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					title:":exclamation: Cancel the lobby __" + g.lobby + "__ ?",
					description: m.author + " Type ``[yes, y]`` or ``[no, n]``to confirm.",
					footer: {
						icon_url: g.host.avatarURL(),
						text: "Host : " + g.host.username
					}
				}}).then(function(){
					ihs.set_tbd_mid("cancel",m.channel.lastMessageID);
				}).catch(console.error);

				// handle timeout if user doesn't respond in time
				ihs.ur["cancel"]["timeout"] = setTimeout(function(){ 
					if(ihs.ur["cancel"]["awaiting"]){
						// fetch and delete confirmation request
						if(ihs.tbd_mids["cancel"]!=null){
							ihs.ur["cancel"]["message"].channel.messages.find("id",ihs.tbd_mids["cancel"]).delete().catch(console.error);
						}
						ihs.ur["cancel"]["message"].channel.send({embed:{
							color: 0xff050c,
							description: ":zzz:  " + ihs.ur["cancel"]["message"].author + " did not respond in time. \n\t\t Not performing **.cancel**. "
						}});
						//reset ur variable
						ihs.ur["cancel"] = null;
					}
				}, config.bot.reply_wait_time_seconds * 1000);
			}
		} else {
			// delete the message
			m.delete().then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(m.author, util.redx_embed("The command ``" + config.bot.prefix + "cancel`` may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete(config.bot.delete_time_wait_seconds * 1000).then().catch(console.error)
			});
		}
	}	
}

cancel.name = "cancel";
cancel.arg = "";
cancel.description = "cancels in-house sign up sheet";
cancel.perms = "mod";
cancel.category = "in-house";
cancel.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var teams = module.exports = {};

teams.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g != null ){
				if( g.made_teams){
					g.show_teams(m,ihs);
				} else { m.channel.send(util.redx_embed("Teams for __" + g.lobby + "__ have not been made yet.")); }} 
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

teams.name = "teams";
teams.arg = "";
teams.description = "shows teams for in-house game";
teams.perms = "mod";
teams.category = "in-house";
teams.dm = false;
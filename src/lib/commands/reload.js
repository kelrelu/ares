var util = require("../util.js");
var db = require("../db.js");
var config = util.config;
var Game = require("../game.js");

var reload = module.exports = {};

reload.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var free_lobbies = Object.keys(ihs.lobbies).filter(function(t){return ihs.lobbies[t] == null}).sort();
			if(free_lobbies.length != 0){
				//check that host isn't in another lobby as a player
				ihs.cl = free_lobbies[0];
				var ih_msgs = m.channel.messages.sort(function(a,b){
					alc = a.createdTimestamp;
					blc = b.createdTimestamp;
					return alc < blc ? 1 : alc > blc ? -1 : 0;
				}).array().filter(function(t){ return t.author.id === m.author.id });
				var ih_reload_msg = ih_msgs[1];
				if(ih_reload_msg){
					var pt_list = ih_reload_msg.content.toLowerCase().split("\n").map(function(t){return t.trim()}).filter(function(t){return ((t === "team 1") || (t === "team 2")) == false}).map(function(t){return t.substring(3,t.length)})
					// console.log(pt_list)

					var all_valid_names = true;
					var invalid_name = "";
					var gp = new Map();
					var capt1 = "";
					var capt2 = "";

					for(var i = 0; i < pt_list.length; i++){
						if(!db.is_on_ladder_ign(pt_list[i])) {
							invalid_name = pt_list[i];
							all_valid_names = false;
							break;
						} else {
							var pinfo = db.dbquery("select * from players where ign='" + db.dbify(pt_list[i]) + "';");
							gp[pinfo[0].discord_id] = pinfo[0].ign;

							if(i == 0){
								capt1 = pinfo[0].ign + " [" + String(pinfo[0].elo) + "]";
							}

							if(i == 5){
								capt2 = pinfo[0].ign + " [" + String(pinfo[0].elo) + "]";
							}
						}
					}
					
					if(pt_list.length == config.ih.max_team_size * 2){
						if(all_valid_names){

							var g = new Game(m.author,ihs.cl); 
							ihs.lobbies[ihs.cl] = g;
											
							g.ih_open = false;
							g.made_teams = true;
							g.checked_names = true;

							g.players = gp;
							g.player_count = 10;

							g.t1 = pt_list.splice(0,5);
							g.t2 = pt_list;

							g.c1 = capt1;
							g.c2 = capt2;

							g.player_count = config.ih.max_team_size * 2;
							g.set_status();

							db.insert_lobby(g);

							m.channel.send(util.cm_embed("Reloaded specified game in __" + ihs.cl + "__."))
							g.show_teams(m,ihs);
						} else {m.channel.send(util.redx_embed("**" + invalid_name + "** is not in the database. Please **__copy and paste__** directly from the original team log."))}
					} else {m.channel.send(util.redx_embed("**Incorrect Format**\n:black_small_square: Could be spelling. Please **__copy and paste__** directly from the original team log.\n:black_small_square: Make sure the team-reload message is the __**last message that you have typed**__ before you type **" + config.bot.prefix + "reload**."))}
				}
			} else { m.channel.send(util.redx_embed("No free lobbies for reloading. :frowning: "))}
		} else {
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

reload.name = "reload";
reload.arg = "";
reload.description = "reloads teams into first free lobby";
reload.perms = "mod";
reload.category = "in-house";
reload.dm = false;
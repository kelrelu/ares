var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var setrnddays = module.exports = {};

setrnddays.function = function(m,ihs){
	var arg = util.parse_arg(m.content, "setrnddays");
	
	if(util.is_integer(arg[0])){
		config.moderation.new_discord_account_min_days = parseInt(arg[0]);
		m.channel.send(util.cm_embed("Set restriction day limit (days since account creation) for new discord accounts to **" + arg[0] + "**."));
	} else {
		m.channel.send(util.redx_embed("Day limit (days since account creation) for new discord accounts **can only be set to an integer.**"));
	}
	

	util.write_config_to_file(config);
}

setrnddays.name = "setrnddays";
setrnddays.arg = "<days>";
setrnddays.description = "sets days limit for new discord accounts";
setrnddays.perms = "admin";
setrnddays.category = "settings";
setrnddays.dm = false;
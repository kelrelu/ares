var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderadd = module.exports = new Map();

ladderadd.function = function(m,ihs){
	var args = util.parse_args(m.content, "ladderadd");
	
	if(args.length === 3){
		args = [args[0],args[1]].concat(args[2].split(" ").filter(Boolean));
	}
	
	if(args.length === 6){
		var user_mention = args[0];
		var user_ign = args[1];
		if(util.is_user_mention(user_mention)){
			// remove user mention and ign from args
			args.splice(0,2);
			if(args.map(util.is_integer).reduce(function(a,b){return a && b})){
				var elo = parseInt(args[0]);
				var mmr = parseInt(args[1]);
				var wins = parseInt(args[2]);
				var losses = parseInt(args[3]);

				// check if the user mention is already on the ladder
				if(!db.is_on_ladder_did(util.get_did(user_mention))){
					// check if the IGN is already on the ladder
					if(!db.is_on_ladder_ign(user_ign)){
						// add to ladder and send success message
						var utc_epoch = new Date().getTime()/1000;
						db.dbquery("insert into summoners(ign) values('" + db.dbify(user_ign) + "') on conflict do nothing;");
						db.dbquery("insert into players(discord_id,ign,elo,mmr,wins,losses,last_played) values ('" + util.get_did(user_mention) + "','" + db.dbify(user_ign) + "'," + String(elo) + "," + String(mmr) + "," + String(wins) + "," + String(losses) + ",to_timestamp(" + utc_epoch + "));");
						m.channel.send(util.cm_embed("Successfully added " + user_mention + " to the ladder as **" + user_ign + "**."));
					
					} else {m.channel.send(util.redx_embed("**" + user_ign + "** is already on the ladder. Cannot add"));}
				} else {m.channel.send(util.redx_embed(user_mention + " is already on the ladder. Cannot add."));}
			} else { m.channel.send(util.error_format(this)); }
		} else { m.channel.send(util.error_format(this)); }
	} else { m.channel.send(util.error_format(this)); }
}

ladderadd.name = "ladderadd";
ladderadd.arg = "<@user> <ign> <elo> <mmr> <wins> <losses>";
ladderadd.description = "adds discord user and IGN to ladder with specified stats";
ladderadd.perms = "admin";
ladderadd.category = "ladder";
ladderadd.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;
var Game = require("../game.js");

var load = module.exports = {};

load.function = function(m,ihs){
	var arg = util.parse_arg(m.content,"load");
	if(arg.length == 1){
		if(db.is_on_ladder_ign(arg[0])){
			ihs.cl = "Lobby D";
			if(ihs.lobbies[ "Lobby D" ] == null){
				ihs.lobbies[ "Lobby D" ] = new Game(m.author,'Lobby D');
			}
			g = ihs.lobbies[ "Lobby D" ];

			var pinfo = db.dbquery("select * from players where ign='" + db.dbify(arg[0]) + "';");
			g.ih_open = true;
			g.made_teams  = false;
			g.t1 = [];
			g.t2 = [];
			if(g.player_count < config.ih.max_team_size * 2){
				g.players[pinfo[0].discord_id] = pinfo[0].ign;
				g.player_count = Object.keys(g.players).length;
				m.channel.send("Loaded player **" + pinfo[0].ign + "** on sign up sheet for Lobby D.\n\n*Current lobby is set to Lobby D*.")	
			} else { m.channel.send("Cannot load. Lobby D is full.")}

		} else { m.channel.send(util.redx_embed("Cannot load. **" + arg[0] + "** is not on the ladder.")) }
	}

}

load.name = "load";
load.arg = "";
load.description = "loads user into Lobby D";
load.perms = "admin";
load.category = "secret";
load.dm = false;
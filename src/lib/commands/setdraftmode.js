var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var setdraftmode = module.exports = {};

setdraftmode.function = function(m,ihs){
	var arg = util.parse_arg(m.content, "setdraftmode");
	if(arg[0].toLowerCase() === "captains" || arg[0].toLowerCase() === "bot mmr" || arg[0].toLowerCase() === "bot soloq"){
		config.ih.draftmode = arg[0].toLowerCase();
		m.channel.send(util.cm_embed("Set draft mode to **" + arg[0].toLowerCase() + "**."));
	} else {m.channel.send(util.redx_embed("Please set draft mode to **captains** or **bot mmr** or **bot soloq**."));}

	util.write_config_to_file(config);
}

setdraftmode.name = "setdraftmode";
setdraftmode.arg = "<captains,bot mmr,bot soloq>";
setdraftmode.description = "sets draft mode to either captains, bot mmr, or bot soloq";
setdraftmode.perms = "admin";
setdraftmode.category = "settings";
setdraftmode.dm = false;
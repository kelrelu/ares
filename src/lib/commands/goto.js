var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var gotolobby = module.exports = {};

gotolobby.function = function(m,ihs){
	var args = util.parse_arg(m.content, "goto");
	if(args.length == 1){

		var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
		if(ih_channel != null){
			// only allow people to host in in-house channel
			if(m.channel == ih_channel){
				var goto_index = util.io_ci(args[0],Object.keys(ihs.lobbies));
				if(goto_index > -1){
					var als = Object.keys(ihs.lobbies).filter(function(t){ return ihs.lobbies[t] != null; });
					var als_index = util.io_ci(args[0],als);
					if(als_index > -1){
						ihs.cl = als[als_index];
						m.channel.send(util.cm_embed("Current lobby is now set to __" + ihs.cl + "__."));
						ihs.set_tbd_mid("drafting",null);
					} else { m.channel.send(util.redx_embed("The lobby __" + config.ih.lobby_names[goto_index] + "__ does not contain an active game."));}
				} else { m.channel.send(util.redx_embed( "__" + args[0] + "__ is not a valid lobby name.")); }
			} else {
				// delete the message
				m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
				// send a notice to the user about posting commands in the right channel
				m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
					e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
				});
			}
		}
	}
}

gotolobby.name = "goto";
gotolobby.arg = "<lobby name>";
gotolobby.description = "sets current lobby to specified lobby";
gotolobby.perms = "mod";
gotolobby.category = "in-house";
gotolobby.dm = false;
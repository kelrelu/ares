var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var stopsilence = module.exports = {};

stopsilence.function = function(m,ihs){
    var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel){
        if(m.channel.id === ih_channel.id){
            var guild = ihs.client.guilds.find("id",config.server.server_id);
            if(guild){
                // var region_role = guild.roles.find("name", config.ih.region.toUpperCase());
                // if(region_role){
                //     ih_channel.overwritePermissions(region_role,{
                //         SEND_MESSAGES: false
                //     }, ["Silence spamming while drafting for an in-house game"] );

                //     m.channel.send(util.cm_embed("In-House channel is now read-only."))
                // }

                ih_channel.overwritePermissions(guild.id,{
                    SEND_MESSAGES: null
                }, ["Disable read-only for in-house channel"] );

                 m.channel.send(util.cm_embed("In-House channel is now back open."))

            }
        } else {
            // delete the message
            m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
            // send a notice to the user about posting commands in the right channel
            m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
                e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
            });
        }
    }
}

stopsilence.name = "stopsilence";
stopsilence.arg = "";
stopsilence.description = "opens up in-house channel for everyone to type again";
stopsilence.perms = "mod";
stopsilence.category = "secret";
stopsilence.dm = false;
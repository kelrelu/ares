var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var shame = module.exports = {};

shame.function = function(m,ihs){
    var arg = util.parse_arg(m.content, "shame");

    m.channel.send(util.cm_embed(arg + " has been added to the shame list."));
}

shame.name = "shame";
shame.arg = "";
shame.description = "shows all current shame";
shame.perms = "all";
shame.category = "secret";
shame.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var move = module.exports = {};

move.function = function(m,ihs){
	// move all t1 people to t1
	// move al t2 people to t2
	
	m.channel.send(util.cm_embed("Moved all players to their respective voice channels."));
}

move.name = "move";
move.arg = "";
move.description = "force moves all signed up players to team voice channels";
move.perms = "all";
move.category = "in-house";

move.dm = true;
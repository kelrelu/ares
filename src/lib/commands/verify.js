var util = require("../util.js");
var db = require("../db.js");
var config = util.config;
var rt = require("rand-token");

var LolApi = require("../../lolapi/lol_api.js");
var lol = new LolApi(config.api.lol);

var verify = module.exports = {};

var regions = util.regions;

verify.function = function(m,ihs){
	var args = util.parse_args(m.content,"verify");
	if(args.length > 0){
		// for one word IGNs
		if(args.length == 1){
			args = args[0].split(" ");
		}
		// verify
		if(args.length > 1){
			var region = args[args.length - 1];
			args.splice(args.length - 1, 1);
			// check if valid region
			if(Object.keys(regions).includes(region.toLowerCase())){
				// check if valid summoner name
				var summoner_name = args.join(" ");
				// call riot api to see if summoner exists
				var api_region = regions[region.toLowerCase()];
				

				lol.get_summoner_by_name(api_region,encodeURIComponent(summoner_name)).then(function(summoner){
					var verify_profile_icon_id = Math.floor(Math.random() * 28);
					while(summoner.profileIconId == verify_profile_icon_id){
						verify_profile_icon_id = Math.floor(Math.random() * 28);
					}

					ihs.lol_verification[m.author.id] = new Map();
					ihs.lol_verification[m.author.id]["piid"] = verify_profile_icon_id;
					ihs.lol_verification[m.author.id]["region"] = region.toLowerCase();
					ihs.lol_verification[m.author.id]["api_region"] = api_region;
					ihs.lol_verification[m.author.id]["summoner_name"] = summoner_name;
					ihs.lol_verification[m.author.id]["summoner_id"] = summoner.id;

					m.author.send({embed:{
						author:{
							name : "Verification Instructions",
							icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
						},
						image : {
							url : "http://ddragon.leagueoflegends.com/cdn/6.24.1/img/profileicon/" + String(verify_profile_icon_id) + ".png"
						},
						description: "To verify the account **" + summoner_name + "**, please change your summoner icon to the icon below.\n\nThen, reply to me with ```\n.done``` "
					}}).catch(function(){
						m.channel.send(m.author,util.error_embed("You don't have DMs enabled for " + config.server.server_name + ". :frowning:\nI can't send you verification instructions."));
					});

					if(m.channel.type != "dm"){
						m.channel.send(util.cm_embed(m.author + ", I've messaged you verification instructions.\nPlease check your messages! :smile:")).then(function(vm){
							m.delete({timeout: config.bot.delete_wait_time_seconds * 1000 }).catch(console.error);
							vm.delete({timeout: config.bot.delete_wait_time_seconds * 1000 }).catch(console.error);
							
						}).catch(console.error);
					}
				}).catch(function(error){
					console.log("here");
					console.log(error);
					if(error.statusCode == 404){
						m.channel.send(util.redx_embed( "**Invalid Summoner Name**\n\nThe summoner **" + summoner_name + "** cannot be found in " + region.toUpperCase() + "."));
					} else {
						util.handle_api_error(m,ihs.client,error);
					}
				});

				// lol.get_summoner_by_name(api_region,encodeURIComponent(summoner_name),function(error,summoner){
				// 	if(error){
				// 		if(error.statusCode == 404){
				// 			m.channel.send(util.redx_embed( "**Invalid Summoner Name**\n\nThe summoner **" + summoner_name + "** cannot be found in " + region.toUpperCase() + "."));
				// 		} else {
				// 			util.handle_api_error(m,ihs.client,error);
				// 		}
				// 	} else {
				// 		// make sure generated icon is different from summoner's current piid
				// 		var verify_profile_icon_id = Math.floor(Math.random() * 28);
				// 		while(summoner.profileIconId == verify_profile_icon_id){
				// 			verify_profile_icon_id = Math.floor(Math.random() * 28);
				// 		}

				// 		ihs.lol_verification[m.author.id] = new Map();
				// 		ihs.lol_verification[m.author.id]["piid"] = verify_profile_icon_id;
				// 		ihs.lol_verification[m.author.id]["region"] = region.toLowerCase();
				// 		ihs.lol_verification[m.author.id]["api_region"] = api_region;
				// 		ihs.lol_verification[m.author.id]["summoner_name"] = summoner_name;
				// 		ihs.lol_verification[m.author.id]["summoner_id"] = summoner.id;

				// 		m.author.send({embed:{
				// 			author:{
				// 				name : "Verification Instructions",
				// 				icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
				// 			},
				// 			image : {
				// 				url : "http://ddragon.leagueoflegends.com/cdn/6.24.1/img/profileicon/" + String(verify_profile_icon_id) + ".png"
				// 			},
				// 			description: "To verify the account **" + summoner_name + "**, please change your summoner icon to the icon below.\n\nThen, reply to me with ```\n.done``` "
				// 		}}).catch(function(){
				// 			m.channel.send(m.author,util.error_embed("You don't have DMs enabled for " + config.server.server_name + ". :frowning:\nI can't send you verification instructions."));
				// 		});

				// 		if(m.channel.type != "dm"){
				// 			m.channel.send(util.cm_embed(m.author + ", I've messaged you verification instructions.\nPlease check your messages! :smile:")).then(function(vm){
				// 				m.delete({timeout: config.bot.delete_wait_time_seconds * 1000 }).catch(console.error);
				// 				vm.delete({timeout: config.bot.delete_wait_time_seconds * 1000 }).catch(console.error);
								
				// 			}).catch(console.error);
				// 		}	
				// 	}
				// });
			} else { m.channel.send(util.redx_embed("**Invalid Region**\n\n**" + config.bot.prefix + "verify " + this.arg + "**")).then(function(mes){
				// m.delete({timeout: config.bot.delete_wait_time_seconds * 1000 }).catch(console.error);
				// mes.delete({timeout:config.bot.delete_wait_time_seconds * 1000}).catch(console.error);
			}); }
		} else { m.channel.send(util.error_format(this)).then(function(mes){
			// m.delete({timeout: config.bot.delete_wait_time_seconds * 1000 }).catch(console.error);
			// mes.delete({timeout:config.bot.delete_wait_time_seconds * 1000}).catch(console.error);
		}); }
	} else { m.channel.send(util.error_format(this)).then(function(mes){
		// m.delete({timeout: config.bot.delete_wait_time_seconds * 1000 }).catch(console.error);
		// mes.delete({timeout:config.bot.delete_wait_time_seconds * 1000}).catch(console.error);
	}); } 
}

verify.name = "verify";
verify.arg = "IGN REGION";
verify.description = "starts verification process for summoner rank tags";
verify.perms = "all";
verify.category = "info";
verify.dm = true;
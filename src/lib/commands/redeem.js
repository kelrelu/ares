var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var redeem = module.exports = {};

redeem.function = function(m,ihs){
    var arg = util.parse_arg(m.content, "redeem");

    m.channel.send(util.cm_embed(arg + " has been redeemed."));
}

redeem.name = "redeem";
redeem.arg = "";
redeem.description = "redeems in-house mod from shame list";
redeem.perms = "admin";
redeem.category = "secret";
redeem.dm = false;
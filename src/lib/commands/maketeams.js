var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var maketeams = module.exports = {};

maketeams.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g != null ){
				if(!g.ih_open){
					if(g.made_teams === false){
						g.made_teams = true;
						g.show_teams(m,ihs);
					} else { m.channel.send(util.redx_embed("Team log already exists. To delete existing team log, type **" + config.bot.prefix + "deleteteams**")); }
				} else { m.channel.send(util.redx_embed("Please close current in-house lobby before making teams!\nType **" + config.bot.prefix + "close** to close the in-house lobby.")); }
			} 
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

maketeams.name = "maketeams";
maketeams.arg = "";
maketeams.description = "makes teams for in-house lobby";
maketeams.perms = "mod";
maketeams.category = "in-house";
maketeams.dm = false;
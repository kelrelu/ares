var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderclear = module.exports = new Map();

ladderclear.function = function(m,ihs){
	// add user response for ladderclear if it doesn't exist
	if(ihs.ur["ladderclear"] == null){
		ihs.ur["ladderclear"] = new Map();
		ihs.ur["ladderclear"]["awaiting"] = true;
		ihs.ur["ladderclear"]["timeout"] = null;
		ihs.ur["ladderclear"]["message"] = [m].map(function(t){return t;})[0] // why are you doing this? figure out now
	}
	// send confirmation message
	m.channel.send({ embed:{
		color:0xff050c,
		author: {
			name: config.server.server_name,
			icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL
		},
		title:":bangbang:Clear the " + config.server.server_name + " " + config.ih.region.toUpperCase() + " In-House Season " + String(config.ih.season) + " Ladder ?",
		description: "**:octagonal_sign: THIS WILL DELETE THE ENTIRE LADDER AND ALL PLAYERS**\n\n" + m.author + " Type ``[yes, y]`` or ``[no, n]``to confirm.",
		timestamp : new Date(),
		footer: {
			icon_url: m.author.avatarURL,
			text: + m.author.username
		}
	}}).then(function(){
		ihs.set_tbd_mid("ladderclear",m.channel.lastMessageID);
	}).catch(console.error);

	// handle timeout if user doesn't respond in time
	ihs.ur["ladderclear"]["timeout"] = setTimeout(function(){ 
		if(ihs.ur["ladderclear"]["awaiting"]){
			// fetch and delete confirmation request
			if(ihs.tbd_mids["ladderclear"]!=null){
				ihs.ur["ladderclear"]["message"].channel.messages.find("id",ihs.tbd_mids["ladderclear"]).delete().catch(console.error);
			}
			ihs.ur["ladderclear"]["message"].channel.send({embed:{
				color: 0xff050c,
				description: ":zzz:  " + ihs.ur["ladderclear"]["message"].author + " did not respond in time. \n\t\t Not performing **.ladderclear**. "
			}});
			//reset ur variable
			ihs.ur["ladderclear"] = null;
		}
	}, config.bot.reply_wait_time_seconds * 1000);
}

ladderclear.name = "ladderclear";
ladderclear.arg = "";
ladderclear.description = "clears all ladder data";
ladderclear.perms = "admin";
ladderclear.category = "ladder";
ladderclear.dm = false;
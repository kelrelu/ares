var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var close = module.exports = {};

close.function = async function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g!=null ) {
				if(g.ih_open) {
					if(Object.keys(g.players).length == config.ih.max_team_size * 2){
						if(g.checked_names){

							// captains mode
							if(config.ih.draftmode === "captains"){
								if(g.bad_player === false){
									g.ih_open = false;
									g.set_status();
									g.update_players();
									g.set_captains();

									// delete message in lfg
									if(config.misc.generate_lfg_ih_notice){
										if(ihs.tbd_mids["lfg_ih_notice"]){
											var lfg_channel = ihs.client.channels.find("id",config.server.lfg_channel);
											if(lfg_channel != null){
												var msg = lfg_channel.messages.find("id",ihs.tbd_mids["lfg_ih_notice"]);
												if(msg){
													msg.delete().then().catch(console.error);
												}
											}
										}
									}

									// create invite if invite setting is turned on
									if(config.misc.generate_lobby_invite === "on"){
										// generate invite to voice lobby
										var ih_voice_channel= ihs.client.guilds.find("id",config.server.server_id).channels.find("id",config.server.ih_voice_channel);
										if(ih_voice_channel != null){
											ih_voice_channel.createInvite({ "reason": "Invite for " + config.ih.region.toUpperCase() + " In-House Players"}).then(function(invite){
												m.channel.send( {embed :{
														color:3447003,
														author:{
															name: g.lobby + " Closed!",
															icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
														},
														title: "",
														description: "\n\n All players please go to the **In-House Lobby Voice Channel** linked below to start drafting. :smile: ",
														footer: {
															icon_url: g.host.avatarURL,
															text: "Host : " + g.host.username
														}
												}});
												m.channel.send(invite.url);
											});
										} 
									} else {
										m.channel.send({embed:{
											color:3447003,
											author:{
												name: g.lobby + " Closed!",
												icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
											},
											title: "",
											description: "\n\n All players please go to the **In-House Lobby Voice Channel** to start drafting. :smile: ",
											footer: {
												icon_url: g.host.avatarURL(),
												text: "Host : " + g.host.username
											}
										}});
									}
								} else {
									m.channel.send(util.redx_embed("There are players marked with an :x: on the sign up sheet!\n Please make changes and then **" + config.bot.prefix + "checknames** again before closing."));
								}

							}
							// bot mmr mode
							else if(config.ih.draftmode === "bot mmr"){
								if(g.bad_player === false){
									g.draft_teams_ih_mmr();
									g.set_status();
									g.set_captains();
									g.show_teams(m,ihs);

									db.insert_lobby(g);

									// delete message in lfg
									if(config.misc.generate_lfg_ih_notice){
										if(ihs.tbd_mids["lfg_ih_notice"]){
											var lfg_channel = ihs.client.channels.find("id",config.server.lfg_channel);
											if(lfg_channel != null){
												var msg = lfg_channel.messages.find("id",ihs.tbd_mids["lfg_ih_notice"]);
												if(msg){
													msg.delete().then().catch(console.error);
												}
											}
										}
									}
								} else {
									m.channel.send(util.redx_embed("There are players marked with an :x: on the sign up sheet!\n Please make changes and then **" + config.bot.prefix + "checknames** again before closing."));
								}
							// bot soloq mode
							} else if(config.ih.draftmode === "bot soloq"){
								m.channel.send(util.cm_embed("Please wait several seconds for teams to be generated....")).then(m => m.delete({timeout:2000}));
								if( (g.invalid_ign === false) && (g.underleveled_account === false)){
									// console.log("NOT SUPPOED TO HAPPEN".red)
									await g.update_players();
									console.log("hi".blue);
									g.set_captains();
									g.draft_teams_soloq();
									g.set_status();
									g.show_teams(m,ihs);

									db.insert_lobby(g);

									// delete message in lfg
									if(config.misc.generate_lfg_ih_notice){
										if(ihs.tbd_mids["lfg_ih_notice"]){
											var lfg_channel = ihs.client.channels.find("id",config.server.lfg_channel);
											if(lfg_channel != null){
												var msg = lfg_channel.messages.find("id",ihs.tbd_mids["lfg_ih_notice"]);
												if(msg){
													msg.delete().then().catch(console.error);
												}
											}
										}
									}
								} else {
									m.channel.send(util.redx_embed("There are players marked with an :x: on the sign up sheet!\n Please make changes and then **" + config.bot.prefix + "checknames** again before closing."));
								}
							}

							
							// create team lobbies if setting if vc generation setting is turned on
							var guild = ihs.client.guilds.find("id",config.server.server_id);
							if(config.misc.generate_team_vc === "on"){
								var ih_category_channel = ihs.client.guilds.find("id",config.server.server_id).channels.find("id",config.server.ih_category_channel);
								if(ih_category_channel != null ){
									var ihcc_position = ih_category_channel.position;
									// get the correct position depending on existing lobbies
									var vc_lobbies = Object.keys(ihs.vc);
									var vc_l_index = vc_lobbies.indexOf(g.lobby);
									var position_plus = 0;

									// get number of existing lobbies before our lobby
									for(var i = 0;i < vc_l_index; i++){
										if(vc_lobbies[i]){
											position_plus += 2;
										}
									}

									guild.createChannel(g.lobby + " - Team 1", "voice").then(function(c){
										c.edit({
											position: ihcc_position + position_plus + 1,
											userLimit: 5,
											parentID: ih_category_channel.id
										}).then(function(c){
											g.set_t1_vc(c);
										});
									});

									guild.createChannel(g.lobby + " - Team 2", "voice").then(function(c){
										c.edit({
											position: ihcc_position + position_plus + 2,
											userLimit: 5,
											parentID: ih_category_channel.id
										}).then(function(c){
											g.set_t2_vc(c);
										});
									});
								
									ihs.vc[g.lobby] = true;
								}
							}
								
							
						} else {m.channel.send(util.redx_embed("Please **" + config.bot.prefix + "checknames** before closing."))}
					} else {m.channel.send(util.redx_embed("Cannot close the lobby __" + ihs.cl + "__! Sign up list has not reached max capacity."))}
				} 
			} 
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

close.name = "close";
close.arg = "";
close.description = "closes an in-house sign up, saves IGN and discord user changes, adds new players to the ladder";
close.perms = "mod";
close.category = "in-house";
close.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ign = module.exports = {};

ign.function = function(m,ihs){
	var args = util.parse_arg(m.content, "ign");
	if(args.length == 1){
		var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
		if(ih_channel != null){
			// only allow people to host in in-house channel
			if(m.channel == ih_channel){
				
					// allow sign up even if mods are updating another game
				var g = ihs.get_open_lobby();
				if( g != null ){
					if(args[0].length <=16){
						// send message if already signed up
						if(g.is_signed_up_user(m.author)){
							var sus_ign = g.players[m.author.id];
							if(sus_ign.toUpperCase() === args[0].toUpperCase()) {
								m.channel.send(util.cm_embed("You are already signed up with ign **" + args[0] + "**.\n"));
							} else {
								// check they aren't signing up with something already on the sign up sheet
								if(!(g.is_signed_up_ign(args[0]))){
									g.players[m.author.id] = args[0];
									m.channel.send(util.cm_embed(m.author + ", updated your ign from **" + sus_ign + "** to **" + args[0] + "**."));
									g.show_players(m);
								} else { m.channel.send(util.redx_embed("**" + args[0] + "** is already on the sign up sheet."));}
							}
						} else {
							if(g.player_count < config.ih.max_team_size * 2){
								if(!(g.is_signed_up_ign(args[0]))){
									g.players[m.author.id] = args[0];
									g.player_count += 1;
									g.show_players(m);
								} else { m.channel.send(util.redx_embed("**" + args[0] + "** is already signed up!"));}
							} else { m.channel.send(util.redx_embed(m.author + " Sorry! Current in-house game is full. :sob:")); }			
						}
					} else { m.channel.send(util.redx_embed( m.author + " IGN too long! League IGNs must be 16 characters or less. "))}
				} else {
					m.channel.send({embed:{
						description : ":x:" + m.author + " Sorry! No open in-house lobby right now.\nPlease wait for a mod to host a lobby before signing up."
					}});
				}
				
			} else {
				// delete the message
				m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
				// send a notice to the user about posting commands in the right channel
				m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
					e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
				});
			}
		}
	}
}

ign.name = "ign";
ign.arg = "<ign>";
ign.description = "signs up player on in-house sign up sheet";
ign.perms = "all";
ign.category = "in-house";
ign.dm = false;
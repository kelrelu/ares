var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderstats = module.exports = new Map();

ladderstats.function = function(m,ihs){
	// variables to show in ladder statistics report
	var ranked_players = db.dbquery("select * from players p where (p.wins + p.losses) >= " + String(config.ih.num_placement_matches) + ";");
	var players = db.dbquery("select * from players p where p.wins > 0 or p.losses > 0;");
	
	var games = db.dbquery("select * from games;");
	var avg_games = db.dbquery("select avg(p.wins + p.losses) from players p;")[0];
	var avg_elo = db.dbquery("select avg(elo) from players;")[0];
	var avg_mmr = db.dbquery("select avg(mmr) from players;")[0];
	
	var t1_wins = db.dbquery("select * from games where winner = 1");
	var t2_wins = db.dbquery("select * from games where winner = 2");

	m.channel.send({embed:{
		color:3447003,
		author:{
			name: config.server.server_name + " Ladder Statistics",
			icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
		},
		fields: [{
			name: "__Season__",
			value: String(config.ih.season),
			inline: true
		},
		{
			name: "__Games__",
			value: "Number of Games : " + String(games.length) + "\n      T1 Wins : " + String(t1_wins.length) + "\n      T2 Wins : " + String(t2_wins.length),
			inline: true
		},
		{
			name: "__Players__",
			value: "Number of Active Players : " + String(players.length) + "\n      Ranked : " +  String(ranked_players.length) + "\n      Unranked : " + String(players.length - ranked_players.length),
			inline: true
		},
		{
			name: "__Numbers by Player__",
			value: "Avg Games : " + String(Math.round(avg_games.avg * 100)/100) + "\nAvg Elo : " + String(Math.round(avg_elo.avg * 100)/100) + "\nAvg MMR : " + String(Math.round(avg_mmr.avg * 100)/100),
			inline: true
		}]
	}});
}

ladderstats.name = "ladderstats";
ladderstats.arg = "";
ladderstats.description = "shows season statistics for the ladder";
ladderstats.perms = "all";
ladderstats.category = "info";
ladderstats.dm = true;
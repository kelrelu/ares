var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var setrnd = module.exports = {};

setrnd.function = function(m,ihs){
	var arg = util.parse_arg(m.content, "setrnd");
	if(arg.length == 1){
		if(arg[0].toLowerCase() === "on" || arg[0].toLowerCase() === "off"){
			config.moderation.new_discord_account_ih_restrict_access = arg[0].toLowerCase();
			m.channel.send(util.cm_embed("Restricting new discord accounts from in-houses is now turned **" + arg[0].toLowerCase() + "**."));
		} else { m.channel.send(util.redx_embed("Please set restricting new discord accounts to **on** or **off**.")); }
	}

	util.write_config_to_file(config);
}

setrnd.name = "setrnd";
setrnd.arg = "<on or off>";
setrnd.description = "sets toggle to add unverified role to new discord accounts";
setrnd.perms = "admin";
setrnd.category = "settings";
setrnd.dm = false;
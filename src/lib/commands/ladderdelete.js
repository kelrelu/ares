var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderdelete = module.exports = new Map();

ladderdelete.function = function(m,ihs){
	var args = util.parse_arg(m.content, "ladderdelete");

	if(util.is_user_mention(args[0])){
		if(db.is_on_ladder_did(util.get_did(args[0]))){
			db.dbquery("delete from players where discord_id='" + util.get_did(args[0]) + "';");
			m.channel.send(util.cm_embed("Successfully deleted " + args[0] + " from the ladder."));
		} else { m.channel.send(util.redx_embed(args[0] + " is not on the ladder. Cannot delete.")); }
	} else {
		if(db.is_on_ladder_ign(args[0])){
			db.dbquery("delete from players where ign='" + db.dbify(args[0]) + "';")
			m.channel.send(util.cm_embed("Successfully deleted **" + args[0] + "** from the ladder."));
		} else { m.channel.send(util.redx_embed("**" + args[0] + "** is not on the ladder. Cannot delete."))}
	}
	
}

ladderdelete.name = "ladderdelete";
ladderdelete.arg = "<@user or ign>";
ladderdelete.description = "deletes player from the ladder";
ladderdelete.perms = "admin";
ladderdelete.category = "ladder";
ladderdelete.dm = false;
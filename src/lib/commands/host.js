var util = require("../util.js");
var db = require("../db.js");
var config = util.config;
var Game = require("../game.js");

var host = module.exports = {};

host.function = function(m,ihs){
	var args = util.parse_arg(m.content, "host");
	if(args.length == 1){
		var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
		if(ih_channel != null){
			// only allow people to host in in-house channel
			if(m.channel == ih_channel){
				// don't allow two open lobbies at a time
				if(!(ihs.exists_open_lobby())){
					// create game and set to first free lobby
					var fls = Object.keys(ihs.lobbies).filter(function(t){return ihs.lobbies[t] == null});
					if(fls.length > 0){
						var host_ign = args[0]
						ihs.cl = fls[0];
						ihs.lobbies[fls[0]] = new Game(m.author,fls[0]);

						// notplaying starts empty sheet, otherwise add host ign to sign up sheet
						if(host_ign.toLowerCase() != "notplaying"){
							ihs.lobbies[ihs.cl].players[m.author.id] = host_ign;
							ihs.lobbies[ihs.cl].player_count += 1;
						}

						if(config.misc.generate_lfg_ih_notice === "on"){
							// send message notice to lfg channel
							var lfg_channel = ihs.client.channels.find("id",config.server.lfg_channel);
							if(lfg_channel != null){
								lfg_channel.send({embed:{
									color: 3447003,
									author:{
										name: config.server.server_name  + " " + config.ih.region.toUpperCase() + " In-House Sign Ups Open!",
										icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
									},
									description: "\u200b\n\u200b**Looking for players __right now__ in <#" + config.server.ih_channel + ">!**\n\nCome sign up for **5v5 custom games** with members of " + config.server.server_name + ".:thumbsup:\nAll ranks welcome."
									// \nNOTE:**\n:diamond_shape_with_a_dot_inside: You must be at least level 30 and own 20 champions in order to participate in In-Houses as there are now 10 bans in Tournament Draft. \n:diamond_shape_with_a_dot_inside: Players may also have no choice but to play an off role. __Please be prepared for this__."
								}}).then(function(){
									ihs.set_tbd_mid("lfg_ih_notice",lfg_channel.lastMessageID);
								});
							}
						}

						// send message notice to in-house channel
						m.channel.send("<@&" + config.roles.ih_player.join("> <@&") + ">", {embed :{
								color:3447003,
								author:{
									name: config.server.server_name + " " + config.ih.region.toUpperCase() + " In-House Sign Ups Open!",
									icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
								},
								title: "",
								description: "\n\nHost : " + m.author + "\nLobby : "+ ihs.cl + "\n```Type " + config.bot.prefix + "ign <ign> below to sign up.``` ",
						}});

					} else { m.channel.send(util.redx_embed("All lobbies are full! Cannot open a host right now.")); }
				} else { m.channel.send(util.redx_embed(m.author + ", another host is in the middle of signing up players for the lobby __" + ihs.get_open_lobby().lobby + "__!\n\nPlease **wait for their sign up to finish** before opening up a new lobby.")); }
			} else {
				// delete the message
				m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
				// send a notice to the user about posting commands in the right channel
				m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
					e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
				});
			}
		}
	}
	
}

host.name = "host";
host.arg = "<notplaying or ign>";
host.description = "opens in-house signup sheet";
host.perms = "mod";
host.category = "in-house";
host.dm = false;
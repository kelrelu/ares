var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var veterans = module.exports = {};

veterans.function = function(m,ihs){
    var players = db.dbquery("select * from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + " order by (wins+losses) desc, wins desc, losses asc limit " + config.ih.max_vets + ";");
	var vets_string = "```css\n   IGN" + " ".repeat(14) + " GAMES W   L ``````md\n";
	if(players.length!=0){
		for (var i = 0; i < Math.min(config.ih.max_vets,players.length); i ++){
			vets_string += String(i+1) + "." + " ".repeat(3-String(i+1).length) + players[i].ign + " ".repeat(17 - players[i].ign.length) + String(players[i].losses + players[i].wins) + " ".repeat(6-String(players[i].losses + players[i].wins).length) + String(players[i].wins) + " ".repeat(4-String(players[i].wins).length) + String(players[i].losses) + "\n";
		}
	}
	vets_string += " ```\n";
	m.channel.send({embed:{
		color:3447003,
		title: config.ih.emoji_vet + " ** " + config.server.server_name + "'s Veteran " + config.ih.region.toUpperCase() + " In-House Players **" + config.ih.emoji_vet + "\n",
		description: vets_string
	}});
}

veterans.name = "veterans";
veterans.arg = "";
veterans.description = "shows all current veterans";
veterans.perms = "all";
veterans.category = "info";
veterans.dm = true;
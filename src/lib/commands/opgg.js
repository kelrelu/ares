var fetch = require("fetch").fetchUrl;

var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var opgg = module.exports = {};

opgg.function = function(m,ihs){
	var args = util.parse_args(m.content,"opgg");
	var player_opgg = "https://" + config.ih.region + ".op.gg/summoner/userName=" + args.join("+");
	// check that webpage exists before sending it
	
	fetch(player_opgg, function(error, meta, body){
    	var xml = body.toString()
    	if(xml.includes("This summoner is not registered at OP.GG. Please check spelling.")){
    		m.channel.send(util.q_embed("**Invalid IGN**\n\t\tThe summoner **" + args.join(" ") + "** cannot be found in " + config.ih.region.toUpperCase() + ".")); 
    	} else {
    		m.channel.send(player_opgg);
    	}
	});
}

opgg.name = "opgg";
opgg.arg = "<ign>";
opgg.description = "outputs opgg for player in given in-house region";
opgg.perms = "all";
opgg.category = "info";
opgg.dm = true;
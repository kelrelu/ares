var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var bot_mention = module.exports = {};

bot_mention.function = function(m){
	if(m.content.toLowerCase().includes("help")){
		m.channel.send({embed:{
			description: "Hi! Try typing **" + config.bot.prefix + "helplfg** to see a list of commands you can run. :smile: "
		}});
	}
	else if(m.content.toLowerCase().includes("creator") || m.content.toLowerCase().includes("who made you") || m.content.toLowerCase().includes("who created you") || m.content.toLowerCase().includes("create") || m.content.toLowerCase().includes("sapc") || m.content.toLowerCase().includes("kel")) {
		m.channel.send({embed:{
			description:"My creator is <@" + config.bot.owner + ">. Please don't ping though. :thumbsup: "
		}});
	}
	else if(m.content.toLowerCase().includes("love")){
		var responses = [
			"Love. :thinking:", 
			"Love. Let me think. :thinking: Try pinging me later.",
			"Hmm :pensive:...don't talk to me about love.",
		];
		var random_index = Math.floor(Math.random() * responses.length);
		m.channel.send(responses[random_index]);
	} else {
		var responses = [ 
			":unamused:",
			":shrug:",
			":astonished:",
			":wink:",
			":kissing_heart:", 
			":kissing_closed_eyes:",
			":stuck_out_tongue_closed_eyes:",
			":angry:",
			":thinking:",
			":scream:",
			":smirk:",
			":sleeping:",
			"K. :ok_hand:",
			":thumbsup:",
			":thumbsdown:"
		];
		var random_index = Math.floor(Math.random() * responses.length);
		m.channel.send(responses[random_index]);
	}	
}

bot_mention.name = "bot_mention";
bot_mention.arg = "";
bot_mention.description = "mentioning the bot triggers these responses";
bot_mention.perms = "all";
bot_mention.category = "secret";
bot_mention.dm = true;
var fs = require("fs");
var db = require("../db.js");
var util = require("../util.js");
var config = util.config;

var ladder = module.exports = {}

ladder.function = function(m,ihs){
	var players = db.dbquery("select * from players p where (p.wins + p.losses) >= " + String(config.ih.num_placement_matches) + " order by p.elo desc,p.mmr desc, p.wins desc,p.losses asc,p.ign asc limit " + String(config.ih.max_leaderboard) + ";");
	var ladder_string = "```css\n   IGN" + " ".repeat(15) + "ELO   MMR   W   L ``````fix\n";
	if(players.length!=0){
		if(players.length < config.ih.max_top_players){
			for (var i = 0; i < players.length; i ++){
				ladder_string += String(i+1) + "." + " ".repeat(3-String(i+1).length) + players[i].ign + " ".repeat(17 - players[i].ign.length) + String(players[i].elo) + "  " + String(players[i].mmr) + "  " + String(players[i].wins) + " ".repeat(4-String(players[i].wins).length) + String(players[i].losses) + "\n";
			}
		}
		else{
			for (var i = 0; i < config.ih.max_top_players; i ++){
				ladder_string += String(i+1) + "." + " ".repeat(3-String(i+1).length) + players[i].ign + " ".repeat(17 - players[i].ign.length) + String(players[i].elo) + "  " + String(players[i].mmr) + "  " + String(players[i].wins) + " ".repeat(4-String(players[i].wins).length) + String(players[i].losses) + "\n";
			}
			ladder_string += "``````ini\n"
			for (var i = config.ih.max_top_players; i < Math.min(config.ih.max_leaderboard,players.length); i ++){
				ladder_string += String(i+1) + "." + " ".repeat(3-String(i+1).length) + players[i].ign + " ".repeat(17 - players[i].ign.length) + String(players[i].elo) + "  " + String(players[i].mmr) + "  " + String(players[i].wins) + " ".repeat(4-String(players[i].wins).length) + String(players[i].losses) + "\n";
			}
		}
	}
	ladder_string += " ```\n";
	m.channel.send({embed:{
		color:3447003,
		title: config.ih.emoji_leaderboard + "__**" + config.server.server_name + "'s Top " + String(config.ih.max_leaderboard) + " " + config.ih.region.toUpperCase() + " In-House Players**__" + config.ih.emoji_leaderboard,
		description: ladder_string,
		timestamp: new Date()
	}});
}
ladder.name = "ladder";
ladder.arg = "";
ladder.description = "shows leaderboard, top players on the ladder";
ladder.perms = "all";
ladder.category = "info"
ladder.dm = true;

var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var remove = module.exports = new Map();

remove.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var arg = util.parse_arg(m.content, "delete");
			if(arg.length > 0){
				var g = ihs.get_lobby(ihs.cl);
				if( g!=null ){
					// delete player on sign up sheet
					if(g.ih_open){
						// user mention
						if(util.is_user_mention(arg[0])){
							if(g.is_signed_up_user(arg[0])){
								var ign = g.players[util.get_did(arg[0])];
								delete g.players[util.get_did(arg[0])];
								g.player_count -= 1;
								g.set_status();
								//add delete messages
								m.channel.send(util.cm_embed("Deleted " + arg[0] + " (**" + ign + "**) from the sign up sheet."));
								g.show_players(m);
							} else { m.channel.send(util.redx_embed("**" + arg[0] + "** is not on the sign up sheet. Cannot remove."));}
						// ign
						} else {
							if(g.is_signed_up_ign(arg[0])){
								var pid = Object.keys(g.players).filter(function(u) {return ((g.players)[u]).toUpperCase() === arg[0].toUpperCase()})[0];
								delete g.players[pid];
								g.player_count -= 1;
								g.set_status();
								
								// add delete messsages
								m.channel.send(util.cm_embed("Deleted **" + arg[0] + "** from the sign up sheet."));
								g.show_players(m);
							} else { m.channel.send(util.redx_embed("**" + arg[0] + "** is not on the sign up sheet. Cannot remove."));}
						}
					// remove on team log
					} else if(g.made_teams){
						if(g.is_on_team(arg[0])){
							var t1_index = util.io_ci(arg[0],g.t1);
								// console.log(t1_index);
							var t2_index = util.io_ci(arg[0],g.t2);
							// console.log(t2_index);
							if(t1_index > -1){
								g.t1.splice(t1_index,1);
								g.t1_count -= 1;
							} else if(t2_index > -1) {
								g.t2.splice(t2_index,1);
								g.t2_count -= 1;
							}
							g.set_status();
							if(g.status != "closed, in game"){
								db.delete_lobby(g);
							}
							m.channel.send(util.cm_embed("Deleted **" + arg[0] + "** from team log."));
							g.show_teams(m,ihs);
						} else { m.channel.send(util.redx_embed("**" + arg[0] + "** is not on a team. Cannot remove.")); }
					}
				}
			}
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

remove.name = "delete";
remove.arg = "<@user or ign>";
remove.description = "deletes player from the in-house sign up sheet or team log";
remove.perms = "mod";
remove.category = "in-house";

remove.dm = false;
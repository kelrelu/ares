var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var rename = module.exports = {};

rename.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var args = util.parse_args(m.content, "rename");
			if(args.length == 2){
				var g = ihs.get_lobby(ihs.cl);
				if( g!=null ){
					// rename on sign up sheet
					if(g.ih_open){
						// user mention
						if(util.is_user_mention(args[0])){
							if(g.is_signed_up_user(args[0])){
								if(!g.is_signed_up_ign(args[1])){
									var ign = g.players[util.get_did(args[0])];
									g.players[util.get_did(args[0])] = args[1];
									m.channel.send(util.cm_embed("Successfully renamed " + args[0] + " (**" + ign + "**) with **" + args[1] +"** on the sign up sheet."));
								} else { m.channel.send(util.redx_embed("New ign **" + args[1] + "** is already signed up. Cannot rename."));}
							} else { m.channel.send(util.redx_embed("**" + args[0] + "** is not on the sign up sheet. Cannot rename."));}
						// ign
						} else {
							if(g.is_signed_up_ign(args[0])){
								if(!g.is_signed_up_ign(args[1])){
									var pid = Object.keys(g.players).filter(function(t){return g.players[t].toUpperCase() === args[0].toUpperCase()})[0];
									g.players[pid] = args[1];
									m.channel.send(util.cm_embed("Successfully renamed **" + args[0] + "** with **" + args[1] + "** on the sign up sheet."));
									g.show_players(m,ihs);
								} else { m.channel.send(util.redx_embed("New ign **" + args[1] + "** is already signed up. Cannot rename."));}
							} else { m.channel.send(util.redx_embed("**" + args[0] + "** is not on the sign up sheet. Cannot rename."));}
						}
					// rename on team log
					} else if(g.made_teams){

						console.log("HERE".magenta)
						if(g.is_on_team(args[0])){
							if(!g.is_on_team(args[1])){
								var t1_index = util.io_ci(args[0], g.t1);
								var t2_index = util.io_ci(args[0], g.t2);

								if(t1_index > -1){ g.t1[t1_index] = args[1];}
								else { g.t2[t2_index] = args[1]; }
								m.channel.send(util.cm_embed("Successfully renamed **" + args[0] + "** with **" + args[1] + "** on the team log."));
								g.show_teams(m,ihs);
							} else { m.channel.send(util.redx_embed("**" + args[1] + "** is already on a team! Cannot rename."));}
						} else { m.channel.send(util.redx_embed("**" + args[0] + "** is not on a team. Cannot rename.")); }
					}
				}
			} else { m.channel.send(util.error_format(this)); }
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

rename.name = "rename";
rename.arg = "<@user or ign> <ign>";
rename.description = "renames player on sign up sheet or team log";
rename.perms = "mod";
rename.category = "in-house";
rename.dm = false;
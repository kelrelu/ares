var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderprune = module.exports = new Map();

ladderprune.function = function(m,ihs){
	// delete summoners who haven't played a game from summoners + players
	db.dbquery("delete from players where wins = 0 and losses = 0;");
	// clean up summoners
	db.dbquery("delete from summoners s where s.ign not in \
		((select t1p1 from games) \
		union (select t1p2 from games) \
		union (select t1p3 from games) \
		union (select t1p4 from games) \
		union (select t1p5 from games) \
		union (select t2p1 from games) \
		union (select t2p2 from games) \
		union (select t2p3 from games) \
		union (select t2p4 from games) \
		union (select t2p5 from games) \
		union (select ign from players)) on conflict do nothing;");
	m.channel.send(util.cm_embed("Pruned ladder and removed all players who have not played a game."));
}

ladderprune.name = "ladderprune";
ladderprune.arg = "";
ladderprune.description = "prunes all players with 0 wins and 0 losses from the ladder";
ladderprune.perms = "admin";
ladderprune.category = "ladder";
ladderprune.dm = false;

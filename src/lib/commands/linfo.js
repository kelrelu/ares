var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var linfo = module.exports = new Map();

linfo.function = function(m,ihs){
}

linfo.name = "linfo";
linfo.arg = "";
linfo.description = "messages detailed information about all currently active lobbies to user, to be used for bets";
linfo.perms = "all";
linfo.category = "betting";
linfo.dm = true;
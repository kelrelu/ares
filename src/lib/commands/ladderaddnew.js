var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderaddnew = module.exports = new Map();

ladderaddnew.function = function(m,ihs){
	var args = util.parse_args(m.content, "ladderaddnew");
	if(args.length === 2){
		var user_mention = args[0];
		var user_ign = args[1];
		if(util.is_user_mention(user_mention)){
			// check if the user mention is already on the ladder
			if(!db.is_on_ladder_did(util.get_did(user_mention))){
				// check if the IGN is already on the ladder
				if(!db.is_on_ladder_ign(user_ign)){
					var utc_epoch = new Date().getTime()/1000;
					db.dbquery("insert into summoners(ign) values('" + db.dbify(user_ign) + "') on conflict(ign) do nothing;");
					db.dbquery("insert into players(discord_id,ign,elo,mmr,wins,losses,last_played) values ('" + util.get_did(user_mention) + "','" + db.dbify(user_ign) + "'," + String(config.ih.default_elo) + "," + String(config.ih.default_mmr) + ",0,0,to_timestamp(" + utc_epoch + "));");
					m.channel.send(util.cm_embed("Successfully added new player " + user_mention + " to the ladder as **" + user_ign + "**."));
				} else { m.channel.send(util.redx_embed("**" + user_ign + "** is already on the ladder! Cannot add. "))}
			} else { m.channel.send(util.redx_embed(user_mention + " is already on the ladder! Cannot add. "))}
		} else { m.channel.send(util.error_format(this)); }
	} else { m.channel.send(util.error_format(this)); }
}

ladderaddnew.name = "ladderaddnew";
ladderaddnew.arg = "<@user> <ign>";
ladderaddnew.description = "adds discord user and IGN to ladder with default elo and mmr";
ladderaddnew.perms = "admin";
ladderaddnew.category = "ladder";
ladderaddnew.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;
var colors = require("colors");

var reopen = module.exports = {};

reopen.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		// only allow people to host in in-house channel
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if(g!=null){
				if(g.ih_open == false){
					// delete from lobbies
					db.delete_lobby(g);

					// set game variables
					g.ih_open = true;
					g.checked_names = false;
					g.made_teams = false;
					g.t1 = [];
					g.t2 = [];
					g.t1_count = 0;
					g.t2_count = 0;
					g.set_status();

					// resend lfg message
					if(config.misc.generate_lfg_ih_notice === "on"){
						var lfg_channel = ihs.client.channels.find("id",config.server.lfg_channel);
						if(lfg_channel!=null){
							lfg_channel.send({embed:{
								color: 3447003,
								author:{
									name: config.server.server_name  + " " + config.ih.region.toUpperCase() + " In-House Sign Ups Open!",
									icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
								},
								description: "\u200b\n\u200b**Looking for players __right now__ in <#" + config.server.ih_channel + ">!**\n\nCome sign up for **5v5 custom games** with members of " + config.server.server_name + ".:thumbsup:\nAll ranks welcome."
									// \nNOTE:**\n:diamond_shape_with_a_dot_inside: You must be at least level 30 and own 20 champions in order to participate in In-Houses as there are now 10 bans in Tournament Draft. \n:diamond_shape_with_a_dot_inside: Players may also have no choice but to play an off role. __Please be prepared for this__."
							}}).then(function(){
								ihs.set_tbd_mid("lfg_ih_notice",lfg_channel.lastMessageID);
							});
						}
					}
					
					if(config.misc.generate_team_vc === "on"){
						// delete team vc if exists
						if(g.t1_vc){
							g.t1_vc.delete();
						}
						if(g.t2_vc){
							g.t2_vc.delete();
						}

						ihs.vc[g.lobby] = false;
					}

					// send reopen notice
					m.channel.send(util.cm_embed("Reopened __" + ihs.cl + "__."));
					g.show_players(m);
				} else {m.channel.send(util.redx_embed("__" + ihs.cl + "__ is already open."))}
			}
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}	
}

reopen.name = "reopen";
reopen.arg = "";
reopen.description = "reopens in-house sign up sheet";
reopen.perms = "mod";
reopen.category = "in-house";
reopen.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var setelocap = module.exports = {};

setelocap.function = function(m,ihs){
	var arg = util.parse_arg(m.content, "setelocap");
	if(arg.length == 1){
		if(arg[0].toLowerCase() === "on" || arg[0].toLowerCase() === "off"){
			config.ih.elocap = arg[0].toLowerCase();
			m.channel.send(util.cm_embed("Elo cap is now turned **" + arg[0].toLowerCase() + "**."));
		} else { m.channel.send(util.redx_embed("Please set elocap to **on** or **off**.")); }
	}

	util.write_config_to_file(config);
}

setelocap.name = "setelocap";
setelocap.arg = "<on or off>";
setelocap.description = "sets elocap";
setelocap.perms = "admin";
setelocap.category = "settings";
setelocap.dm = false;
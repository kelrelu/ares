var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var sethost = module.exports = {};

sethost.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g != null ){
				var arg = util.parse_arg(m.content,"sethost");
				if(arg.length == 1){
					// check that user mention
					if(util.is_user_mention(arg[0])){
						// check that user exists in guild, get rid of fakes
						var new_host_member = m.guild.members.find("id", util.get_did(arg[0]));
						if(new_host_member){
							var new_host = ihs.client.users.find("id",util.get_did(arg[0]));

							if(util.has_role(new_host_member,config.roles["mod"])){	
								g.host = new_host;
								m.channel.send(util.cm_embed("Host for __" + g.lobby + "__ has been changed to " + new_host +"."));
							} else { m.channel.send(util.redx_embed( m.author + " Um......\n**You can't assign someone that isn't an in-house mod as the host of a game.**\n...\n...\n.......")) }
						
						} else { m.channel.send(util.redx_embed("**Invalid User**\nThis user isn't on this server :thinking:\n\n**" + config.bot.prefix + "sethost <@user>**")) }
					} else { m.channel.send(util.redx_embed("**Incorrect Format**\nArgument must be an @user, e.g. <@" + config.bot.owner + ">.\n\n**" + config.bot.prefix + "sethost <@user>**")) }
				}
			}
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

sethost.name = "sethost";
sethost.arg = "<@user>";
sethost.description = "sets host of current in-house game to specified in-house mod";
sethost.perms = "mod";
sethost.category = "in-house";
sethost.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var deleteteams = module.exports = {};

deleteteams.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g != null ){
				g.made_teams = false;
				g.t1 = [];
				g.t1_count = 0;

				g.t2 = [];
				g.t2_count = 0;

				m.channel.send(util.cm_embed("Team log has been deleted."));
			} 
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

deleteteams.name = "deleteteams";
deleteteams.arg = "";
deleteteams.description = "deletes teams for in-house lobby";
deleteteams.perms = "mod";
deleteteams.category = "in-house";

deleteteams.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var win = module.exports = {};

win.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g != null ){
				var args = util.parse_args(m.content,"win").map(function(t){return t.toLowerCase()});
				if(g.made_teams){
					var winner = args[0];
					if(winner === "team1" || winner === "t1"|| winner === "team2"|| winner === "t2"){
						if(g.t1.length === config.ih.max_team_size && g.t2.length === config.ih.max_team_size){
							// update database and add tags to top 3
							var all_players_valid = true;
							var all_players = g.t1.concat(g.t2);
							var invalid_player = "";

							for (var p = 0; p < all_players.length; p ++){
								if(!db.is_on_ladder_ign(all_players[p])) {
									all_players_valid = false;
									invalid_player = all_players[p];
								}
							}

							if(all_players_valid){
								g.update_win(winner,ihs);
								g.show_winner(m,winner);

								// change top 3 tags if applicable
								db.update_itp_tags(ihs);
								g.award_currency(winner,ihs);

								// delete from lobbies
								db.delete_lobby(g);
								//reset variables: empty lobby
								ihs.lobbies[ihs.cl] = null;
								if(config.misc.generate_team_vc === "on"){
									if(g.t1_vc){
										g.t1_vc.delete().catch(console.error);
									}

									if(g.t2_vc){
										g.t2_vc.delete().catch(console.error);
									}

									ihs.vc[g.lobby] = false;
								}

								
								ihs.set_tbd_mid("drafting",null);
								// store 
							} else { m.channel.send(util.redx_embed("**" + invalid_player + "** is not in the database!"));}
						} else {m.channel.send(util.redx_embed("Teams are not filled.\nPlease enter in all team members before submitting results with **" + config.bot.prefix + "win**."))}
					} else { m.channel.send(util.redx_embed("Please either enter in **team1** or **team2** as the winning team."));}
				} else { m.channel.send(util.redx_embed("Teams have not been made yet! Please complete drafting before submitting a win."))}
			} 
		} else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
	}
}

win.name = "win";
win.arg = "<team1 or team2>";
win.description = "records win";
win.perms = "mod";
win.category = "in-house";
win.dm = false;
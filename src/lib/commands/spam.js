var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var spam = module.exports = {};

spam.function = function(m,ihs){
    var spam = util.parse_arg(m.content,this.name);
    var spammables = new Map();
    spammables["fred"] = "<:thonking:307949512985411585>";

    var arg = util.parse_arg(m.content,"spam");
    var spamming_message = "";

    var sk = Object.keys(spammables);
    
    spamming_message += (spam + "\n").repeat(config.misc.max_spam - 1) + spam;
    
    m.channel.send(spamming_message);
}

spam.name = "spam";
spam.arg = "";
spam.description = "spams specified text 5 times";
spam.perms = "admin";
spam.category = "other";
spam.dm = false;
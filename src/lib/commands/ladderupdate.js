var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderupdate = module.exports = new Map();

ladderupdate.function = function(m,ihs){
	var args = util.parse_args(m.content, "ladderupdate");
	if(args.length == 2){
		args = [args[0]].concat(args[1].split(" ").filter(Boolean));
	}
	if(args.length == 5){
		var player_ref = args[0];
		args.splice(0,1);
		if(args.map(util.is_integer).reduce(function(a,b){ return a && b })){
			var elo = parseInt(args[0]);
			var mmr = parseInt(args[1]);
			var wins = parseInt(args[2]);
			var losses = parseInt(args[3]);

			if(util.is_user_mention(player_ref)){
				if(db.is_on_ladder_did(util.get_did(player_ref))){
					// update stats for player
					db.dbquery("update players set elo=" + elo + ",mmr=" + mmr + ",wins=" + wins + ",losses=" + losses + "where discord_id='" + db.dbify(util.get_did(player_ref)) + "';");
					m.channel.send(util.cm_embed("Successfully updated statistics for " + player_ref + " on the ladder."));
				} else { m.channel.send(util.redx_embed(player_ref + " is not on the ladder. Cannot update.")); }
			} else {
				if(db.is_on_ladder_ign(player_ref)){
					// update stats for player
					db.dbquery("update players set elo=" + elo + ",mmr=" + mmr + ",wins=" + wins + ",losses=" + losses + "where ign='" + db.dbify(player_ref) + "';");
					m.channel.send(util.cm_embed("Successfully updated statistics for **" + player_ref + "** on the ladder."));
				} else { m.channel.send(util.redx_embed("**" + player_ref + "** is not on the ladder. Cannot update.")); }
			}
		} else { m.channel.send(util.error_format(this)); }
	} else { m.channel.send(util.error_format(this)); }
}

ladderupdate.name = "ladderupdate";
ladderupdate.arg = "<@user or ign> <elo> <mmr> <wins> <losses>";
ladderupdate.description = "updates existing player on the ladder with new stats";
ladderupdate.perms = "admin";
ladderupdate.category = "ladder";
ladderupdate.dm = false;
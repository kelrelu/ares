var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var post = module.exports = {};

post.function = function(m,ihs){
    var args = util.parse_args(m.content,"post");
    if(args.length >= 2){
        if(util.is_channel(args[0])){
            var channel_id = util.get_cid(args[0]);
            args.splice(0,1);

            var post_message = args.join(" ");
            var post_channel = ihs.client.channels.find("id",channel_id);
            if(post_channel){
                post_channel.send(post_message).then().catch(console.error);
            }
        } else { m.channel.send(util.redx_embed("First argument must be a channel.\n Post a message by using **.post <channel> <msg - in quotes>**."));}
    } else { m.channel.send(util.redx_embed("Incorrectly formatted. Remember to use quotes around your message\ne.g. **.post #general \"hi\"**")); }
}

post.name = "post";
post.arg = "";
post.description = "shows all current post";
post.perms = "admin";
post.category = "secret";
post.dm = true;
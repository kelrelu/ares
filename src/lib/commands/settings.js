var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var settings = module.exports = {};

settings.function = function(m,ihs){
	m.channel.send({embed:{
		author: {
			name: "Settings",
			icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
		},
		fields: [{
			name: "__Draft Mode__",
			value: "**" + config.ih.draftmode + "**"
		},{
			name: "__Elo Cap__",
			value: "**" + config.ih.elocap + "**"
		}]
	}});
}

settings.name = "settings";
settings.arg = "";
settings.description = "shows all current settings";
settings.perms = "mod";
settings.category = "settings";
settings.dm = true;
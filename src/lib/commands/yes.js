var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var yes = module.exports = {};

yes.function = function(m,ihs){
	if(ihs.ur["cancel"]){
		if(ihs.ur["cancel"]["message"].author == m.author){
			var cancelled_lobby = ihs.cl;
			var g = ihs.get_lobby(ihs.cl);
			ihs.lobbies[ihs.cl] = null;

			// set current to new lobby
			var other_ag = Object.keys(ihs.lobbies).filter(function(l){return ihs.lobbies[l]!=null})
			if(other_ag.length > 0){
				ihs.cl = other_ag[0]
			} else {
				ihs.cl = null;
			}

			if(ihs.tbd_mids["cancel_confirmation"]){
				var cc_message = m.channel.messages.find("id",ihs.tbd_mids["cancel_confirmation"]);
				if(cc_message){
					cc_message.delete().catch(console.error);
				}
			}
			// reset 
			clearTimeout(ihs.ur["cancel"]["timeout"]);
			ihs.ur["cancel"] = null;

			// delete lfg annoucenment
			if(config.misc.generate_lfg_ih_notice){
				if(ihs.tbd_mids["lfg_ih_notice"]){
					var lfg_channel = ihs.client.channels.find("id",config.server.lfg_channel);
					if(lfg_channel!=null){
						var lfg_ih_notice = lfg_channel.messages.find("id",ihs.tbd_mids["lfg_ih_notice"]);
						if(lfg_ih_notice){
							lfg_ih_notice.delete().catch(console.error);
						}
					}
				}
			}

			// delete cancel question message
			if(ihs.tbd_mids["cancel"]){
				var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
				if(ih_channel!=null){
					var cancel_message = ih_channel.messages.find("id",ihs.tbd_mids["cancel"]);
					if(cancel_message){
						cancel_message.delete().catch(console.error)
					}
				}
			}
			
			// delete vc if applicable
			if(config.misc.generate_team_vc){
				// delete team vc if exists
				if(g.t1_vc){
					g.t1_vc.delete();
				}
				if(g.t2_vc){
					g.t2_vc.delete();
				}

				ihs.vc[g.lobby] = false;
			}

			// delete from lobbies
			db.delete_lobby(g);
			
			// send message confirming cancel
			if(!ihs.cl){
				m.channel.send({embed:{
					color:0xff050c,
					author:{
						name: g.lobby,
						icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					title:":o: Canceled.",
					footer: {
						icon_url: g.host.avatarURL(),
						text: "Host : " + g.host.username
					}
				}});
			} else {
				m.channel.send({embed:{
					color:0xff050c,
					author:{
						name: g.lobby,
						icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					title:":o: Canceled.",
					description: " Current lobby has been changed to __" + ihs.cl + "__.",
					footer: {
						icon_url: g.host.avatarURL(),
						text: "Host : " + g.host.username
					}
				}});
			}
		}
	} else if(ihs.ur["ladderclear"]){
		// delete ladderclear question message
		if(ihs.ur["ladderclear"]["message"].author == m.author){
			// reset
			clearTimeout(ihs.ur["ladderclear"]["timeout"]);
			ihs.ur["ladderclear"] = null;

			if(ihs.tbd_mids["ladderclear"]){
				var ladderclear_message = m.channel.messages.find("id",ihs.tbd_mids["ladderclear"]);
				if(ladderclear_message){
					ladderclear_message.delete().catch(console.error);
				}
			}

			// clear database
			db.clear_all();

			// send message confirming clear
			m.channel.send(util.cm_embed("All ladder data has been cleared."));
		}

	}
}

yes.name = "yes";
yes.arg = "";
yes.description = "handles yes responses to appropriate bot questions";
yes.perms = "all";
yes.category = "secret";
yes.dm = false;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var players = module.exports = {};

players.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g!=null ){
				g.show_players(m);
			}
		} else {
			// delete the message
			m.delete().then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(m.author, util.redx_embed("The command ``" + config.bot.prefix + "players`` may only be used in the inhouse channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete(config.delete_time_wait_seconds * 1000).then().catch(console.error)
			})
		}
	}
}

players.name = "players";
players.arg = "";
players.description = "shows all signed-up players";
players.perms = "all";
players.category = "in-house";
players.dm = false;
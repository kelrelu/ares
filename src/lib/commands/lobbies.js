var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var lobbies = module.exports = {};

lobbies.function = function(m,ihs){
	var lobby_summaries = Object.keys(ihs.lobbies).map(function(l){
		if(ihs.lobbies[l]!= null){
			if(ihs.cl === l){
				return {
					name: "__"+ l + "__ (Current)",
					value: "*Active*\nStatus : " + ihs.lobbies[l].status + "\nHost : " + ihs.lobbies[l].host,
					inline:true
				}
			} else {
				return {
					name: "__" + l + "__",
					value: "*Active*\nStatus : " + ihs.lobbies[l].status + "\nHost : " + ihs.lobbies[l].host,
					inline:true
				}	
			}
			
		} else {
			return {
				name: "__" + l + "__",
				value: "*Inactive*",
				inline:true
			}
		}
	});
	m.channel.send({ embed:{
		color:0xff050c,
		author:{
			name: config.ih.name + " Lobbies",
			icon_url:ihs.client.guilds.find("id",config.server.server_id).iconURL()
		},
		fields: lobby_summaries
	}}).then().catch(console.error);
}

lobbies.name = "lobbies";
lobbies.arg = "";
lobbies.description = "shows all lobbies and current status of each lobby";
lobbies.perms = "all";
lobbies.category = "in-house";
lobbies.dm = true;
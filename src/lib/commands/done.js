var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var LolApi = require("../../lolapi/lol_api.js");
var lol = new LolApi(config.api.lol);
var lol_ranks = util.lol_ranks;

var done = module.exports = {};

Map.prototype.deep_copy = function(map){
	var keys = Object.keys(map);
	for(k in keys){
		this[keys[k]] = map[keys[k]];
	}
}

done.function = function(m,ihs){
	if(ihs.lol_verification[m.author.id]!= null){

		var svi = new Map();
		svi.deep_copy(ihs.lol_verification[m.author.id]);

		lol.get_summoner_by_name(svi.api_region,encodeURIComponent(svi.summoner_name)).then(function(summoner){
			var profile_icon_id = summoner.profileIconId;

			if(profile_icon_id == svi.piid){

				lol.get_rank_by_summoner_id(svi.api_region,svi.summoner_id).then(function(data){
					var guild = ihs.client.guilds.find("id",config.server.server_id);
					var member = guild.members.find("id",m.author.id);

					// if member has left server
					if(member){
						var soloq = data.filter(function(t){return t.queueType === "RANKED_SOLO_5x5" });
						var flexq = data.filter(function(t){return t.queueType === "RANKED_FLEX_SR" });

						// if unranked don't assign tag
						if(soloq.length == 0){

							m.channel.send({embed:{
								author: {
									name: "Could Not Assign Rank Tag - Unranked",
									icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
								},
								description: "Unfortunately, this account is currently unranked, and cannot be used to verify a rank tag.\n\nTry verifying again for a rank tag once when you have a rank! :thumbsup:",
								timestamp : new Date()
							}});

						// if ranked, insert them into the players database, remove previous rank tags if they are lower
						} else {

							var rank = util.first_uppercase(soloq[0].tier);
							var rank_role = guild.roles.find("name",rank);

							if(rank != "Challenger"){
								if(rank_role){
									if(member.roles.exists("name", rank) == false){
										member.addRole(rank_role).then(function(member){
											// remove old rank tags
											var mr = member.roles.array();

											for( var m = 0; m < mr.length; m++ ){
												if(mr[m].name != rank){
													if(lol_ranks.includes( mr[m].name )) {
														member.removeRole( mr[m] );
													}
												}
											}
										});
									}
								}

								db.dbquery("insert into summoners(ign,id,rank,division) values('" + db.dbify(summoner.name) + "'," + summoner.id + ",'" + db.dbify(rank) + "','" + db.dbify(division) + "') on conflict do nothing;");
								
								var rinfo = util.get_rank_info(soloq[0].tier, soloq[0].rank);
								var cp = db.dbquery("select * from players where discord_id='" + summoner.id + "';");
								if(cp){

									// check if accounts are the same, if they are, update with new rank
									if(cp.summoner_id == summoner.id){
										db.dbquery("update players set soloq_rank='" + rinfo[0] + "', soloq_rank_points=" + rinfo[1] + " where discord_id='" + cp.discord_id + "');")
									// if accounts are different, use the ign with the higher rank
									} else {
										if(cp.soloq_rank_points < rinfo[1]){
											db.dbquery("update players set summoner_id='" + summoner.id + "',soloq_rank='" + rinfo[0] + "', soloq_rank_points=" + rinfo[1] + " where discord_id='" + cp.discord_id + "');")
										}
									}

								} else {
									var utc_epoch = new Date().getTime()/1000;

									db.dbquery("insert into players(discord_id,ign,elo,mmr,wins,losses,last_played,summoner_id,soloq_rank,soloq_rank_points) values("
									+ "'" + m.author.id + "',"
									+ "'" + db.dbify(summoner.name) + "',"
									+ config.ih.default_elo + ","
									+ config.ih.default_mmr + ","
									+ "0,"
									+ "0,"
									+ "to_timestamp(" + utc_epoch + "),"
									+ summoner.id + ","
									+ db.dbify(rinfo[0]) + ","
									+ rinfo[1]
									+ ");");
								}

								m.channel.send({embed:{
									author: {
										name: "Verified!",
										icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
									},
									description: "You now have the following role: ```\n" + rank + "```",
									timestamp : new Date()
								}}).then(function(){
									console.log("Verified " + m.author.tag + "(" + svi.summoner_name + ") with rank " + rank + ".");
								}).catch(function(){
									console.log("Couldn't send verified message to " + m.author.tag);
									console.error;
								});

							} else {
								m.channel.send({embed:{
									author: {
										name: "Could Not Automatically Assign Tag - Challenger Account!!",
										icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
									},
									description: " It seems that you are Challenger.:astonished:\n\nPlease message an admin to verify this rank!\nChallenger accounts can't be automatically verified through me."
								}}).catch(function(){
									console.log("Couldn't send challenger verified message to " + m.author.tag);
									console.error;
								});
							}
						}
					}


				}).catch(function(error){
					util.handle_api_error(m,ihs.client,error);
				});

			} else {
				m.author.send({embed:{
					author: {
						name: "Summoner Icons Don't Match",
						icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: m.author + ", summoner icons don't match for me for some reason.\n\nIf you already changed it, you might need to __wait 10 seconds for League to receive the icon change__.\n\nAfter waiting, type ```.done```",
					timestamp : new Date()
				}}).catch(function(){
					console.log("Couldn't send unsuccessful verification message to " + m.author.tag);
				});
				
			}
		}).catch(function(error){
			util.handle_api_error(m,ihs.client,error);
		});
	}
}


done.name = "done";
done.arg = "";
done.description = "confirms rank verification, assigns rank and region tags";
done.perms = "all";
done.category = "secret";
done.dm = true;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var team1 = module.exports = {};

team1.function = function(m,ihs){
	var args = util.parse_arg(m.content,"t1");
	if(args.length > 0){
		var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
		if(ih_channel != null){
			if(m.channel == ih_channel){
				var g = ihs.get_lobby(ihs.cl);
				if( g != null ){
					if(g.made_teams){

						if(!(g.is_on_team(args[0]))){
							if(g.t1.length < config.ih.max_team_size){
								// check that they are in the database
								if(db.exists_ign(args[0])){
									g.t1.push(args[0]);
									g.t1_count += 1;
									g.set_status();
										// add delete ms
									g.show_teams(m,ihs);
								} else { m.channel.send(util.redx_embed("**" + args[0] + "** is not in the database.")); }

							} else { m.channel.send(util.redx_embed("Cannot add player to Team 1. Team 1 is full!")); }
						} else { m.channel.send(util.redx_embed("**" + args[0] + "** is already on a team! \n")); }
					} else { m.channel.send(util.redx_embed("Please **" + config.bot.prefix + "maketeams** before assigning a player to a team."))}
				} 
			} else {
				// delete the message
				m.delete({timeout: config.bot.delete_wait_time_seconds * 1000 * 2}).then().catch(console.error);
				// send a notice to the user about posting commands in the right channel
				m.channel.send(m.author, util.redx_embed("The command ``" + config.bot.prefix + "team1`` only works in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
					e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000 * 2}).then().catch(console.error)
				})
			}
		}
	}
}

team1.name = "t1";
team1.arg = "<ign>";
team1.description = "adds player to team1";
team1.perms = "mod";
team1.category = "secret";
team1.dm = false;
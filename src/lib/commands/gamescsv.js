var fs = require("fs");
var db = require("../db.js");
var util = require("../util.js");
var config = util.config;

var gamescsv = module.exports = new Map();

gamescsv.function = function(m,ihs){
	var filepath = __dirname + "/../../files/Games_" + config.server.server_name + "_S" + config.ih.season + ".csv";
	
	var games = db.dbquery("select * from games order by game_date desc;");
	var csv_string = "t1p1,t1p2,t1p3,t1p4,t1p5,t2p1,t2p2,t2p3,t2p4,t2p5,winner,game_date,game_id,host\r\n";
	csv_string += util.convert_to_csv(games);
	fs.writeFileSync(filepath,csv_string, 'utf16le', { flag: 'w' });

	m.channel.send({
		files: [filepath]
	});
}
gamescsv.name = "gamescsv";
gamescsv.arg = "";
gamescsv.description = "creates downloadable spreadsheet of all games played";
gamescsv.perms = "mod";
gamescsv.category = "info";
gamescsv.dm = true;

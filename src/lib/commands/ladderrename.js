var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var ladderrename = module.exports = new Map();

ladderrename.function = function(m,ihs){
	var args = util.parse_args(m.content, this.name);
	if(args.length == 2){
		if(util.is_user_mention(args[0])){
			// check that player id is on ladder
			if(db.is_on_ladder_did(util.get_did(args[0]))){
				// check that new IGN is not on ladder
				if(!db.is_on_ladder_ign(args[1])){
					var old_ign = db.dbquery("select * from players where discord_id='" + util.get_did(args[0]) + "';")[0].ign;
					// perform update
					db.dbquery("insert into summoners(ign) values ('" + db.dbify(args[1]) + "') on conflict(ign) do update set ign='" + db.dbify(args[1]) + "';");
					db.dbquery("update players set ign='" + db.dbify(args[1]) + "' where discord_id='" + util.get_did(args[0]) + "';");
					// update games, replacing all instances of old IGN with new IGN
					db.update_ign_games(old_ign, args[1]);
					m.channel.send(util.cm_embed("Successfully renamed " + args[0] + " as **" + args[1] + "** on the ladder."));
				} else { m.channel.send(util.redx_embed("**" + args[1] + "** is already on the ladder. Cannot rename. "))}
			} else { m.channel.send(util.redx_embed("**" + args[0] + "** is not on the ladder. Cannot rename."))}
		} else {
			// check that player IGN is on the ladder
			if(db.is_on_ladder_ign(args[0])){
				// check that new IGN is not on ladder
				if(!db.is_on_ladder_ign(args[1])){
					// perform update
					db.dbquery("insert into summoners(ign) values ('" + db.dbify(args[1]) + "') on conflict(ign) do update set ign='" + db.dbify(args[1]) + "';");
					db.dbquery("update players set ign='" + db.dbify(args[1]) + "' where ign='" + db.dbify(args[0]) + "';");
					// update games, replacing all instances of old IGN with new IGN
					db.update_ign_games(args[0], args[1]);
					m.channel.send(util.cm_embed("Successfully renamed **" + args[0] + "** as **" + args[1] + "** on the ladder."));
				} else { m.channel.send(util.redx_embed("**" + args[1] + "** is already on the ladder. Cannot rename. "))}
			} else { m.channel.send(util.redx_embed("**" + args[0] + "** is not on the ladder. Cannot rename."))}
		}
	} else { m.channel.send(util.error_format(this)); }
}

ladderrename.name = "ladderrename";
ladderrename.arg = "<@old user or old ign> <new ign>";
ladderrename.description = "renames player on the ladder";
ladderrename.perms = "admin";
ladderrename.category = "ladder";
ladderrename.dm = false;
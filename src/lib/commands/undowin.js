var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var undowin = module.exports = {};

undowin.function = function(m,ihs){
    var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
        if(m.channel == ih_channel){
            if(ihs.last_game){
                ihs.undo_win();
                m.channel.send(util.cm_embed("Undid the last win and returned you to the saved team log."));
            } else { m.channel.send(util.redx_embed("Cannot revert win. No past win data recorded!\n*Note: Bot can only undo up to one win.*"))}
        } else {
			// delete the message
			m.delete({timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(util.redx_embed("The command **" + config.bot.prefix + this.name + "** may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete({ timeout: config.bot.delete_wait_time_seconds * 1000}).then().catch(console.error)
			});
		}
    }
}

undowin.name = "undowin";
undowin.arg = "";
undowin.description = "undo last win, return you to saved team log";
undowin.perms = "mod";
undowin.category = "in-house";
undowin.dm = false;
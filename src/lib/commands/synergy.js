var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var synergy = module.exports = {};

synergy.function = function(m,ihs){
    var args = util.parse_args(m.content,"synergy");
    if(args.length == 2){
        var players = db.dbquery("select * from players;");
        var players_ids = players.map(function(t){return t.discord_id;});
        var players_igns = players.map(function(t){return t.ign;});
        var games = db.dbquery("select * from games;");
        var user1 = args[0]; var user2 = args[1];
        var valid_user1 = false; var valid_user2 = false;

        var p1_ign = ""; var p2_ign = "";
        var p1_id = ""; var p2_id = "";

        if(util.is_user_mention(user1)){
            if(players_ids.includes(util.get_did(user1))) {
                var p1_info = db.dbquery("select * from players where discord_id='" + util.get_did(user1) + "';")[0]
                p1_ign = p1_info.ign;
                p1_id = p1_info.discord_id;
                
                valid_user1 = true;
            } else {
                valid_user1 = false;
            }
        } else {
            if(players_igns.includes(user1.toUpperCase())){
                p1_ign = user1.toUpperCase();
                var p1_info = db.dbquery("select * from players where ign='" + db.dbify(p1_ign) + "';")[0]
                p1_id = p1_info.discord_id;

                valid_user1 = true;
            } else {
                valid_user1 = false;
            }
        }

        if(util.is_user_mention(user2)){
            if(players_ids.includes(util.get_did(user2))) {
                
                var p2_info = db.dbquery("select * from players where discord_id='" + util.get_did(user2) + "';")[0]
                p2_ign = p2_info.ign;
                p2_id = p2_info.discord_id;

                valid_user2 = true;
            } else {
                valid_user2 = false;
            }
        } else {
            if(players_igns.includes(user2.toUpperCase())){
                p2_ign = user2.toUpperCase();
                var p2_info = db.dbquery("select * from players where ign='" + db.dbify(p2_ign) + "';")[0]
                p2_id = p2_info.discord_id;

                valid_user2 = true;
            } else {
                valid_user2 = false;
            }
        }
        if(valid_user1 && valid_user2){
            // do database queries
            var shared_games_t1 = db.dbquery("(select * from games where t1p1='" + db.dbify(p1_ign) + "' or t1p2='" + db.dbify(p1_ign) + "' or t1p3='" + db.dbify(p1_ign) + "' or t1p4='" + db.dbify(p1_ign) + "' or t1p5='" + db.dbify(p1_ign) +"') intersect (select * from games where t1p1='" + db.dbify(p2_ign) + "' or t1p2='" + db.dbify(p2_ign) + "' or t1p3='" + db.dbify(p2_ign) + "' or t1p4 = '" + db.dbify(p2_ign) + "' or t1p5 = '" + db.dbify(p2_ign) + "');");
            var shared_games_t1_won = db.dbquery("(select * from games where winner = 1 and (t1p1='" + db.dbify(p1_ign) + "' or t1p2='" + db.dbify(p1_ign) + "' or t1p3='" + db.dbify(p1_ign) + "' or t1p4='" + db.dbify(p1_ign) + "' or t1p5='" + db.dbify(p1_ign) +"')) intersect (select * from games where winner=1 and (t1p1='" + db.dbify(p2_ign) + "' or t1p2='" + db.dbify(p2_ign) + "' or t1p3='" + db.dbify(p2_ign) + "' or t1p4 = '" + db.dbify(p2_ign) + "' or t1p5 = '" + db.dbify(p2_ign) + "'));");
            
            var shared_games_t2 = db.dbquery("(select * from games where t2p1='" + db.dbify(p1_ign) + "' or t2p2='" + db.dbify(p1_ign) + "' or t2p3='" + db.dbify(p1_ign) + "' or t2p4='" + db.dbify(p1_ign) + "' or t2p5='" + db.dbify(p1_ign)+"') intersect (select * from games where t2p1='" + db.dbify(p2_ign) + "' or t2p2='" + db.dbify(p2_ign) + "' or t2p3='" + db.dbify(p2_ign) + "' or t2p4 = '" + db.dbify(p2_ign) + "' or t2p5 = '" + db.dbify(p2_ign) + "');");
            var shared_games_t2_won = db.dbquery("(select * from games where winner = 2 and (t2p1='" + db.dbify(p1_ign) + "' or t2p2='" + db.dbify(p1_ign) + "' or t2p3='" + db.dbify(p1_ign) + "' or t2p4='" + db.dbify(p1_ign) + "' or t2p5='" + db.dbify(p1_ign)+"')) intersect (select * from games where winner=2 and (t2p1='" + db.dbify(p2_ign) + "' or t2p2='" + db.dbify(p2_ign) + "' or t2p3='" + db.dbify(p2_ign) + "' or t2p4 = '" + db.dbify(p2_ign) + "' or t2p5 = '" + db.dbify(p2_ign) + "'));");

            var shared_games = shared_games_t1.length + shared_games_t2.length;
            var shared_games_won = shared_games_t1_won.length + shared_games_t2_won.length;
            var shared_games_lost = shared_games - shared_games_won;

            var enemy_games_12 = db.dbquery("(select * from games where t1p1='" + db.dbify(p1_ign) + "' or t1p2='" + db.dbify(p1_ign) + "' or t1p3='" + db.dbify(p1_ign) + "' or t1p4='" + db.dbify(p1_ign) + "' or t1p5='" + db.dbify(p1_ign)+"') intersect (select * from games where t2p1='" + db.dbify(p2_ign) + "' or t2p2='" + db.dbify(p2_ign) + "' or t2p3='" + db.dbify(p2_ign) + "' or t2p4 = '" + db.dbify(p2_ign) + "' or t2p5 = '" + db.dbify(p2_ign) + "');");
            var enemy_games_12_1_win = db.dbquery("(select * from games where winner=1 and (t1p1='" + db.dbify(p1_ign) + "' or t1p2='" + db.dbify(p1_ign) + "' or t1p3='" + db.dbify(p1_ign) + "' or t1p4='" + db.dbify(p1_ign) + "' or t1p5='" + db.dbify(p1_ign)+"')) intersect (select * from games where winner=1 and (t2p1='" + db.dbify(p2_ign) + "' or t2p2='" + db.dbify(p2_ign) + "' or t2p3='" + db.dbify(p2_ign) + "' or t2p4 = '" + db.dbify(p2_ign) + "' or t2p5 = '" + db.dbify(p2_ign) + "'));");

            var enemy_games_21 = db.dbquery("(select * from games where t1p1='" + db.dbify(p2_ign) + "' or t1p2='" + db.dbify(p2_ign) + "' or t1p3='" + db.dbify(p2_ign) + "' or t1p4='" + db.dbify(p2_ign) + "' or t1p5='" + db.dbify(p2_ign)+"') intersect (select * from games where t2p1='" + db.dbify(p1_ign) + "' or t2p2='" + db.dbify(p1_ign) + "' or t2p3='" + db.dbify(p1_ign) + "' or t2p4 = '" + db.dbify(p1_ign) + "' or t2p5 = '" + db.dbify(p1_ign) + "');");
            var enemy_games_21_1_win = db.dbquery("(select * from games where winner=2 and (t1p1='" + db.dbify(p2_ign) + "' or t1p2='" + db.dbify(p2_ign) + "' or t1p3='" + db.dbify(p2_ign) + "' or t1p4='" + db.dbify(p2_ign) + "' or t1p5='" + db.dbify(p2_ign)+"')) intersect (select * from games where winner=2 and (t2p1='" + db.dbify(p1_ign) + "' or t2p2='" + db.dbify(p1_ign) + "' or t2p3='" + db.dbify(p1_ign) + "' or t2p4 = '" + db.dbify(p1_ign) + "' or t2p5 = '" + db.dbify(p1_ign) + "'));");

            var enemy_games = enemy_games_12.length + enemy_games_21.length;
            var p1_wins = enemy_games_12_1_win.length + enemy_games_21_1_win.length;
            var p2_wins = enemy_games - p1_wins;

            // send message
            m.channel.send({embed:{
                author : {
                    name : "Synergy Report",
                    icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
                },
                fields:[{
                    name: "__Players__",
                    value: "<@" + p1_id + "> (" + p1_ign + ")\n<@" + p2_id + ">(" + p2_ign + ")",
                    inline:false 
                },{
                    name: ":couplekiss:__Games Together : " + shared_games + "__",
                    value: "Wins: " + shared_games_won + " (" + String(shared_games == 0 ? 0 :(Math.round(shared_games_won * 10000/shared_games)/100)) + "%)\nLosses: " + shared_games_lost + " (" + String(shared_games == 0 ? 0 :(Math.round(shared_games_lost * 10000/shared_games)/100)) + "%)",
                    inline:false
                },{
                    name: ":crossed_swords:__Games Against : " + enemy_games + "__",
                    value:"<@" + p1_id + "> Wins : " + p1_wins + " ("  + String(enemy_games == 0 ? 0 :(Math.round(p1_wins * 10000/enemy_games)/100)) + "%)\n<@" + p2_id + "> Wins : " + p2_wins + " ("  + String(enemy_games == 0 ? 0 :(Math.round(p2_wins * 10000/enemy_games)/100)) + "%)",
                    inline:false
                }],
                timestamp: new Date()
            }});
        } else {m.channel.send(util.redx_embed("**User Not On Ladder**\nMake sure both users are on the ladder before testing synergy."));}
    } else {m.channel.send(util.error_format(this));}
}

synergy.name = "synergy";
synergy.arg = "<@user or ign> <@user or ign>";
synergy.description = "shows synergy(games together, games against, win/loss percentage) for two in-house players";
synergy.perms = "all";
synergy.category = "info";

synergy.dm = true;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var hoststats = module.exports = {};

hoststats.function = function(m,ihs){
	var hoststats_fields = [];
	var host_counts_info = db.dbquery("select host from games;");
	var host_counts = {};
	for(var i =0; i < host_counts_info.length;i ++){
		var count = host_counts_info[i].host;
		host_counts[count] = host_counts[count] ? host_counts[count] + 1: 1;
	}

	var hosts_users = ihs.client.guilds.find("id", config.server.server_id).members.filter(function(t){return Object.keys(host_counts).includes(t.user.id);}).array();
	for(var i =0; i < hosts_users.length; i ++){
		if(util.has_role(hosts_users[i],config.roles.mod)) {
			hoststats_fields.push({
				name: "__" + hosts_users[i].user.username + "__",
				value: host_counts[hosts_users[i].user.id],
				inline:true
			});
		}
	}

	hoststats_fields.sort(function(a,b){
		return b.value - a.value;
	});

	m.channel.send({embed:{
		author: {
			name: "In-House Mod Hosting Counts",
			icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
		},
		fields: hoststats_fields
	}});
}

hoststats.name = "hoststats";
hoststats.arg = "";
hoststats.description = "shows host counts for current in-house mods";
hoststats.perms = "admin";
hoststats.category = "info";
hoststats.dm = true;
var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var checknames = module.exports = {};

checknames.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
	if(ih_channel != null){
		if(m.channel == ih_channel){
			var g = ihs.get_lobby(ihs.cl);
			if( g!=null ){
				g.checked_names = true;
				g.check_names(m,ihs);
			} 
		} else {
			// checknames the message
			m.delete().then().catch(console.error);
			// send a notice to the user about posting commands in the right channel
			m.channel.send(m.author, util.redx_embed("The command ``" + config.bot.prefix + "delete`` may only be used in the in-house channel <#" + config.server.ih_channel + ">.")).then(function(e_msg){
				e_msg.delete(config.delete_time_wait_seconds * 1000).then().catch(console.error)
			});
		}
	}
}

checknames.name = "checknames";
checknames.arg = "";
checknames.description = "checks signed up players (IGN and user)";
checknames.perms = "mod";
checknames.category = "in-house";
checknames.dm = false;

//gives report on signed up players - shows new players, changed igns, changed discord accounts, and players who are trying to confuse the system by signing up as other players
var fs = require("fs");
var db = require("../db.js");
var util = require("../util.js");
var config = util.config;

var laddercsv = module.exports = new Map();

laddercsv.function = function(m,ihs){
	var filepath = __dirname + "/../../files/Ladder_" + config.server.server_name + "_S" + config.ih.season + ".csv";

	var ranked_players = db.dbquery("select ign,elo,mmr,wins,losses,last_played from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + " order by elo desc,mmr desc,(wins - losses) desc,ign asc;");
	var csv_string = "ign,elo,mmr,wins,losses,last_played\r\n";
	csv_string += "RANKED\r\n";
	csv_string += util.convert_to_csv(ranked_players);

	var unranked_players = db.dbquery("select ign,elo,mmr,wins,losses,last_played from players p where (p.wins + p.losses) <" + String(config.ih.num_placement_matches) + " order by elo desc,mmr desc,(wins - losses) desc,ign asc;");
	csv_string += "\r\n\r\n\r\nUNRANKED\r\n";
	csv_string += util.convert_to_csv(unranked_players);
	fs.writeFileSync(filepath,csv_string, 'utf16le', { flag: 'w' }, function(err){if (err) throw err;});

	m.channel.send({
		files: [filepath]
	});
}

laddercsv.name = "laddercsv";
laddercsv.arg = "";
laddercsv.description = "creates downloadable spreadsheet of full ladder";
laddercsv.perms = "all";
laddercsv.category = "info";
laddercsv.dm = true;


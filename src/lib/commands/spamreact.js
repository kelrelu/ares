var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var spamreact = module.exports = {};

spamreact.function = function(m,ihs){
    var arg = util.parse_arg(m.content,this.name);
    var spam = arg[0];

    var messages = m.channel.messages.sort(function(a,b){
        alc = a.createdTimestamp;
        blc = b.createdTimestamp;
        return alc < blc ? 1 : alc > blc ? -1 : 0;
    }).array().filter(function(t){return t.content.includes(config.bot.prefix +'spamreact') == false});

    for(var i=0;i < Math.min(messages.length,config.misc.max_spam); i ++){ 
        messages[i].react(util.get_emoji(spam,ihs)).then().catch(console.error);
    }
    m.delete().then().catch(console.error);
}

spamreact.name = "spamreact";
spamreact.arg = "";
spamreact.description = "reacts to last 5 messages in channel with specified emoji";
spamreact.perms = "admin";
spamreact.category = "other";
spamreact.dm = false;
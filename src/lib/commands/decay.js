var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var decay = module.exports = {};

function convert_date_to_UTC(date) { 
	return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()); 
}

decay.function = function(m,ihs){
	var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
    if(ih_channel!=null){
        var top_players = db.dbquery("select * from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + "order by p.elo desc,p.mmr desc, p.wins desc,p.losses asc,p.ign asc limit " + String(config.ih.max_leaderboard) + ";");
        var decaying_players = new Map();

        // decay buffer amounts
        var decay_lb_buffer_ms = config.ih.decay_lb_buffer_days * 24 * 60 * 60 * 1000;
        var decay_halflb_buffer_ms = config.ih.decay_halflb_buffer_days * 24 * 60 * 60 * 1000;
        var decay_itp_buffer_ms = config.ih.decay_itp_buffer_days * 24 * 60 * 60 * 1000;

        var decay_lb_warning_ms_lower = (config.ih.decay_lb_buffer_days - config.ih.decay_warning_before_days) * 24 * 60 * 60 * 1000;
        var decay_lb_warning_ms_upper = (config.ih.decay_lb_buffer_days - config.ih.decay_warning_before_days + 1) * 24 * 60 * 60 * 1000;

        var decay_halflb_warning_ms_lower = (config.ih.decay_halflb_buffer_days - config.ih.decay_warning_before_days) * 24 * 60 * 60 * 1000;
        var decay_halflb_warning_ms_upper = (config.ih.decay_halflb_buffer_days - config.ih.decay_warning_before_days + 1) * 24 * 60 * 60 * 1000;

        var decay_itp_warning_ms_lower = (config.ih.decay_itp_buffer_days - config.ih.decay_warning_before_days) * 24 * 60 * 60 * 1000;
        var decay_itp_warning_ms_upper = (config.ih.decay_itp_buffer_days - config.ih.decay_warning_before_days + 1) * 24 * 60 * 60 * 1000;

        for(p in top_players){
            var player = top_players[p];
            var now_ms = convert_date_to_UTC(new Date()).getTime();
            var last_played_ms = new Date(String(player.last_played)).getTime();

            if(p < config.ih.max_top_players){
                if( ((now_ms - last_played_ms) > decay_itp_warning_ms_lower) && ((now_ms - last_played_ms) < decay_itp_warning_ms_upper) ) {
                    ihs.send_decay_warning(player.discord_id,config.ih.max_top_players);
                }
                // do decay for people who decay
                if((now_ms - last_played_ms) > decay_itp_buffer_ms){
                    // console.log("logged decaying for " + player.ign);
                    if(player.elo > config.ih.default_elo){
                        var decayed_elo = player.elo - (config.ih.decay_amount) - (0.05 * player.elo);
                        if(decayed_elo < config.ih.default_elo){ decayed_elo = config.ih.default_elo; }
                        decaying_players[player.ign] = "[" + player.elo + " -> " + Math.round(decayed_elo) + "] *Top 3 decay*";
                        db.dbquery("update players set elo=" + String(Math.round(decayed_elo)) + " where discord_id='" + player.discord_id + "';");
                    }
                }
            }
            else if(p < config.ih.max_leaderboard / 2){
                if( ((now_ms - last_played_ms) > decay_halflb_warning_ms_lower) && ((now_ms - last_played_ms) < decay_halflb_warning_ms_upper) ) {
                    ihs.send_decay_warning(player.discord_id, config.ih.max_leaderboard / 2);
                }
                // do decay for people who decay
                if((now_ms - last_played_ms) > decay_halflb_buffer_ms){
                    // console.log("logged decaying for " + player.ign);
                    if(player.elo > config.ih.default_elo){
                        var decayed_elo = player.elo - (config.ih.decay_amount) - (0.05 * player.elo);
                        if(decayed_elo < config.ih.default_elo){ decayed_elo = config.ih.default_elo; }
                        decaying_players[player.ign] = "[" + player.elo + " -> " + Math.round(decayed_elo) + "] *Top 20 decay*";
                        db.dbquery("update players set elo=" + String(Math.round(decayed_elo)) + " where discord_id='" + player.discord_id + "';");
                    }
                }
            }
            else {
                // message if about to decay
                if( ((now_ms - last_played_ms) > decay_lb_warning_ms_lower) && ((now_ms - last_played_ms) < decay_lb_warning_ms_upper) ) {
                    ihs.send_decay_warning(player.discord_id,40);
                }

                if((now_ms - last_played_ms) > decay_lb_buffer_ms){
                    // console.log("logged decaying for " + player.ign);
                    var decayed_elo = player.elo - (config.ih.decay_amount) - (0.05 * player.elo);
                    if(player.elo > config.ih.default_elo){
                        if(decayed_elo < config.ih.default_elo){ decayed_elo = config.ih.default_elo; }
                        decaying_players[player.ign] = "[" + player.elo + " -> " + Math.round(decayed_elo) + "]";
                        db.dbquery("update players set elo=" + String(Math.round(decayed_elo)) + " where discord_id='" + player.discord_id + "';");
                    }
                }
            }
        }

        var decaying_players_string = Object.keys(decaying_players).map(function(t){ return t + " " + decaying_players[t]; }).join("\n");
        ih_channel.send({embed:{
            title: "Decayed Players",
            description: decaying_players_string.length === 0 ? "*None*" : decaying_players_string,
            timestamp : new Date()
        }});
        decaying_players = [];
        db.update_itp_tags(ihs);
    }
}

decay.name = "decay";
decay.arg = "";
decay.description = "performs decay right now";
decay.perms = "owner";
decay.category = "secret";
decay.dm = true;
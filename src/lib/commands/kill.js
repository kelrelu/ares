var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var kill = module.exports = {};

kill.function = function(m,ihs){
    m.channel.send(util.cm_embed(" **Killing current instance of " + config.bot.username + "!**\n " + config.bot.username + " should be resurrected in several seconds :thumbsup: .")).then(function(){
        process.exit(0);
    });
}

kill.name = "kill";
kill.arg = "";
kill.description = "kills bot";
kill.perms = "owner";
kill.category = "secret";
kill.dm = false;
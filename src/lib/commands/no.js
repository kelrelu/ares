var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var no = module.exports = {};

no.function = function(m,ihs){
	if(ihs.ur["cancel"]){
		if(ihs.ur["cancel"]["message"].author == m.author){
			clearTimeout(ihs.ur["cancel"]["timeout"]);
			ihs.ur["cancel"] = null;
			
			// delete cancel question message
			if(ihs.tbd_mids["cancel"]){
				var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
				if(ih_channel!=null){
					ih_channel.messages.find("id",ihs.tbd_mids["cancel"]).delete().catch(console.error)
				}
			}

			m.channel.send(util.cm_embed(m.author + ", gotcha."));
		}
	} else if(ihs.ur["ladderclear"]){
		if(ihs.ur["ladderclear"]["message"].author == m.author){
			clearTimeout(ihs.ur["ladderclear"]["timeout"]);
			ihs.ur["ladderclear"] = null;
		}

		if(ihs.tbd_mids["ladderclear"]){
			var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
			if(ih_channel!=null){
				ih_channel.messages.find("id",ihs.tbd_mids["ladderclear"]).delete().catch(console.error)
			}
		}

		m.channel.send(util.cm_embed(m.author + ", gotcha."));
	}
}

no.name = "no";
no.arg = "";
no.description = "handles no responses to appropriate bot questions";
no.perms = "all";
no.category = "secret";
no.dm = false;
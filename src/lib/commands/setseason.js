var util = require("../util.js");
var db = require("../db.js");
var config = util.config;

var setseason = module.exports = {};

setseason.function = function(m,ihs){
	var arg = util.parse_arg(m.content, "setseason");
	config.ih.season = arg[0];
	m.channel.send(util.cm_embed("Set season to **" + arg[0] + "**."));

	util.write_config_to_file(config);
}

setseason.name = "setseason";
setseason.arg = "<season>";
setseason.description = "sets season";
setseason.perms = "admin";
setseason.category = "settings";
setseason.dm = false;
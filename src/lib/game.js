var colors = require("colors");

var util = require("./util.js");
var config = util.config;
var db = require("./db.js");
var LolApi = require("../lolapi/lol_api.js");
var lol = new LolApi(config.api.lol);
var regions = util.regions;
var mranks = util.mranks;

Map.prototype.copy = function(map){
	var keys = Object.keys(map);
	for(var k=0;k < keys.length; k++){
		this[keys[k]] = map[keys[k]];
	}
}

/* game object, created with "host" command */
class Game {
	constructor(host,lobby){
		// change to generator
		this.id = Math.floor(Math.random() * 1000000000);
		this.host = host;
		this.lobby = lobby;
		this.timestamp = new Date();
		this.status = "open"

		this.players = new Map();
		this.player_count = 0;

		this.ih_open = true;
		this.checked_names = false;
		this.bad_player = false;
		this.made_teams = false;

		this.underleveled_account = false;
		this.invalid_ign = false;

		this.c1 = ""; // captain 1
		this.c2 = ""; // captain 2
		this.t1 = [];
		this.t2 = [];
		this.t1_vc = null;		
		this.t2_vc = null;
	}

	copy(game){
		this.id = game.id;
		this.host = game.host;
		this.lobby = game.lobby;
		this.timestamp = game.timestamp;
		this.status = game.status;

		this.players = new Map();
		this.players.copy(game.players);
		this.player_count = game.player_count;

		this.ih_open = game.ih_open;
		this.checked_names = game.checked_names;
		this.bad_player = game.bad_player;
		this.made_teams = game.made_teams;

		this.c1 = game.c1;
		this.c2 = game.c2;
		this.t1 = game.t1.map(function(t){return t;});
		this.t2 = game.t2.map(function(t){return t;});
		this.t1_vc = game.t1_vc;
		this.v2_vc = game.t2_vc;
	}

	set_status(){
		if(this.ih_open){
			this.status = "open";
		}
		else if(this.made_teams){
			if(this.t1.length == config.ih.max_team_size && this.t2.length == config.ih.max_team_size){
				this.status = "closed, in game";
			} else {
				this.status = "closed, drafting";
			}
		}
		else{
			this.status = "closed";
		}
	}

	is_signed_up_ign(ign){
		var players = this.players;
		var igns = Object.keys(players).map(function(t){ return players[t].toUpperCase(); });
		return igns.includes(ign.toUpperCase());
	}

	is_signed_up_user(user){
		var user_ids = Object.keys(this.players);
		// if a user object, do this
		if(user.id){
			return user_ids.includes(user.id);
		// if a mention, do this
		} else {
			var user_id = util.get_did(user);
			return user_ids.includes(user_id); 
		}
	}

	is_on_team(ign){
		var ign_upper = String(ign).toUpperCase();
		var t1_upper = this.t1.map(function(t){return t.toUpperCase();});
		var t2_upper = this.t2.map(function(t){return t.toUpperCase();});
		return t1_upper.includes(ign_upper) || t2_upper.includes(ign_upper);
	}

	async update_players(){
		var existing_players = db.dbquery("select p.discord_id,p.ign from players p");
		var existing_uids = existing_players.map(function(t){return t.discord_id});
		var existing_igns = existing_players.map(function(t){return t.ign});
		var utc_epoch = new Date().getTime()/1000;
		// check if ign is changed
		var players = this.players;
		var players_igns = Object.keys(players).map(function(t){ return players[t]; });
		var players_uids = Object.keys(this.players);

		var api_region = regions[config.ih.region.toLowerCase()];

		// console.log("in update players".magenta)
		if(config.ih.draftmode === "bot soloq"){
			for(var p=0;p < players_uids.length; p++){
				var uid = players_uids[p];
				var ign = players_igns[p];


				var summoner = await (lol.get_summoner_by_name(api_region,ign));
				var summoner_data = await (lol.get_rank_by_summoner_id(api_region,summoner.id));
				var soloq = summoner_data.filter(function(t){return t.queueType === "RANKED_SOLO_5x5" });
				var rinfo = util.get_rank_info( soloq.length ? soloq[0].tier : "UNRANKED" ,soloq.length ? soloq[0].rank : "UNRANKED");

				console.log(("updating player " + ign + " " + rinfo[0]).magenta);

				// new player linking to new account
				if(!(existing_uids.includes(uid)) && !(util.includes_upper(this.players[uid],existing_igns))) {

					// console.log("NEW PLAYER".green)
					db.dbquery("insert into summoners(ign) values('" + db.dbify(ign) + "') on conflict do nothing;");
					db.dbquery("insert into players(discord_id,ign,elo,mmr,wins,losses,last_played,soloq_rank,soloq_rank_points,summoner_id) values ('" + uid + "','" + db.dbify(this.players[uid]) + "'," + String(config.ih.default_elo) + "," + String(config.ih.default_mmr) + ",0,0,to_timestamp(" + utc_epoch + "),'" + rinfo[0] + "'," + rinfo[1] + "," + summoner.id + ");")

				// if old discord player not playing on main
				} else if(existing_uids.includes(uid) && !(util.includes_upper(this.players[uid],existing_igns))) {
					var cp = db.dbquery("select * from players where discord_id='" + uid + "';")[0];
					console.log("OLD PLAYER CHANGE IGN".green);
					db.dbquery("insert into summoners(ign) values('" + db.dbify(ign) + "') on conflict do nothing;");
					
					// compare ranks for two accounts, set higher one as new main
					if(cp.soloq_rank_points){
						if(cp.soloq_rank_points < rinfo[1]){
							db.dbquery("update players set ign='" + db.dbify(ign) + "',summoner_id=" + summoner.id + ",soloq_rank='" + rinfo[0] + "',soloq_rank_points=" + rinfo[1] + " where discord_id='" + uid + "';");
						} else {
							db.dbquery("update players set ign='" + db.dbify(ign) + "' where discord_id='" + uid + "';")
						}
					} else {
						console.log(cp);
						console.log(("updating cp ign to " + ign).magenta);
						console.log("update players set ign='" + db.dbify(ign) + "',summoner_id=" + summoner.id + ",soloq_rank='" + rinfo[0] + "',soloq_rank_points=" + rinfo[1] + " where discord_id='" + uid + "';");
						db.dbquery("update players set ign='" + db.dbify(ign) + "',summoner_id=" + summoner.id + ",soloq_rank='" + rinfo[0] + "',soloq_rank_points=" + rinfo[1] + " where discord_id='" + uid + "';");
					}
				// if someone tries to sign up with an existing IGN from a nonexisting discord	
				} else if(!(existing_uids.includes(uid)) && util.includes_upper(this.players[uid],existing_igns)) {
					console.log("OLD IGN CHANGING DISCORDS".green);
					db.dbquery("update players set discord_id='" + uid +"' where ign='" + db.dbify(ign) + "';");
				}
			}

			console.log("FINISHED UPATING".red);
		} else {
			for(var d=0;d < players_uids.length; d++){
				var uid = players_uids[d];
				// completely new
				if(!(existing_uids.includes(uid)) && !(util.includes_upper(this.players[uid],existing_igns))) {
					// ("insert into summoners(ign) values ('" + db.dbify(this.players[uid]) + "');");
					db.dbquery("insert into summoners(ign) values('" + db.dbify(this.players[uid]) + "') on conflict do nothing;");
					db.dbquery("insert into players(discord_id,ign,elo,mmr,wins,losses,last_played) values ('" + uid + "','" + db.dbify(this.players[uid]) + "'," + String(config.ih.default_elo) + "," + String(config.ih.default_mmr) + ",0,0,to_timestamp(" + utc_epoch + "));")
				// new ign, old player
				} else if(existing_uids.includes(uid) && !(util.includes_upper(this.players[uid],existing_igns))){
					var old_user_ign = db.dbquery("select * from players where discord_id='" + uid + "';")[0].ign;
					db.dbquery("insert into summoners(ign) values('" + db.dbify(this.players[uid]) + "') on conflict do nothing;");
					db.dbquery("update players set ign='" + db.dbify(this.players[uid]) + "' where discord_id='" + uid + "';");
					//update games table
					db.update_ign_games(old_user_ign,this.players[uid])
				// old ign, new player
				} else if(!(existing_uids.includes(uid)) && util.includes_upper(this.players[uid],existing_igns)){
					db.dbquery("update players set discord_id='" + uid +"' where ign='" + db.dbify(this.players[uid]) + "';");
				}
			}
		}
	}

	async check_names(m,ihs){	
		var existing_players = db.dbquery("select p.discord_id,p.ign from players p");
		var existing_uids = existing_players.map(function(t){return t.discord_id});
		var existing_igns = existing_players.map(function(t){return t.ign});

		if(config.ih.draftmode == "bot soloq"){
			m.channel.send(util.cm_embed("Please wait several seconds for the check for complete....")).then(m => m.delete({timeout:2000}));

			var newly_linking_valid_string = "";
			var underleveled_accounts_string = "";
			var invalid_igns_string = "";
			var account_switching_string = "";
			var updating_igns_string = "";
			var discord_changed_players_string = "";

			var players = this.players;
			var players_uids = Object.keys(players);
			var players_igns = Object.keys(players).map(function(t){ return players[t]; });
			
			var api_region = regions[config.ih.region.toLowerCase()];

			for(var p=0; p < players_igns.length; p++){
				var uid = players_uids[p];
				var ign = players_igns[p];

				console.log( ("AT UID:<@" + uid + ">, IGN:" + ign +" DOING CHECK").red );

				try {
					var summoner = await (lol.get_summoner_by_name(api_region,ign));
					// under level 30 
					if(summoner.summonerLevel < config.ih.min_account_level){
						underleveled_accounts_string += ":x: <@" + uid + "> **" + summoner.name + "** ( Level " + summoner.summonerLevel + " )\n";
					} else {

						var summoner_data = await (lol.get_rank_by_summoner_id(api_region,summoner.id));
						var soloq = summoner_data.filter(function(t){return t.queueType === "RANKED_SOLO_5x5" });
						var rinfo = util.get_rank_info( soloq.length ? soloq[0].tier : "UNRANKED" ,soloq.length ? soloq[0].rank : "UNRANKED");

						console.log("IGN:" + ign + " ,rank:" + rinfo[0]);
						// if new discord player linking new valid LoL Account
						if(!(existing_uids.includes(uid)) && !(util.includes_upper(this.players[uid],existing_igns))) {
							newly_linking_valid_string += "" + ign.toUpperCase() + "" + " ".repeat(17-ign.length) + "[" + rinfo[0] + "]\n"; 
						
						// if old discord player not playing on main
						} else if(existing_uids.includes(uid) && !(util.includes_upper(this.players[uid],existing_igns))) {
							var cp = db.dbquery("select * from players where discord_id='" + uid + "';")[0];
							
								// check if new account rank is higher, note that it will update

							// console.log(cp.soloq_rank_points)
							// console.log(rinfo[1])
							if(cp.soloq_rank_points){
								if(cp.soloq_rank_points < rinfo[1]){
									updating_igns_string += ":small_orange_diamond: <@" + uid + "> updating from **" + cp.ign + "** " + "[" + cp.soloq_rank + "] to **" + ign.toUpperCase() + "** [" + rinfo[0] + "]\n";
								} 
							} else {
								// console.log(cp);
								updating_igns_string += ":small_orange_diamond: <@" + uid + "> updating from **" + cp.ign + "** to **" + ign.toUpperCase() + "**\n";
							}
						
						// if someone tries to sign up with an existing IGN from a nonexisting discord	
						} else if(!(existing_uids.includes(uid)) && util.includes_upper(this.players[uid],existing_igns)) {
							var ign_index = util.indexof_upper(this.players[uid],existing_igns);
							var new_user = "<@" + uid + ">";
							var old_user = "<@" + existing_uids[ign_index] + ">";
							discord_changed_players_string += ":small_orange_diamond: **" + this.players[uid] + "** " + new_user +", (old discord: " + old_user + ")\n";
						
						// account switching
						} else {
							var uid_index = existing_uids.indexOf(uid);
							var ign_index = util.indexof_upper(this.players[uid],existing_igns);
							if(uid_index!=ign_index){
								var user = "<@" + uid + ">";
								account_switching_string += ":x: " + user + " playing as **" + this.players[uid] +" **, (old ign: **" + existing_igns[uid_index] + "**).\n"
							}
						}
					}

				} catch(error){
					// invalid IGN
					invalid_igns_string += ":x: <@" + uid + "> **" + ign + "**\n";				
				}	
			}

			var names_summaries = [];
			if(invalid_igns_string.length > 0){
				names_summaries.push({
					name: "__Invalid IGNs__",
					value: "" + invalid_igns_string + "",
				});
				this.invalid_ign = true;
			} else { this.invalid_ign = false; }


			if(underleveled_accounts_string.length > 0){
				names_summaries.push({
					name: "__Under Level 30__",
					value: "" + underleveled_accounts_string + "",
				});
				this.underleveled_account = true;
			} else { this.underleveled_account = false; }

			if(newly_linking_valid_string.length > 0){
				names_summaries.push({
					name: "__ New Players__",
					value: "```css\n" + newly_linking_valid_string + "```",
				});
			}

			if(updating_igns_string.length > 0){
				names_summaries.push({
					name:"__Updating IGNs__",
					value: updating_igns_string
				})
			}
			if(discord_changed_players_string.length > 0){
				names_summaries.push({
					name:"__Changed Discord Accounts(or Misspelled)__",
					value: discord_changed_players_string
				});
			}

			if(account_switching_string.length > 0){
				names_summaries.push({
					name:"__Playing As Someone Else On the Ladder__",
					value: account_switching_string
				});
				this.bad_player = true;
			} else { this.bad_player = false; }

			if(names_summaries.length === 0){
				m.channel.send({ embed:{
					color:3447003,
					author:{
						name: this.lobby,
						icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					title: "Checking Names",
					description: ":white_check_mark: Good.",
					fields: names_summaries,
					footer: {
						icon_url: this.host.avatarURL(),
						text: "Host : " + this.host.username
					}
				}});
			} else {
				m.channel.send({ embed:{
					color:3447003,
					author:{
						name: this.lobby,
						icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "If your name is misspelled, use **.ign <ign>** to update your name.",
					fields: names_summaries,
					footer: {
						icon_url: this.host.avatarURL(),
						text: "Host : " + this.host.username
					}
				}});
			}
		}
		else {
			var new_players_string = "";
			var misspelled_players_string = "";
			var discord_changed_players_string = "";
			var account_switching_string = "";

			var players_uids = Object.keys(this.players);
			for(var d =0; d < players_uids.length; d ++) {
				var uid = players_uids[d];
				if(!(existing_uids.includes(uid)) && !(util.includes_upper(this.players[uid],existing_igns))) { new_players_string += " " + this.players[uid] + "\n"; }
				else if(existing_uids.includes(uid) && !(util.includes_upper(this.players[uid],existing_igns))) { 
					var old_ign = existing_igns[existing_uids.indexOf(uid)];
					misspelled_players_string += ":small_orange_diamond: **" + this.players[uid] + "**, (old ign: **" + old_ign +"**)\n";
				} 
				else if(!(existing_uids.includes(uid)) && util.includes_upper(this.players[uid],existing_igns)) {
					var ign_index = util.indexof_upper(this.players[uid],existing_igns);
					var new_user = "<@" + uid + ">";
					var old_user = "<@" + existing_uids[ign_index] + ">";
					discord_changed_players_string += ":small_orange_diamond: **" + this.players[uid] + "** " + new_user +", (old discord: " + old_user + ")\n";
				}
				else{
					var uid_index = existing_uids.indexOf(uid);
					var ign_index = util.indexof_upper(this.players[uid],existing_igns);
					if(uid_index!=ign_index){
						var user = "<@" + uid + ">";
						account_switching_string += ":x: " + user + " playing as **" + this.players[uid] +" **, (old ign: **" + existing_igns[uid_index] + "**).\n"
					}
				}
			}

			var names_summaries = [];
			if(new_players_string.length > 0){
				names_summaries.push({
					name: "__New Players__",
					value: "```css\n" + new_players_string + "```",
				});
			}
			if(misspelled_players_string.length > 0){
				names_summaries.push({
					name:"__Changed Igns(or Misspelled)__",
					value: misspelled_players_string
				})
			}
			if(discord_changed_players_string.length > 0){
				names_summaries.push({
					name:"__Changed Discord Accounts(or Misspelled)__",
					value: discord_changed_players_string
				});
			}

			if(account_switching_string.length > 0){
				names_summaries.push({
					name:"__Playing As Someone Else On the Ladder__",
					value: account_switching_string
				});
				this.bad_player = true;
			} else { this.bad_player = false; }

			if(names_summaries.length === 0){
				m.channel.send({ embed:{
					color:3447003,
					author:{
						name: this.lobby,
						icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					title: "Checking Names",
					description: ":white_check_mark: Good.",
					fields: names_summaries,
					footer: {
						icon_url: this.host.avatarURL(),
						text: "Host : " + this.host.username
					}
				}});
			} else {
				m.channel.send({ embed:{
					color:3447003,
					author:{
						name: this.lobby,
						icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "If your name is misspelled, use **.ign <ign>** to update your name.",
					fields: names_summaries,
					footer: {
						icon_url: this.host.avatarURL(),
						text: "Host : " + this.host.username
					}
				}});
			}
		}
	}

	set_captains(){
		// igns = (Object.keys(this.players)).map(function(t){ return this.players[t].toUpperCase(); });
		var players = this.players;
		var igns = Object.keys(this.players).map(function(a){return players[a];});
		var igns_string = (igns.map(function(t){ return '\'' + db.dbify(t) + '\'' })).join(",");
		var players_info = db.dbquery("select p.ign, p.elo,p.wins,p.losses from players p where p.ign in (" + igns_string +") order by p.elo desc;");

		// get ranked people as first captain
		// console.log(players_info);
		var ranked_players_info = players_info.filter(function(t){return (t.wins + t.losses) >= config.ih.num_placement_matches;});
		// console.log(ranked_players_info);
		if(ranked_players_info.length >= 2){
			// console.log("2 more");
			this.c2 = ranked_players_info[0].ign + " [" + String(ranked_players_info[0].elo) + "]";
			this.c1 = ranked_players_info[1].ign + " [" + String(ranked_players_info[1].elo) + "]";
		// if not enough, get person with next highest elo to captain
		} else if(ranked_players_info.length == 1){
			this.c2 = ranked_players_info[0].ign + " [" + String(ranked_players_info[0].elo) + "]";
			var players_info_sorted_by_num_games = players_info.filter(function(t){return t.ign!= ranked_players_info[0].ign; }).sort(function(a,b){
				return (b.wins + b.losses) - (a.wins + a.losses);
			});
			// console.log(players_info_sorted_by_num_games);
			this.c1 = players_info_sorted_by_num_games[0].ign + " [" + String(players_info_sorted_by_num_games[0].elo) + "] *unranked*";
		} else {
			// console.log("none");
			this.c2 = players_info[0].ign + " [" + String(players_info[0].elo) + "] *unranked*";
			this.c1 = players_info[1].ign + " [" + String(players_info[1].elo) + "] *unranked*";
		}
	}

	draft_teams_ih_mmr(){
		var players = this.players;
		var igns = Object.keys(this.players).map(function(a){return players[a];});
		var igns_string = (igns.map(function(t){ return '\'' + db.dbify(t) + '\'' })).join(",");
		var players_info = db.dbquery("select * from players p where p.ign in (" + igns_string +") order by p.mmr desc;");

		var t1 = [];
		var t1_score = 0;

		var t2 = [];
		var t2_score = 0;

		// sort players_info by mmr
		var pis = players_info.sort(function(a,b){
			return (b.mmr - a.mmr);
		});
		// console.log(pis);

		t1.push(pis[0].ign)
		t1_score += pis[0].mmr

		t2.push(pis[1].ign)
		t2_score += pis[1].mmr

		for(var i =2; i < pis.length;i ++){
			if(t1_score + pis[i].mmr < t2_score + pis[i].mmr){
				if(t1.length < 5){
					t1.push(pis[i].ign);
					t1_score += pis[i].mmr;
				} else {
					t2.push(pis[i].ign);
					t2_score += pis[i].mmr;	
				}
			} else {
				if(t2.length < 5){
					t2.push(pis[i].ign);
					t2_score += pis[i].mmr;	
				} else {
					t1.push(pis[i].ign);
					t1_score += pis[i].mmr;
				}
			}
		}

		this.t1 = t1;
		this.t2 = t2;
		this.made_teams = true;
		this.ih_open = false;
		this.timestamp = new Date();
	}

	set_t1_vc(c){
		this.t1_vc = c;
	}

	set_t2_vc(c){
		this.t2_vc = c;
	}

	// TO IMPLEMENT
	draft_teams_soloq(){

		// console.log("drafting soloq teams".green);
		var players = this.players;
		var igns = Object.keys(this.players).map(function(a){return players[a];});
		var igns_string = (igns.map(function(t){ return '\'' + db.dbify(t) + '\'' })).join(",");
		var players_info = db.dbquery("select * from players p where p.ign in (" + igns_string +") order by p.mmr desc;");

		// console.log(players_info);
		var t1 = [];
		var t1_score = 0;

		var t2 = [];
		var t2_score = 0;

		// sort players_info by soloq rank
		var pis = players_info.sort(function(a,b){
			return (b.soloq_rank_points - a.soloq_rank_points);
		});

		t1.push(pis[0].ign)
		t1_score += pis[0].soloq_rank_points

		t2.push(pis[1].ign)
		t2_score += pis[1].soloq_rank_points

		for(var i =2; i < pis.length;i ++){
			if(t1_score + pis[i].soloq_rank_points < t2_score + pis[i].soloq_rank_points){
				if(t1.length < 5){
					t1.push(pis[i].ign);
					t1_score += pis[i].soloq_rank_points;
				} else {
					t2.push(pis[i].ign);
					t2_score += pis[i].soloq_rank_points;	
				}
			} else {
				if(t2.length < 5){
					t2.push(pis[i].ign);
					t2_score += pis[i].soloq_rank_points;	
				} else {
					t1.push(pis[i].ign);
					t1_score += pis[i].soloq_rank_points;
				}
			}
		}

		this.t1 = t1;
		this.t2 = t2;
		this.made_teams = true;
		this.ih_open = false;
		this.timestamp = new Date();
	}

	update_win(winner,ihs){
		var t1w = 1;
		var t2w = 0;
		
		if(winner === "team1" || winner === "t1"){ t1w = 1; t2w = 0; } else { t1w = 0; t2w = 1; }

		var t1_string = this.t1.map(function(t){ return "\'" + db.dbify(t) + "\'"; }).join(",");
		var t1_info = db.dbquery("select * from players p where p.ign in (" + t1_string + ");");

		var t2_string = this.t2.map(function(t){ return "\'" + db.dbify(t) + "\'"; }).join(",");
		var t2_info = db.dbquery("select * from players p where p.ign in (" + t2_string + ");");

		// store last game information
		ihs.last_game = new Map();
		ihs.last_game["game"] = new Game(this.host,this.lobby);
		ihs.last_game["game"].copy(this);

		ihs.last_game["t1_info"] = db.dbquery("select * from players p where p.ign in (" + t1_string + ");");
		ihs.last_game["t2_info"] = db.dbquery("select * from players p where p.ign in (" + t2_string + ");");

		// console.log("t1_info");
		// console.log(t1_info);
		// console.log("t2_info");
		// console.log(t2_info);

		// start calculations
		var t1_wins = t1_info.map(function(t){return t.wins});
		var t2_wins = t2_info.map(function(t){return t.wins});
		var t1_losses = t1_info.map(function(t){return t.losses});
		var t2_losses = t2_info.map(function(t){return t.losses});

		//elos
		var t1_elos = t1_info.map(function(t){return t.elo });
		var t1_elo_weights = [1.15,1.1,1.05,1.05,0.9];
		var t1_avg_weighted_elo = util.w_avg(t1_elos, t1_elo_weights);

		var t2_elos = t2_info.map(function(t){return t.elo });
		var t2_elo_weights = [1.25,1,1,1,1];
		var t2_avg_weighted_elo = util.w_avg(t2_elos, t2_elo_weights);	

		//mmrs
		var t1_mmrs = t1_info.map(function(t){return t.mmr });
		var t1_mmr_weights = [1.15,1.1,1.05,0.9,0.9];
		var t1_avg_weighted_mmr = util.w_avg(t1_mmrs, t1_mmr_weights);

		// console.log("t1 mmr avg");
		// console.log(t1_avg_weighted_mmr);

		var t2_mmrs = t2_info.map(function(t){return t.mmr });
		var t2_mmr_weights = [1.25,1,1,1,0.85];
		var t2_avg_weighted_mmr = util.w_avg(t2_mmrs, t2_mmr_weights);

		// console.log("t2 mmr avg");
		// console.log(t2_avg_weighted_mmr);
		

		/**************************** MMR Calculations *********************************/
		// more calculations
		var t1_win_percentage = 1 / (1 + Math.pow(10,(t2_avg_weighted_mmr - t1_avg_weighted_mmr)/400));
		var t2_win_percentage = 1 / (1 + Math.pow(10,(t1_avg_weighted_mmr - t2_avg_weighted_mmr)/400));

		// console.log("t1 win %");
		// console.log(t1_win_percentage);

		// console.log("t2 win %");
		// console.log(t2_win_percentage);

		var t1_post_game_elo = t1_avg_weighted_elo + 69 * (t1w - t1_win_percentage);
		var t2_post_game_elo = t2_avg_weighted_elo + 69 * (t2w - t2_win_percentage);

		var t1_elo_diff_raw = t1_post_game_elo - t1_avg_weighted_elo;
		var t2_elo_diff_raw = t2_post_game_elo - t2_avg_weighted_elo;

		var t1_elo_diff = t1_elo_diff_raw * 2 * 1.1;
		var t2_elo_diff = t2_elo_diff_raw * 2 * 1.1;

		// make it so that you lose less lp if you lose
		if(t1_elo_diff < 0){
			t1_elo_diff = config.ih.loss_multiplier * t1_elo_diff;
		}
		else {
			t2_elo_diff = config.ih.loss_multiplier * t2_elo_diff;
		}

		// console.log("t1 elo diff");
		// console.log(t1_elo_diff);

		// console.log("t2 elo diff");
		// console.log(t2_elo_diff);

		var t1_elo_diff_values = [];
		var t2_elo_diff_values = [];
		//factor in multiplier for placements
		for(var i = 0;i < t1_info.length;i++){
			var t1p = t1_info[i]; var t2p = t2_info[i];
			var t1p_games = t1p.wins + t1p.losses; var t2p_games = t2p.wins + t2p.losses;
			if(t1p_games < config.ih.num_placement_matches){
				t1_elo_diff_values.push(config.ih.placement_multiplier * t1_elo_diff);
			} else { 
				if(config.ih.elocap === "on"){
					// console.log("HERE ONNN")
					// console.log(t1p.elo);
					// console.log(t1_elo_diff);

					var capped_diff = util.adjusted_diff(t1p.elo, t1_elo_diff);
					// console.log("capped_diffconsole.log")
					// console.log(capped_diff);
					t1_elo_diff_values.push(capped_diff); 
				} else {
					t1_elo_diff_values.push(t1_elo_diff);
				}
			}

			if(t2p_games < config.ih.num_placement_matches){
				t2_elo_diff_values.push(config.ih.placement_multiplier * t2_elo_diff);
			} else { 
				if(config.ih.elocap === "on"){
					t2_elo_diff_values.push(util.adjusted_diff(t2p.elo, t2_elo_diff)); 
				} else {
					t2_elo_diff_values.push(t2_elo_diff); 
				}
			}
		}

		// console.log("t1 elo diff values");
		// console.log(t1_elo_diff_values);

		// console.log("t2 elo diff values");
		// console.log(t2_elo_diff_values);
		// new values to update
		var t1_new_elos = util.ae_add(t1_elos,t1_elo_diff_values);
		var t1_new_mmrs = util.ae_add(t1_mmrs,t1_elo_diff_values.map(function(t){return 1.1 * t;}));

		// console.log("t1 new elos");
		// console.log(t1_new_elos);

		// console.log("t1 new mmrs");
		// console.log(t1_new_mmrs);

		var t2_new_elos = util.ae_add(t2_elos,t2_elo_diff_values);
		var t2_new_mmrs = util.ae_add(t2_mmrs,t2_elo_diff_values.map(function(t){return 1.1 * t;}));

		// console.log("t2 new elos");
		// console.log(t2_new_elos);

		// console.log("t2 new mmrs");
		// console.log(t2_new_mmrs);


		var t1_new_wins = [];
		var t1_new_losses = [];
		var t2_new_wins = [];
		var t2_new_losses = [];

		// t1 win
		var t1_new_wins = null;
		var t1_new_losses = null;
		var t2_new_wins = null;
		var t2_new_losses = null;

		if(winner === "team1" || winner === "t1"){
			t1_new_wins = util.ae_add(t1_wins,[1,1,1,1,1]);
			t1_new_losses = t1_losses;
			t2_new_wins = t2_wins;
			t2_new_losses = util.ae_add(t2_losses,[1,1,1,1,1]);
		}
		else if(winner === "team2" || winner === "t2"){
			t1_new_wins = t1_wins;
			t1_new_losses = util.ae_add(t1_losses,[1,1,1,1,1]);
			t2_new_wins = util.ae_add(t2_wins,[1,1,1,1,1]);
			t2_new_losses = t2_losses;
		}

		var t1_new_info = util.update_info(t1_info,t1_new_elos,'elo');
		t1_new_info = util.update_info(t1_info,t1_new_mmrs,'mmr');
		t1_new_info = util.update_info(t1_info,t1_new_wins,'wins');
		t1_new_info = util.update_info(t1_info,t1_new_losses,'losses');

		var t2_new_info = util.update_info(t2_info,t2_new_elos,'elo');
		t2_new_info = util.update_info(t2_info,t2_new_mmrs,'mmr');
		t2_new_info = util.update_info(t2_info,t2_new_wins,'wins');
		t2_new_info = util.update_info(t2_info,t2_new_losses,'losses');

		// update new info to database
		var utc_epoch = new Date().getTime()/1000;
		for(var i = 0;i < t1_new_info.length;i++){
			// console.log("t1")
			// console.log(t1)
			// console.log("t2")
			// console.log(t2)

			var t1 = t1_new_info[i];
			var t2 = t2_new_info[i];
			db.dbquery("update players set elo =" +  t1.elo + ", mmr = " + t1.mmr + ", wins =" + t1.wins +", losses = " + t1.losses +", last_played = to_timestamp(" + utc_epoch + ") where ign='" + db.dbify(t1.ign) + "';");
			db.dbquery("update players set elo =" +  t2.elo + ", mmr = " + t2.mmr + ", wins =" + t2.wins +", losses = " + t2.losses +", last_played = to_timestamp(" + utc_epoch + ") where ign='" + db.dbify(t2.ign) + "';");
		}

		// add game to database
		var t1_dbified = this.t1.map(function(t){return db.dbify(t)});
		var t2_dbified = this.t2.map(function(t){return db.dbify(t)});

		var query_string = "insert into games (t1p1, t1p2, t1p3, t1p4, t1p5, t2p1, t2p2, t2p3, t2p4, t2p5, winner, game_date,game_id, host) values(" 
		+  "\'" + t1_dbified[0] + "\'," 
		+  "\'" + t1_dbified[1] + "\'," 
		+  "\'" + t1_dbified[2] + "\'," 
		+  "\'" + t1_dbified[3] + "\'," 
		+  "\'" + t1_dbified[4] + "\'," 
		+  "\'" + t2_dbified[0] + "\'," 
		+  "\'" + t2_dbified[1] + "\'," 
		+  "\'" + t2_dbified[2] + "\'," 
		+  "\'" + t2_dbified[3] + "\'," 
		+  "\'" + t2_dbified[4] + "\',"
		+ String(( (winner==="team1" || winner === "t1") ? 1 : 2 )) + ","
		+ "to_timestamp(" + utc_epoch + "),"
		+ String(this.id) + ","
		+ "\'" + this.host.id + "\'"
		+ ");";

		db.dbquery(query_string);
	}

	show_winner(m,winner){
		var winning_string = "";
		var winning_team = 0;

		if(winner.toLowerCase() === "team1" || winner.toLowerCase() === "t1"){ 
			winning_string = this.t1.map(function(t){return t.toUpperCase()}).join(", "); 
			winning_team = 1;
		}
		else { 
			winning_string = this.t2.map(function(t){return t.toUpperCase()}).join(", "); 
			winning_team = 2;
		}

		var top_players = db.dbquery("select * from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + " order by p.elo desc,p.mmr desc, p.wins desc,p.losses asc,p.ign asc limit " + String(config.ih.max_top_players) + ";");
		var announce_string = "```css\n   IGN" + " ".repeat(14) + "ELO    MMR   W   L ``````fix\n";
		for (var i = 0; i < top_players.length; i ++){ announce_string += String(i+1) + ". " + top_players[i].ign + " ".repeat(17 - top_players[i].ign.length) + String(top_players[i].elo) + "   " + String(top_players[i].mmr) + "  " + String(top_players[i].wins) + " ".repeat(4-String(top_players[i].wins).length) + String(top_players[i].losses) + "\n";}
		announce_string += " ```";

		m.channel.send({embed: {
			color:3447003,
			author:{
				name: this.lobby,
				icon_url:m.guild.iconURL()
			},
			title: ":tada: :confetti_ball: __Winner: Team "  + winning_team + "__ :confetti_ball: :tada:",
			description: "Congratulations to **Team " + winning_team + " ("+ winning_string +") ** for winning the last in-house game!\nTop players are:" + announce_string,
		}}).then().catch(console.error);
	}

	award_currency(winner,ihs){
		var guild = ihs.client.guilds.find("id",config.server.server_id);

		var currency_award_channel = guild.channels.find("id",config.server.currency_award_channel);
		if(currency_award_channel!=null){
			var t1 = this.t1.map(function(t){return t.toUpperCase()});
			var t2 = this.t2.map(function(t){return t. toUpperCase()});
			
			//build players from team
			var players = []
			for(var i = 0;i < t1.length; i++){
				var player = db.dbquery("select * from players where ign='" + db.dbify(t1[i]) + "';")[0]
				players.push(player.discord_id);
			}
			for(var i = 0; i < t2.length; i++){
				var player = db.dbquery("select * from players where ign='" + db.dbify(t2[i]) + "';")[0]
				players.push(player.discord_id);
			}

			// send messages about flowers rewards
			var queries = players.map(function(t){return [config.nadeko.ih_loser_reward,t]});
			if(winner.toLowerCase() === "team1" || winner.toLowerCase() === "t1"){
				for(var i = 0; i < config.ih.max_team_size;i ++){
					queries[i][0] = config.nadeko.ih_winner_reward;
					var winner_member = guild.members.find("id",queries[i][1]);
					if(winner_member){
						winner_member.user.send({embed:{
							description: "``You've received``: " + String(config.nadeko.ih_winner_reward) + " " + config.nadeko.currency + "\n``Reason``: **" + config.server.server_name + " In-House**  + **Winner's Bonus** "
						}});
					}
				}
				for(var i = config.ih.max_team_size; i < config.ih.max_team_size * 2; i ++){
					var loser_member = guild.members.find("id",queries[i][1]);
					if(loser_member){
						loser_member.user.send({embed:{
							description: "``You've received``: " + String(config.nadeko.ih_loser_reward) + " " + config.nadeko.currency + "\n``Reason``: **" + config.server.server_name + " In-House** "
						}});
					}
				}

				var dids = queries.map(function(t){return t[1]});
				var winning_dids = dids.slice(0,config.ih.max_team_size);
				var losing_dids = dids.slice(config.ih.max_team_size, config.ih.max_team_size * 2);
				// send message to currency award channel
				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Award Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Awarded " + config.nadeko.ih_winner_reward + " " + config.nadeko.currency + "**:\n\t<@" + winning_dids.join(">\n\t<@") + ">",
					timestamp: new Date()
				}});

				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Award Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Awarded " + config.nadeko.ih_loser_reward + " " + config.nadeko.currency + "**:\n\t<@" + losing_dids.join(">\n\t<@") + ">",
					timestamp: new Date()

				}});
			} else {
				for(var i = config.ih.max_team_size; i < config.ih.max_team_size * 2; i ++){
					queries[i][0] = config.nadeko.ih_winner_reward;

					var winner_member = guild.members.find("id",queries[i][1]);
					if(winner_member){
						winner_member.user.send({embed:{
							description: "``You've received``: " + String(config.nadeko.ih_winner_reward) + " " + config.nadeko.currency +"\n``Reason``: **" + config.server.server_name + " In-House**  + **Winner's Bonus** "
						}});
					}
				}

				for(var i = 0; i < config.ih.max_team_size; i++){
					var loser_member = guild.members.find("id",queries[i][1]);
					if(loser_member){
						loser_member.user.send({embed:{
							description: "``You've received``: " + String(config.nadeko.ih_loser_reward) + " " + config.nadeko.currency + "\n``Reason``: **" + config.server.server_name + " In-House** "
						}});
					}
				}

				var dids = queries.map(function(t){return t[1]});
				var losing_dids = dids.slice(0,config.ih.max_team_size);
				var winning_dids = dids.slice(config.ih.max_team_size,config.ih.max_team_size * 2)
				// send message to currency award channel
				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Award Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Awarded " + config.nadeko.ih_winner_reward + " " + config.nadeko.currency + "**:\n\t<@" + winning_dids.join(">\n\t<@") + ">",
					timestamp: new Date()
				}});

				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Award Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Awarded " + config.nadeko.ih_loser_reward + " " + config.nadeko.currency + "**:\n\t<@" + losing_dids.join(">\n\t<@") + ">",
					timestamp: new Date()
				}});
			}
			// // database award flowers
			db.nadeko_award_currency(queries)
		}
	}

	undo_award_currency(winner,ihs){
		var guild = ihs.client.guilds.find("id",config.server.server_id);

		var currency_award_channel = guild.channels.find("id",config.server.currency_award_channel);
		if(currency_award_channel!=null){
			var t1 = this.t1.map(function(t){return t.toUpperCase()});
			var t2 = this.t2.map(function(t){return t. toUpperCase()});
			
			//build players from team
			var players = []
			for(var i = 0;i < t1.length; i++){
				var player = db.dbquery("select * from players where ign='" + db.dbify(t1[i]) + "';")[0]
				players.push(player.discord_id);
			}
			for(var i = 0; i < t2.length; i++){
				var player = db.dbquery("select * from players where ign='" + db.dbify(t2[i]) + "';")[0]
				players.push(player.discord_id);
			}

			var queries = players.map(function(t){return [config.nadeko.ih_loser_reward,t]}); 
			if(winner.toLowerCase() === "team1" || winner.toLowerCase() === "t1"){
				for(var i = 0; i < config.ih.max_team_size;i ++){
					queries[i][0] = config.nadeko.ih_winner_reward;

					var winner_member = guild.members.find("id",queries[i][1]);
					if(winner_member){
						winner_member.user.send({embed:{
							description: "``You've lost``: " + String(config.nadeko.ih_winner_reward) + " " + config.nadeko.currency + "\n``Reason``: " + config.server.server_name + " In-House Mod made a mistake with updating the win. :face_palm:  "
						}});
					}
				}
				for(var i = config.ih.max_team_size; i < config.ih.max_team_size * 2; i ++){
					var loser_member = guild.members.find("id",queries[i][1]);
					if(loser_member){
						loser_member.user.send({embed:{
							description: "``You've lost``: " + String(config.nadeko.ih_loser_reward) + " " + config.nadeko.currency + "\n``Reason``: " + config.server.server_name + " In-House Mod made a mistake with updating the win. :face_palm:  "
						}});
					}
				}

				var dids = queries.map(function(t){return t[1]});
				var winning_dids = dids.slice(0,config.ih.max_team_size);
				var losing_dids = dids.slice(config.ih.max_team_size,config.ih.max_team_size * 2)
				// send message to currency award channel
				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Take Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Took " + config.nadeko.ih_winner_reward + " " + config.nadeko.currency + "**:\n\t<@" + winning_dids.join(">\n\t<@") + ">",
                    timestamp: new Date()
				}});

				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Take Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Took " + config.nadeko.ih_loser_reward + " " + config.nadeko.currency + "**:\n\t<@" + losing_dids.join(">\n\t<@") + ">",
					timestamp: new Date()
				}});

			} else {
				for(var i = config.ih.max_team_size; i < config.ih.max_team_size * 2; i ++){
					queries[i][0] = config.nadeko.ih_winner_reward;

					var winner_member = guild.members.find("id",queries[i][1]);
					if(winner_member){
						winner_member.user.send({embed:{
							description: "``You've lost``: " + String(config.nadeko.ih_winner_reward) + " " + config.nadeko.currency + "\n``Reason``: " + config.server.server_name + " In-House Mod made a mistake with updating the win. :face_palm:  "
						}});
					}
				}

				for(var i = 0; i < config.ih.max_team_size; i++){
					var loser_member = guild.members.find("id",queries[i][1]);
					if(loser_member){
						loser_member.user.send({embed:{
							description: "``You've lost``: " + String(config.nadeko.ih_loser_reward) + " " + config.nadeko.currency + "\n``Reason``: " + config.server.server_name + " In-House Mod made a mistake with updating the win. :face_palm:  "
						}});
					}
				}

				var dids = queries.map(function(t){return t[1]});
				var losing_dids = dids.slice(0,config.ih.max_team_size);
				var winning_dids = dids.slice(config.ih.max_team_size,config.ih.max_team_size * 2)
				// send message to currency award channel
				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Take Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Took " + config.nadeko.ih_winner_reward + " " + config.nadeko.currency + "**:\n\t<@" + winning_dids.join(">\n\t<@") + ">",
					timestamp: new Date()
				}});

				currency_award_channel.send({embed:{
					author:{
						name: config.server.server_name + " In-House Currency Take Log",
						icon_url : ihs.client.guilds.find("id",config.server.server_id).iconURL()
					},
					description: "**Took " + config.nadeko.ih_loser_reward + " " + config.nadeko.currency + "**:\n\t<@" + losing_dids.join(">\n\t<@") + ">",
					timestamp: new Date()
				}});
			}

			db.nadeko_undo_award_currency(queries);
		}
	}

	show_players(m){
		var players_string = "";
		var players = this.players;
		var igns = Object.keys(players).map(function(t){return players[t]});
		
		for(var i=0; i<this.player_count; i++) {
			if(this.is_on_team(igns[i])){
				// players_string += "" + String(i+1) + "." + " ".repeat(3 - String(i+1).length) + "[" + igns[i] + "]\n"; 
				players_string += "";
			} else {
				players_string += " " + String(i+1) + "." + " ".repeat(3 - String(i+1).length) + igns[i] + "\n"; 
			}
		}
		// send new message
		m.channel.send({ embed: {
			color:3447003,
			author:{
				name: this.lobby,
				icon_url:m.guild.iconURL()
			},
			title: "__Signed Up Players__",
			description: players_string.length == 0 ? "*None*" :"```css\n" + players_string + "```",
			footer: {
				icon_url: this.host.avatarURL(),
				text: "Host : " + this.host.username
			}
		}}).then(function(){
		}).catch(console.error);
	}

	show_teams(m,ihs){

		var t1_string = "";
		for(var i = 0;i < this.t1.length; i++) {
			if(config.ih.draftmode === "bot mmr"){
				var prating = db.dbquery("select * from players where ign='" + db.dbify(this.t1[i]) + "';")[0].mmr;
				t1_string += String(i+1) + ". [" + String(prating) + "] " + this.t1[i] + "\n"; 
			} else if(config.ih.draftmode === "bot soloq") {
				var psqr = db.dbquery("select * from players where ign='" + db.dbify(this.t1[i]) + "';")[0].soloq_rank;
				t1_string += String(i+1) + ". [" + String(psqr) + "]" + " ".repeat(3-psqr.length) + this.t1[i] + "\n"; 
			} else {
				t1_string += String(i+1) + ". " + this.t1[i] + "\n";	
			}
		}
		var t2_string = "";
		for(var i = 0;i < this.t2.length; i++) {
			if(config.ih.draftmode === "bot mmr"){
				var prating = db.dbquery("select * from players where ign='" + db.dbify(this.t2[i]) + "';")[0].mmr;
				t2_string += String(i+1) + ". [" + String(prating) + "] " + this.t2[i] + "\n"; 
			} else if(config.ih.draftmode === "bot soloq") {
				var psqr = db.dbquery("select * from players where ign='" + db.dbify(this.t2[i]) + "';")[0].soloq_rank;
				t2_string += String(i+1) + ". [" + String(psqr) + "]" + " ".repeat(3-psqr.length) + this.t2[i] + "\n"; 
			} else {
				t2_string += String(i+1) + ". " + this.t2[i] + "\n";
			}
		}

		// delete last team log
		if(ihs.tbd_mids["drafting"]){
			//fetch and delete message
			var tbd_message = m.channel.messages.find("id",ihs.tbd_mids["drafting"]);
			if(tbd_message){
				tbd_message.delete().then().catch(console.error);
			}
		}

		console.log("TESTING".red)
		console.log(config.ih.draftmode === "captains" ? "__Drafting Teams__" : "__Drafted Teams by " + ((config.ih.draftmode === "bot soloq") ? "Solo Queue Rank" :  "MMR") + "__");

		m.channel.send({ embed: {
			color:3447003,
			author:{
				name: this.lobby,
				icon_url:m.guild.iconURL()
			},
			title: config.ih.draftmode === "captains" ? "__Drafting Teams__" : "__Drafted Teams by " + ((config.ih.draftmode === "bot soloq") ? "Solo Queue Rank" :  "MMR") + "__",
			description: config.ih.draftmode === "captains" ? "``Captain 1: " + this.c1 + "``\n``Captain 2: " + this.c2 + "``" : "",
			fields: [{
				name: "Team 1",
				value: t1_string.length === 0 ? "*None*" : "```css\n" + t1_string + "```"
			},
			{
				name: "Team 2",
				value: t2_string.length === 0 ? "*None*" : "```css\n" + t2_string + "```"
			}],
			footer: {
				icon_url: this.host.avatarURL(),
				text: "Host : " + this.host.username
			}
		}}).then(function(tm){
			ihs.set_tbd_mid("drafting",tm.id)
		}).catch(console.error);

	}
}

module.exports = Game;
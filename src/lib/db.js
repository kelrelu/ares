var pgn = require("pg-native");
var fs = require("fs");
var sqlite = require("sqlite3");

var util = require("./util.js");
var config = util.config;

var db = module.exports = {}
var my_db = new pgn();
var db_conn_string = "postgres://" + config.db.user + ":" + config.db.password + "@" + config.db.host+ ":" + config.db.port + "/" + config.db.name;

// dbquery shortcut
db.dbquery = function(query){
	try{
		my_db.connectSync(db_conn_string);
		var info = my_db.querySync(query);
		my_db.end();
		return info;
	} catch(error){ console.log(error); }
}

// alters query so it's safe for SQL, also remove SQL injection-like code if it exists
db.dbify = function(s){
	// check for SQL injection and remove 
	return s.replaceAll("\'","\'\'").toUpperCase();
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

// checks if discord id is on ladder
db.is_on_ladder_did = function(did){
	var players = db.dbquery("select * from players;");
	var players_dids = players.map(function(t){return t.discord_id});
	return players_dids.includes(did);
}

// checks if IGN is on ladder
db.is_on_ladder_ign = function(ign){
	var players = db.dbquery("select * from players;");
	var players_igns = players.map(function(t){return t.ign});
	return players_igns.includes(ign.toUpperCase());
}

// updates IGN in games table with new IGN
db.update_ign_games = function(old_ign, new_ign){
	db.dbquery("update games set t1p1='" + db.dbify(new_ign) + "' where t1p1='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t1p2='" + db.dbify(new_ign) + "' where t1p2='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t1p3='" + db.dbify(new_ign) + "' where t1p3='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t1p4='" + db.dbify(new_ign) + "' where t1p4='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t1p5='" + db.dbify(new_ign) + "' where t1p5='" + db.dbify(old_ign) + "';");
	
	db.dbquery("update games set t2p1='" + db.dbify(new_ign) + "' where t2p1='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t2p2='" + db.dbify(new_ign) + "' where t2p2='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t2p3='" + db.dbify(new_ign) + "' where t2p3='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t2p4='" + db.dbify(new_ign) + "' where t2p4='" + db.dbify(old_ign) + "';");
	db.dbquery("update games set t2p5='" + db.dbify(new_ign) + "' where t2p5='" + db.dbify(old_ign) + "';");
}


db.exists_ign = function(ign){
	var players = db.dbquery("select ign from players").map(function(t){return t.ign});
	return players.includes(ign.toUpperCase());
}

db.is_veteran_ign = function(ign){
	var veterans = db.dbquery("select * from players order by (wins+losses) desc, wins desc, losses asc limit " + config.ih.max_vets + ";");
	var veterans_igns = veterans.map(function(t){return t.ign;});
	if(veterans_igns.includes(ign.toUpperCase())){
		return true;
	}
	return false;
}

db.update_itp_tags = function(ihs){

	var guild = ihs.client.guilds.find("id",config.server.server_id);
	var itp_roles = util.get_roles_by_id(guild,config.roles.ih_top_player);

	var new_itps_info = db.dbquery("select * from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + " order by p.elo desc,p.mmr desc, p.wins desc,p.losses asc,p.ign asc limit " + String(config.ih.max_top_players) + ";");
	// console.log("new itps");
	// console.log(new_itps_info.map(function(t){return t.ign}));
	var new_itps_discord_ids = new_itps_info.map(function(t){ return t.discord_id; })
	
	// remove tag from current members
	if(itp_roles){
		// return arrays of members for each row
	
		for(var tag=0; tag < itp_roles.length;tag++){
			var mems = itp_roles[tag].members.array();
			for(var m=0;m < mems.length;m++){
				if(mems[m].roles.has(itp_roles[tag].id) && (new_itps_discord_ids.includes(mems[m].user.id) === false)) { 
					mems[m].removeRole(itp_roles[tag]);
				}
			}
		}
		// add tag to new members
		for(var i = 0;i < new_itps_discord_ids.length; i++){
			// console.log(new_itps_discord_ids[id]);
			var new_itp = guild.member(new_itps_discord_ids[i]);
			if(new_itp){
				for(var tag = 0; tag < itp_roles.length; tag ++){
					if(new_itp.roles.has(itp_roles[tag].id) == false){
						new_itp.addRole(itp_roles[tag]).then().catch(console.error);
					}
				} 
			}	
		}
	}
}

// initializes database for bot
db.init_all = function(){
	db.dbquery("create table if not exists summoners( \
		ign text, \
		id text, \
		rank text, \
		division text, \
		highest_mastery_champion text, \
		primary key(ign) \
	);");

	db.dbquery("create table if not exists players( \
		discord_id text, \
		ign text not null unique, \
		elo int not null, \
		mmr int not null, \
		wins int not null, \
		losses int not null, \
		last_played timestamp, \
		summoner_id int, \
		soloq_rank text, \
		soloq_rank_points int, \
		primary key(discord_id), \
		foreign key(ign) references summoners(ign) \
	);");

	db.dbquery("create table if not exists games(\
		t1p1 text not null, \
		t1p2 text not null, \
		t1p3 text not null, \
		t1p4 text not null, \
		t1p5 text not null, \
		t2p1 text not null, \
		t2p2 text not null, \
		t2p3 text not null, \
		t2p4 text not null, \
		t2p5 text not null, \
		winner int not null, \
		game_date timestamp not null, \
		game_id bigserial, \
		host text, \
		foreign key(t1p1) references summoners(ign), \
		foreign key(t1p2) references summoners(ign), \
		foreign key(t1p3) references summoners(ign), \
		foreign key(t1p4) references summoners(ign), \
		foreign key(t1p5) references summoners(ign), \
		foreign key(t2p1) references summoners(ign), \
		foreign key(t2p2) references summoners(ign), \
		foreign key(t2p3) references summoners(ign), \
		foreign key(t2p4) references summoners(ign), \
		foreign key(t2p5) references summoners(ign), \
		primary key(game_id) \
	);");

	db.dbquery("create table if not exists lobbies(\
		name text not null, \
		t1p1 text not null, \
		t1p2 text not null, \
		t1p3 text not null, \
		t1p4 text not null, \
		t1p5 text not null, \
		t2p1 text not null, \
		t2p2 text not null, \
		t2p3 text not null, \
		t2p4 text not null, \
		t2p5 text not null, \
		game_date timestamp not null, \
		game_id bigserial, \
		host text, \
		foreign key(t1p1) references summoners(ign), \
		foreign key(t1p2) references summoners(ign), \
		foreign key(t1p3) references summoners(ign), \
		foreign key(t1p4) references summoners(ign), \
		foreign key(t1p5) references summoners(ign), \
		foreign key(t2p1) references summoners(ign), \
		foreign key(t2p2) references summoners(ign), \
		foreign key(t2p3) references summoners(ign), \
		foreign key(t2p4) references summoners(ign), \
		foreign key(t2p5) references summoners(ign), \
		primary key(game_id) \
	);");

	db.dbquery("create table if not exists bets(\
		bet_id text not null, \
		uid text not null, \
		amount int not null, \
		game_id bigserial not null, \
		lobby_name text not null, \
		team int not null, \
		primary key(bet_id) \
	)");
}

db.clear_all = function(){
	db.dbquery("drop table if exists bets;");
	db.dbquery("drop table if exists lobbies;");
	db.dbquery("drop table if exists games;");
	db.dbquery("drop table if exists players;");
	db.dbquery("drop table if exists summoners;");
	db.init_all();
}

db.delete_lobby = function(l){
	db.dbquery("delete from lobbies where game_id=" + l.id);
}

db.insert_lobby = function(l){
	var utc_epoch = l.timestamp.getTime()/1000;

	db.dbquery("insert into lobbies(name,t1p1,t1p2,t1p3,t1p4,t1p5,t2p1,t2p2,t2p3,t2p4,t2p5,game_date,game_id,host) values(" 
	+ "'" + l.lobby + "',"
	+ "\'" + db.dbify(l.t1[0]) + "\',"
	+ "\'" + db.dbify(l.t1[1]) + "\',"
	+ "\'" + db.dbify(l.t1[2]) + "\',"
	+ "\'" + db.dbify(l.t1[3]) + "\',"
	+ "\'" + db.dbify(l.t1[4]) + "\',"
	+ "\'" + db.dbify(l.t2[0]) + "\',"
	+ "\'" + db.dbify(l.t2[1]) + "\',"
	+ "\'" + db.dbify(l.t2[2]) + "\',"
	+ "\'" + db.dbify(l.t2[3]) + "\',"
	+ "\'" + db.dbify(l.t2[4]) + "\',"
	+ "to_timestamp(" + utc_epoch + "),"
	+ l.id + ","
	+ "\'" + l.host.id + "\') on conflict (game_id) do update set " 
	+ "name='" + l.lobby + "',"
	+ "t1p1='" + db.dbify(l.t1[0]) + "',"
	+ "t1p2='" + db.dbify(l.t1[1]) + "',"
	+ "t1p3='" + db.dbify(l.t1[2]) + "',"
	+ "t1p4='" + db.dbify(l.t1[3]) + "',"
	+ "t1p5='" + db.dbify(l.t1[4]) + "',"
	+ "t2p1='" + db.dbify(l.t2[0]) + "',"
	+ "t2p2='" + db.dbify(l.t2[1]) + "',"
	+ "t2p3='" + db.dbify(l.t2[2]) + "',"
	+ "t2p4='" + db.dbify(l.t2[3]) + "',"
	+ "t2p5='" + db.dbify(l.t2[4]) + "',"
	+ "game_date=to_timestamp(" + utc_epoch + "),"
	+ "game_id=" + l.id + ","
	+ "host='" + l.host.id + "';");
}

db.nadeko_undo_award_currency = function(queries){
	try{
		var nadeko_db = new sqlite.Database(config.nadeko.database, (error) => {
			if(error){
				console.error(error.message);
			}
		});

		for(var q =0;q<queries.length;q++){
			var amount = queries[q][0];
			var uid = queries[q][1];
			var date = new Date().toISOString().replaceAll("T"," ").replaceAll("Z", "");			
			var q1 = "insert or ignore into Currency (Amount, UserId, DateAdded) values (0," + uid + ",'" + date + "');";
			var q2 = "update Currency set Amount=Amount - " + String(amount) + " where UserId=" + uid + " and Amount > "+ String(amount) + ";"

			nadeko_db.serialize(()=>{
				nadeko_db.run(q1).run(q2);
			});
		}
		nadeko_db.close((error) => {
			if(error){
				return console.error;
			}
		});
	} catch(error){ console.log(error); }
}

// db query for nadeko database
db.nadeko_award_currency = function(queries){
	try{
		var nadeko_db = new sqlite.Database(config.nadeko.database, (error) => {
			if(error){
				console.log("Nadeko Database Opening Error")
				console.error(error.message);
			}
		});

		for(var q =0;q<queries.length;q++){
			var amount = queries[q][0];
			var uid = queries[q][1];
			var date = new Date().toISOString().replaceAll("T"," ").replaceAll("Z", "");			
			var q1 = "insert or ignore into Currency (Amount, UserId, DateAdded) values (0," + uid + ",'" + date + "');";
			var q2 = "update Currency set Amount=Amount + " + String(amount) + " where UserId=" + uid + ";"

			nadeko_db.serialize(()=>{
				nadeko_db.run(q1).run(q2);
			});
		}
		nadeko_db.close((error) => {
			if(error){
				console.log("Nadeko Database Closing Error")
				return console.error;
			}
		});
	} catch(error){ console.log(error); }
}

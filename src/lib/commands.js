var commands = module.exports = new Map();

var commands_dir = "./commands";
// in-house
commands["lobbies"] = require(commands_dir + "/lobbies.js"); // good
commands["host"] = require(commands_dir + "/host.js"); // good
commands["sethost"] = require(commands_dir + "/sethost.js"); // good
commands["goto"] = require(commands_dir + "/goto.js"); // good
commands["ign"] = require(commands_dir + "/ign.js"); // good
commands["quit"] = require(commands_dir + "/quit.js"); // good
commands["pinglfg"] = require(commands_dir + "/pinglfg.js"); // good
commands["players"] = require(commands_dir + "/players.js"); // good
commands["rename"] = require(commands_dir + "/rename.js"); // good
commands["delete"] = require(commands_dir + "/delete.js"); // good
commands["remove"] = require(commands_dir + "/remove.js"); // good
commands["checknames"] = require(commands_dir + "/checknames.js"); // good
commands["close"] = require(commands_dir + "/close.js"); //good
commands["reopen"] = require(commands_dir + "/reopen.js"); // good
commands["cancel"] = require(commands_dir + "/cancel.js"); // good
commands["maketeams"] = require(commands_dir + "/maketeams.js");
commands["deleteteams"] = require(commands_dir + "/deleteteams.js");
commands["teams"] = require(commands_dir + "/teams.js"); // good
commands["team1"] = require(commands_dir + "/team1.js"); // good
commands["team2"] = require(commands_dir + "/team2.js"); // good
commands["win"] = require(commands_dir + "/win.js"); // good
commands["undowin"] = require(commands_dir + "/undowin.js");
commands["reload"] = require(commands_dir + "/reload.js"); // good
commands["silence"] = require(commands_dir + "/silence.js");
commands["stopsilence"] = require(commands_dir + "/stopsilence.js");


// information
commands["opgg"] = require(commands_dir + "/opgg.js");
commands["stats"] = require(commands_dir + "/stats.js"); // good
commands["synergy"] = require(commands_dir + "/synergy.js");
commands["ladder"] = require(commands_dir + "/ladder.js");
commands["veterans"] = require(commands_dir + "/veterans.js");
commands["ladderstats"] = require(commands_dir + "/ladderstats.js");
commands["laddercsv"] = require(commands_dir + "/laddercsv.js");
commands["gamescsv"] = require(commands_dir + "/gamescsv.js");
commands["hoststats"] = require(commands_dir + "/hoststats.js");
commands["helplfg"] = require(commands_dir + "/help.js");


// ladder
commands["ladderadd"] = require(commands_dir + "/ladderadd.js");
commands["ladderaddnew"] = require(commands_dir + "/ladderaddnew.js");
commands["ladderdelete"] = require(commands_dir + "/ladderdelete.js"); // good
commands["ladderrename"] = require(commands_dir + "/ladderrename.js");
commands["ladderupdate"] = require(commands_dir + "/ladderupdate.js");
commands["ladderprune"] = require(commands_dir + "/ladderprune.js");
commands["ladderclear"] = require(commands_dir + "/ladderclear.js");

// settings
commands["settings"] = require(commands_dir + "/settings.js");
commands["setseason"] = require(commands_dir + "/setseason.js");
commands["setdraftmode"] = require(commands_dir + "/setdraftmode.js");
commands["setelocap"] = require(commands_dir + "/setelocap.js");
commands["setlolapi"] = require(commands_dir + "/setlolapi.js");
commands["setgenerateinvite"] = require(commands_dir + "/setgenerateinvite.js");
commands["setrnd"] = require(commands_dir + "/setrnd.js");
commands["setrnddays"] = require(commands_dir + "/setrnddays.js");

// verify
commands["verify"] = require(commands_dir + "/verify.js");
commands["done"] = require(commands_dir + "/done.js");

// other
commands["yes"] = require(commands_dir + "/yes.js");
commands["no"] = require(commands_dir + "/no.js");
commands["spam"] = require(commands_dir + "/spam.js");
commands["spamreact"] = require(commands_dir + "/spamreact.js");
commands["post"] = require(commands_dir + "/post.js");
commands["bot_mention"] = require(commands_dir + "/bot_mention.js");

// commands for me only, change permissions to owner
commands["test"] = require(commands_dir + "/test.js");
commands["load"] = require(commands_dir + "/load.js");
commands["kill"] = require(commands_dir + "/kill.js");
commands["decay"] = require(commands_dir + "/decay.js");
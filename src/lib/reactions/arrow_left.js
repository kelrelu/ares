var fs = require("fs");
var db = require("../db.js");
var util = require("../util.js");
var config = util.config;
var cd = require("../commands.js");

var arrow_left = module.exports = new Map();

arrow_left.function = function(r,u){
	// generate the fields
	var categories = util.generate_category_pages(cd,"admin");

	// now scroll to next page to the left
	var m = r.message;
	if(m.embeds.length!=0){
		var embed = m.embeds[0];
		if(embed.author){
			if(embed.author.name.toLowerCase().includes("commands") && embed.footer) {
				var user_count = r.users.array().length;
				if(user_count > 1){
						var page_info = embed.footer.text.split("Page").filter(function(t){return t.length!=0})[0].split("of").map(function(t){return parseInt(t.trim());});

						if(page_info[0] > 1){
							
							m.edit({embed:{
								author: {
									name: "" + config.bot.username + " Commands by kel#3382",
									icon_url: m.author.avatarURL()
								},
								title: categories[page_info[0]-2][0],
								description: categories[page_info[0]-2][1],
								footer: {
									text: "Page " + String(page_info[0]-1) + " of " + String(categories.length),
									icon_url: u.avatarURL
								},
								timestamp : new Date()
							}}).then(function(){
								if(m.channel.type!="dm"){
									r.remove(u);
								}
							}).catch(console.error);
						} else {
							if(m.channel.type!="dm"){
								r.remove(u);
							}
						}
				}
			}
		}
	}
}

arrow_left.name = "arrow_left";
arrow_left.description = "pagination reaction - scrolls left";
arrow_left.perms = "all";
arrow_left.category = "pagination";
var fs = require("fs");
var util = module.exports = {}

// reads in config file
util.config = JSON.parse(fs.readFileSync(__dirname + "/../../config.json").toString());

util.mranks = new Map()
util.mranks["CHALLENGER I"] = ["C", 134]
util.mranks["MASTER I"] = ["M", 131]
util.mranks["DIAMOND I"] = ["D1", 128]
util.mranks["DIAMOND II"] = ["D2", 125]
util.mranks["DIAMOND III"] = ["D3", 122]
util.mranks["DIAMOND IV"] = ["D4", 115]
util.mranks["DIAMOND V"] = ["D5", 110]
util.mranks["PLATINUM I"] = ["P1", 105]
util.mranks["PLATINUM II"] = ["P2", 100]
util.mranks["PLATINUM III"] = ["P3", 95]
util.mranks["PLATINUM IV"] = ["P4", 90]
util.mranks["PLATINUM V"] = ["P5", 85]
util.mranks["GOLD I"] = ["G1", 80]
util.mranks["GOLD II"] = ["G2", 75]
util.mranks["GOLD III"] = ["G3", 70]
util.mranks["GOLD IV"] = ["G4", 65]
util.mranks["GOLD V"] = ["G5", 60]
util.mranks["SILVER I"] = ["S1", 55]
util.mranks["SILVER II"] = ["S2", 50]
util.mranks["SILVER III"] = ["S3", 45]
util.mranks["SILVER IV"] = ["S4", 40]
util.mranks["SILVER V"] = ["S5", 35]
util.mranks["BRONZE I"] = ["B1", 28]
util.mranks["BRONZE II"] = ["B2", 25]
util.mranks["BRONZE III"] = ["B3", 22]
util.mranks["BRONZE IV"] = ["B4", 19]
util.mranks["BRONZE V"] = ["B5", 16]
util.mranks["UNRANKED"] = ["UR", 40] // same as S4



util.regions = new Map();
util.regions["br"] = "br1";
util.regions["eune"] = "eun1";
util.regions["euw"] = "euw1";
util.regions["na"] = "na1";
util.regions["lan"] = "la1";
util.regions["las"] = "la2";
util.regions["oce"] = "oc1";
util.regions["kr"] = "kr";
util.regions["ru"] = "ru";
util.regions["pbe"] = "pbe1";
util.regions["jp"] = "jp1";
util.regions["tr"] = "tr1";


util.lol_ranks = ["Challenger", "Master", "Diamond", "Platinum", "Gold", "Silver", "Bronze"];


// util.to = function (promise) {  
//    return promise.then(data => {
//       return [null, data];
//    })
//    .catch(err => [err]);
// }


util.get_rank_info = function(tier, division){
    var tier = tier.toUpperCase();
    var division = division.toUpperCase();
    var mranks = util.mranks;

    if(tier === "UNRANKED"){
        return mranks["UNRANKED"];
    } else {
        return mranks[tier + " " + division];
    }
}

// removes white space from a string
util.rws = function(s){ return s.replace(/\s/g,''); }

// returns embed with a red x
util.redx_embed = function(s){
	return {embed: {
		color:0xff050c,
		title: "",
		description: ":x: " + s
	}};
}

// returns embed with a checkmark
util.cm_embed = function(s){
	return {embed: {
		color:3447003,
		title: "",
		description: ":white_check_mark:  " + s
	}};
}

util.q_embed = function(s){
    return {embed: {
        color:3447003,
        title: "",
        description: ":question:  " + s
    }};

}

util.api_error_embed = function(){
    return {embed:{
        color:0xff050c,
        title: "",
        description: ":scream: For some reason there was an error with the Riot API.\nI've pinged <@"  + this.config.bot.owner + "> about this!\n\nTry verifying again in a couple minutes."
    }};
}

util.handle_api_error = function(m,client,error){
    console.log("API error:" + error.statusCode);
    m.channel.send(this.api_error_embed());

    // notify bot owner of API error
    var owner = client.users.find("id",this.config.bot.owner);
    if(owner!=null){
        owner.send({embed:{
            title: "API Error",
            description: "Something went wrong with the Riot API!\n" + error.statusCode + " Error."
        }});
    }
}

// returns embed with formatting error message for a command
util.error_format = function(command){
    return this.redx_embed("**Incorrectly Formatted**\nNote: Put quotes around multi-word IGNs!\n\n **" + this.config.bot.prefix + command.name + " " + command.arg + "**");
}

// gets command from message
util.parse_command = function(s){
	return s.substring(1,s.length).split(/(\s+)/).map(function(t){ return t.trim(); }).filter(Boolean)[0];
}

// gets single argument from message, returns arg in array
util.parse_arg = function(s, command){
	// var no_quote_split =  s.substring(1,s.length).split(command).map(function(t){ return t.trim(); }).filter(Boolean);
    var a = s.substring(1 + command.length, s.length).trim();
    if(a){
        var quote_split = a.split("\"").map(function(t){return t. trim();}).filter(Boolean);
        return quote_split
    }
    return a;
}

//gets multiple arguments from message, returns args in array
util.parse_args = function(s,command){
	var a = s.substring(1 + command.length, s.length).trim();
    if(a){
        if(a.includes("\"")){
            return a.split("\"").map(function(t){ return t.trim(); }).filter(Boolean);
        } else {
            return a.split(" ").map(function(t){ return t.trim(); }).filter(Boolean);
        }
    }
    return [];
    
}

util.first_uppercase = function(s){
    if(s.length > 0){
        return s[0].toUpperCase() + s.substring(1,s.length).toLowerCase();
    }
    return s;
}

// converts json to csv
util.convert_to_csv = function(obj_array) {
    var array = typeof obj_array != 'object' ? JSON.parse(obj_array) : obj_array;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','
            line += array[i][index];
        }
        str += line + '\r\n';
    }
    return str;
}

// checks if a string is an integer
util.is_integer = function(s){
    if(isNaN(parseInt(s)) === false){
      return true;
    } 
    return false;
}

// checks if a string is a user mention
util.is_user_mention = function(s){
    if(s.startsWith("<@") && s.endsWith(">")){ return true;}
    return false;
}

// gets discord id from a user mention
util.get_did = function(user_mention){
    return user_mention.replaceAll("<","").replaceAll("@","").replaceAll("!","").replaceAll(">","").replaceAll("&","");
}

util.is_channel = function(s){
    if(s.startsWith("<#") && s.endsWith(">")){ return true;}
	return false;
}

util.get_cid = function(s){
    return s.replaceAll("<","").replaceAll("#","").replaceAll(">","");
}

util.is_custom_emoji = function(s){
    if(s.startsWith("<:") && s.endsWith(">")){
		return true
	}
	return false;
}

util.get_custom_emoji_id = function(s){
    return s.substring(1,s.length -1).split(":").filter(function(t){return t.trim().length!=0})[1];
}

util.get_emoji = function(s,ihs){
	if(this.is_custom_emoji(s)){
        var emoji = ihs.client.emojis.get(this.get_custom_emoji_id(s));
        if(emoji){
            return emoji;
        }
	}
	return s;
}


// generates a game id 
util.generate_gid = function*(){
    var i = 0;
    while(true){
        yield i++;
    }
}

// writes config changes to config.json
util.write_config_to_file = function(config){
    var config_s = JSON.stringify(config, null, "\t");
    fs.writeFileSync(__dirname + "/../../config.json", config_s,'utf8',function(){
        console.log("Wrote changes to config.json.");
    });
}

util.read_config = function(){
    this.config = JSON.parse(fs.readFileSync(__dirname + "/../../config.json").toString());
}

// finds index of string element in a string array, ignores case
util.io_ci = function(e, a){ // index of case insensitive
    var a_upper = a.map(function(t){ return t.toUpperCase(); });
    var e_upper = e.toUpperCase();
    if(a_upper.includes(e_upper)){
        return a_upper.indexOf(e_upper);
    } else { return -1 }
}

// gets role objects from guild given role ids
util.get_roles_by_id = function(guild,rids){
    var target_roles = [];
    for(r in rids){ 
        target_roles.push(guild.roles.find("id",rids[r])); 
    }
    return target_roles;
}

util.get_roles_by_name = function(guild,rnames){
    var target_roles = [];
    for(r in rnames){ 
        target_roles.push(guild.roles.find("name",rnames[r])); 
    }
    return target_roles;
}

// array element add - adds amount to every element in the array
util.ae_add = function(values, diffs){
    for(var i = 0; i < values.length; i++){
        values[i] = Math.round(values[i] + diffs[i]);
    }
    return values;
}

// weighted average with weights, values
util.w_avg = function(values,weights){
    var sum = 0;
    for(var i = 0; i < values.length;i++){
        sum += values[i] * weights[i];
    }
    return sum / values.length;
}


// adjusts elo gain/loss based on distance from default elo, can be changed in config.json
util.adjusted_diff = function(current_elo, diff){
    if( (((current_elo - this.config.ih.default_elo) < -1 * this.config.ih.elocap_start_range) && diff < 0) || (((current_elo - this.config.ih.default_elo) > this.config.ih.elocap_start_range) && diff > 0)){
        // console.log("doing cap");
        var diff_modifier = (1-(Math.abs(this.config.ih.default_elo - current_elo) / this.config.ih.elocap_max_range)) + this.config.ih.elocap_min_percent;
        var final_diff =  diff * diff_modifier;

        if(Math.abs(final_diff) <= this.config.ih.elocap_min_elo_diff){
            var new_diff = this.config.ih.elocap_min_elo_diff * ((diff < 0 ) ? -1 : 1);
            return new_diff;
        } else if(Math.abs(final_diff) >= this.config.elocap_max_elo_diff){
            var new_diff = this.config.ih.elocap_max_elo_diff * ((diff < 0 ) ? -1 : 1);
            return new_diff;
        } else {
            return final_diff;
        }
    }
    // check that min elo diff is at least what we set it to be
    var elodiff = Math.max(Math.abs(diff),this.config.ih.elocap_min_elo_diff);
    var elodiff = Math.min(Math.abs(diff),this.config.ih.elocap_max_elo_diff);
    return elodiff * ((diff < 0 ) ? -1 : 1);
}

// updates player object with update_key
util.update_info = function (players,new_info,update_key){
    for(var i = 0; i < players.length; i ++){
        players[i][update_key] = new_info[i];
    }
    return players;
}

// check if user had role listed in rids
util.has_role = function(user,rids){
    if(rids.length == 0){
        return true;
    } else {
        var counter = 0;
        for(var i = 0; i < rids.length; i++){
            var valid = user.roles.has(rids[i]);
            if(valid){ counter += 1}
        }
        return counter > 0;
    }
}

// generates string description for a command, format command
util.fc = function(cd, command){
    return ":small_blue_diamond: **" + cd[command].name + " " + cd[command].arg + "** [" + cd[command].perms.toUpperCase() + "] : " + cd[command].description + "\n";
}

// generates array of command descriptions
util.generate_command_descriptions = function(cd,perms){
    var command_categories = Array.from(new Set(Object.keys(cd).map(function(t){return cd[t].category;})));
    var fields = new Map();
    for(cc in command_categories){
        fields[command_categories[cc]] = [];
    }
    fields['all'] = [];

    var commands = Object.keys(cd);
    for(i in commands){
        var c = commands[i];
        var c_info = cd[c];

        fields[c_info.category].push(this.fc(cd,c));
        if(c_info.perms.toLowerCase() === "all" && c_info.category.toLowerCase() != "secret"){
            fields['all'].push(this.fc(cd,c));
        }
    }
    return fields;
}

// generates categories with commands
util.generate_category_pages = function(cd,perms){
    var command_descriptions = this.generate_command_descriptions(cd, perms);
    var categories = [];

    var cdk = Object.keys(command_descriptions).filter(function(t){ return t.toLowerCase()!= "secret"});
    for(c in cdk){
        if(cdk[c].toLowerCase()!= "all"){
            categories.push([ ":book: " + cdk[c].toUpperCase() + "", command_descriptions[cdk[c]].join("")]);
        }
    }

    return categories;
}


util.includes_upper = function(element,array){
    var array_upper = array.map(function(t){return t.toUpperCase(); });
    return array_upper.includes(element.toUpperCase());
}

// should be io_upper
util.indexof_upper = function(element,array){
    var array_upper = array.map(function(t){return t.toUpperCase(); });
    if(array_upper.includes(element.toUpperCase())){
        return array_upper.indexOf(element.toUpperCase());
    } else {
        return -1;
    }
}

util.add_roles = function(guild, member_id,role_ids,reason){

    var tba_roles = util.get_roles_by_id(guild,role_ids);
    var member = guild.member(member_id);

    console.log(tba_roles.length);

    if(member){
        for(var i = 0; i < tba_roles.length; i ++){
            if(member.roles.has(tba_roles[i].id) === false){
                if(reason){
                    member.addRole(tba_roles[i],[reason]).catch(console.log(error));
                } else {
                    member.addRole(tba_roles[i]).catch(console.log(error));
                }
            }
        } 
    }
}
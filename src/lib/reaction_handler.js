var util = require("./util.js");
var config = util.config;

class ReactionHandler {
	constructor(reaction_directory){
		this.rd = reaction_directory;
	}
	// only execute reaction commands for reactions on messages sent by the bot
	is_valid_message(message){
		return message.author.id === config.bot.id;
	}

	process_reaction(reaction,user){
		if(this.is_valid_message(reaction.message)){
			if(this.rd[reaction.emoji.name] != null){
				this.rd[reaction.emoji.name]["function"](reaction,user);
			}
		}
	}

}

module.exports = ReactionHandler;
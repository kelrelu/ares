var util = require("./util.js");
var config = util.config;

class GuildMemberAddHandler {
	// constructor(guild_member_add_directory){
	// 	this.gmad = guild_member_add_directory;
	// }

	constructor(){}

	process_guild_member_add(member){
		// add tag if their discord was created
		if(config.moderation.new_discord_account_ih_restrict_access === "on"){
			var user = member.user;
			var now_ms = new Date().getTime();
			if( (now_ms - user.createdTimestamp) < config.moderation.new_discord_account_min_days * 24 * 60 * 60 * 1000) {

				// console.log("CHANNELS_HERE");
				// console.log(member.guild);
				var bots_dev_channel = member.guild.channels.find("id", config.server.bots_dev_channel);

				// console.log(bots_dev_channel.id);
				if(bots_dev_channel){
					bots_dev_channel.send("``Discord Account Too New`` => ``Added Unverified Role to``" + member.user);
				}

				util.add_roles(member.guild, member.id, config.roles.ih_restricted, "Discord Account is less than " + config.moderation.new_discord_account_min_days + " days old.");

			}
		}
		
	}

}

module.exports = GuildMemberAddHandler;
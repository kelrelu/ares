var util = require("./util.js");
var config = util.config;
var colors = require("colors");


class CommandHandler {
	constructor(command_directory){
		this.cd = command_directory;
	}

	is_valid_sender(m,command){
		if(m.author.bot){return false;}
		// bot owner is able to execute all commands
		if(m.author.id === config.bot.owner){ return true; }
		else {
			//check that author has permissions before acting on a message
			var perm_group = this.cd[command]["perms"];
			var allowed_rids = config.roles[perm_group];
			if(allowed_rids){
				if(allowed_rids.length == 0){
					return true;
				} else {
					var counter = 0;
					for(var i = 0; i < allowed_rids.length; i++){
						var valid = m.member.roles.has(allowed_rids[i]);
						if(valid){ counter += 1}
					}
					return counter > 0;
				}
			}
			return false;
		}
	}

	process_message(m,ihs){
		if(m.content.length!=0){
			if(m.channel.type === "dm"){
				this.process_dm_message(m,ihs);
			} else {
				this.process_server_message(m,ihs);
			}
		}
	}

	process_server_message(m,ihs){
		// if role assign channel, limit types of messages
		if(m.channel.id === config.server.role_assign_channel){
			if(m.author.bot === false){
				if(m.content.toLowerCase().startsWith(config.bot.prefix + "verify")){

					var command = util.parse_command(m.content);
					if(this.cd[command] != null){
						// check that sender has permission to execute command
						if(this.is_valid_sender(m, command)){
							this.cd[command]["function"](m, ihs);
						}
					}

				} else if(m.content.toLowerCase().startsWith(".iam") || m.content.toLowerCase().startsWith(".iamn") || m.content.toLowerCase().startsWith(".lsar")){
					// do nothing
				} 
				else {
					m.delete().catch(console.error);
					m.author.send({embed:{
						color:0xff050c,
						author: {
							name:"AutoMod - Message Deleted",
							icon_url: ihs.client.guilds.find("id",config.server.server_id).iconURL()
						},
						description: "``Message``: " + m.content + "\n``Reason``: Only the commands ``.verify``,``.iam``,``.iamn``, and ``.lsar`` may be used in the role assign channel <#" + config.server.role_assign_channel +">."
					}});
				}
			}
		}
		else {
			// check that command starts with correct prefix
			if(m.content.startsWith(config.bot.prefix)){
				var command = util.parse_command(m.content);
				//check that command is in valid list of commands
				if(command.toLowerCase() === "helplfg"){
					// show all commands in message
					this.cd[command.toLowerCase()]["function"](m, ihs, this.cd);
				}
				else if(this.cd[command] != null){
					// check that sender has permission to execute command
					if(this.is_valid_sender(m, command)){
						console.log( ("Message:" + m.content + "\nAuthor:" + m.author.tag + "\nChannel:" + m.channel.id + "\nTimestamp:" + new Date() + "\n").magenta );
						this.cd[command]["function"](m, ihs);
					}
				}
			// reply to yes response if appropriate
			} else if(m.content.toLowerCase() === "y" || m.content.toLowerCase() === "yes"){
				this.cd["yes"]["function"](m,ihs);
			// reply to no response if appropriate
			} else if(m.content.toLowerCase() === "n" || m.content.toLowerCase() === "no"){
				this.cd["no"]["function"](m,ihs);
			// special response if @ Ares with no command
			} else if(m.mentions.users.find("id",ihs.client.user.id)){
				this.cd["bot_mention"]["function"](m)
			}
		}
	}

	process_dm_message(m,ihs){
		if(m.content.startsWith(config.bot.prefix)){
			var command = util.parse_command(m.content);
			if(this.cd[command] != null){
				if(this.cd[command].dm == true){
					this.cd[command]["function"](m,ihs);
				} else { m.channel.send(util.redx_embed("The command **" + config.bot.prefix + command + "** can only be called in the server. Try using it there!")).then(function(wm){
					wm.delete( {"timeout": config.bot.delete_wait_time_seconds * 1000} );
				})}
			}
		}
		
		// also send messages to bot owner
		// var owner = ihs.client.users.find("id",config.bot.owner);
		// if(owner.id != m.author.id && config.bot.id != m.author.id){
		// 	owner.send({embed:{
		// 		author: {
		// 			name: m.author.tag,
		// 			icon_url : m.author.avatarURL()
		// 		},
		// 		description: m.content,
		// 		timestamp: new Date(),
		// 	}});
		// }
		
	}
}

module.exports = CommandHandler;
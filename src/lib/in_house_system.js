var ontime = require("ontime");

var util = require("./util.js");
var config = util.config;

var db = require("./db.js");

var Game = require("./game.js");



function convert_date_to_UTC(date) { 
	return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()); 
}

class InHouseSystem {
	constructor(client){
		this.client = client;

		// lobby variables
		this.lobbies = new Map();
		for( var i = 0; i<config.ih.lobby_names.length; i++ ){
			this.lobbies[config.ih.lobby_names[i]] = null;
		}

		this.cl = null; // current lobby

		// management variables
		this.tbd_mids = new Map(); // stores mids for to-be-deleted messages
		this.ur = new Map();// user responses, stores information for commands requiring confirmation

		// stores past ih game (if you need to undo the win)
		this.last_game = null;

		// any discord id in this list will be ignored by the bot permanently
		this.ignore_list = [];
		for( var i = 0; i < config.ih.ignore_list; i ++){
			this.ignore_list.push(config.ih.ignore_list[i])
		}
		// any discord id in this list will be ignored by the bot for X number of games
		this.ignore_temp_list = new Map();

		// any discord id in this list will have a shameful message pop up when they try to host
		this.ihm_shame_list = []; // shamed mods
		for( var i = 0; i < config.ih.ihm_shame_list; i ++){
			this.ihm_shame_list.push(config.ih.ihm_shame_list[i]);
		}

		// any discord id in this list will have in-houses blocked for them, will add blocked tag upon member join
		this.ih_banned_list = [];
		for(var i =0;i < config.ih.ih_banned_list;i++){
			this.ih_banned_list.push(config.ih.ih_banned_list[i]);
		}

		// stores people who are verifying ranks, lol regions
		this.lol_verification = new Map();

		// counts number of created voice channels
		this.vc = new Map();
		for( var i = 0; i<config.ih.lobby_names.length; i++ ){
			this.vc[config.ih.lobby_names[i]] = false;
		}
		
	}

	verify_da(){
		ontime({ cycle: '17:00:00',utc: true,}, function(){
			var guild = this.client.guilds.find("id",config.server.server_id);
			if(guild){
				// fetch all members with unverified role
				// if their account creation date is > limited number of days, remove role
				var unverified_roles = util.get_roles_by_id(guild,config.roles.ih_restricted);
				if(itp_roles){
					for(var tag=0; tag < unverified_roles.length;tag++){
						var mems = unverified_roles_roles[tag].members.array();
						for(var m=0;m < mems.length;m++){

							var user = mems[m].user;
							var now_ms = new Date().getTime();
							if( (now_ms - user.createdTimestamp) >= config.moderation.new_discord_account_min_days * 24 * 60 * 60 * 1000) {
								var bots_dev_channel = member.guild.channels.find("id", config.server.bots_dev_channel);
								
								if(bots_dev_channel){
									bots_dev_channel.send("``Discord Account Approved`` => ``Removed Unverified Role from``" + member.user);
								}

								mems[m].removeRole(unverified_roles[tag]);
							}
						}
					}
				}
			}
		});
	}

	load_lobby(l){
		var al = new Game(l.host,l.name);

		// read in from database of active lobbies
		al.id = l.game_id;

		var guild = this.client.guilds.find("id",config.server.server_id);
		if(guild){
			var host = guild.members.find("id",l.host);
			if(host){
				al.host = host.user;
			} else {
				al.host = guild.members.find("id",config.bot.owner).user;
			}
			al.timestamp = new Date(String(l.timestamp));

			// set players
			var pt_list = [l.t1p1,l.t1p2,l.t1p3,l.t1p4,l.t1p5,l.t2p1,l.t2p2,l.t2p3,l.t2p4,l.t2p5];
			var gp = new Map();
			var capt1 = "";
			var capt2 = "";

			for(var i = 0; i < pt_list.length; i++){
				var pinfo = db.dbquery("select * from players where ign='" + db.dbify(pt_list[i]) + "';");
				gp[pinfo[0].discord_id] = pinfo[0].ign;

				if(i == 0){ capt1 = pinfo[0].ign + " [" + String(pinfo[0].elo) + "]"; }
				if(i == 5) { capt2 = pinfo[0].ign + " [" + String(pinfo[0].elo) + "]"; }
			}
				
			al.ih_open = false;
			al.made_teams = true;
			al.checked_names = true;
			al.bad_player = false;

			al.players = gp;
			al.player_count = config.ih.max_team_size * 2;

			al.t1 = pt_list.splice(0,5);
			al.t2 = pt_list;

			al.c1 = capt1;
			al.c2 = capt2;

			// what to do about voice lobbies
			// al.t1_vc
			// al.t2_vc

			al.set_status();

			this.lobbies[l.name] = al;
		} else {
			// start afresh
			db.dbquery("delete * from lobbies;")
		}
	}

	load_lobbies(){
		// reads in lobbies from database on bot start up
		var active_lobbies = db.dbquery("select * from lobbies;");
		for (var i=0;i < active_lobbies.length; i++){
			this.load_lobby(active_lobbies[i]);
		}
	}

	cl_to_al(){
		var lobby_names = Object.keys(this.lobbies);
		for(var ag = 0; ag < lobby_names.length; ag++){ 
			if(this.lobbies[lobby_names[ag]] != null){ 
				this.cl = lobby_names[ag];
				break;
			} 
		}
	}

	flush_verification(){
		var unverified_uids = Object.keys(this.lol_verification).map(function(t){return t});
		
		var guild = this.client.guilds.find("id",config.server.server_id);
		for(var i = 0; i < unverified_uids.length;i++){
			var member = guild.members.find("id,",unverified_uids[i]);
			if(member){
				member.user.send({embed:{
					author: {
						icon_url: guild.iconURL(),
						name: "Verification Expired"
					},
					description: "Hi! Your verification has expired (flushed on a daily basis).\nIf you still want to receive a ranked tag, you must renew your verification by typing **" + config.bot.prefix + "verify <ign> <region>** again to receive a new summoner icon." 
				}});
			}
		}

		this.lol_verification = null;
		this.lol_verification = new Map();
	}

	decay_check(ihs){
		ontime({ cycle: '17:00:00',utc: true,}, function(){
			// ihs.flush_verification();
			var ih_channel = ihs.client.channels.find("id",config.server.ih_channel);
			if(ih_channel!=null){
				var top_players = db.dbquery("select * from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + "order by p.elo desc,p.mmr desc, p.wins desc,p.losses asc,p.ign asc limit " + String(config.ih.max_leaderboard) + ";");
				var decaying_players = new Map();

				// decay buffer amounts
				var decay_lb_buffer_ms = config.ih.decay_lb_buffer_days * 24 * 60 * 60 * 1000;
				var decay_halflb_buffer_ms = config.ih.decay_halflb_buffer_days * 24 * 60 * 60 * 1000;
				var decay_itp_buffer_ms = config.ih.decay_itp_buffer_days * 24 * 60 * 60 * 1000;

				var decay_lb_warning_ms_lower = (config.ih.decay_lb_buffer_days - config.ih.decay_warning_before_days) * 24 * 60 * 60 * 1000;
				var decay_lb_warning_ms_upper = (config.ih.decay_lb_buffer_days - config.ih.decay_warning_before_days + 1) * 24 * 60 * 60 * 1000;

				var decay_halflb_warning_ms_lower = (config.ih.decay_halflb_buffer_days - config.ih.decay_warning_before_days) * 24 * 60 * 60 * 1000;
				var decay_halflb_warning_ms_upper = (config.ih.decay_halflb_buffer_days - config.ih.decay_warning_before_days + 1) * 24 * 60 * 60 * 1000;

				var decay_itp_warning_ms_lower = (config.ih.decay_itp_buffer_days - config.ih.decay_warning_before_days) * 24 * 60 * 60 * 1000;
				var decay_itp_warning_ms_upper = (config.ih.decay_itp_buffer_days - config.ih.decay_warning_before_days + 1) * 24 * 60 * 60 * 1000;

				var now_ms = convert_date_to_UTC(new Date()).getTime();

				for(var p=0; p <top_players.length;p++){
					var player = top_players[p];	
					var last_played_ms = new Date(String(player.last_played)).getTime();
					if(p < config.ih.max_top_players){
						if( ((now_ms - last_played_ms) > decay_itp_warning_ms_lower) && ((now_ms - last_played_ms) < decay_itp_warning_ms_upper) ) {
							ihs.send_decay_warning(player.discord_id,config.ih.max_top_players);
						}
						// do decay for people who decay
						if((now_ms - last_played_ms) > decay_itp_buffer_ms){
							// console.log("logged decaying for " + player.ign);
							if(player.elo > config.ih.default_elo){
								var decayed_elo = player.elo - (config.ih.decay_amount) - (0.05 * player.elo);
								if(decayed_elo < config.ih.default_elo){ decayed_elo = config.ih.default_elo; }
								decaying_players[player.ign] = "[" + player.elo + " -> " + Math.round(decayed_elo) + "] *Top 3 decay*";
								db.dbquery("update players set elo=" + String(Math.round(decayed_elo)) + " where discord_id='" + player.discord_id + "';");
							}
						}
					}
					else if(p < config.ih.max_leaderboard / 2){
						if( ((now_ms - last_played_ms) > decay_halflb_warning_ms_lower) && ((now_ms - last_played_ms) < decay_halflb_warning_ms_upper) ) {
							ihs.send_decay_warning(player.discord_id, config.ih.max_leaderboard / 2);
						}
						// do decay for people who decay
						if((now_ms - last_played_ms) > decay_halflb_buffer_ms){
							// console.log("logged decaying for " + player.ign);
							if(player.elo > config.ih.default_elo){
								var decayed_elo = player.elo - (config.ih.decay_amount) - (0.05 * player.elo);
								if(decayed_elo < config.ih.default_elo){ decayed_elo = config.ih.default_elo; }
								decaying_players[player.ign] = "[" + player.elo + " -> " + Math.round(decayed_elo) + "] *Top 20 decay*";
								db.dbquery("update players set elo=" + String(Math.round(decayed_elo)) + " where discord_id='" + player.discord_id + "';");
							}
						}
					}
					else {
						// message if about to decay
						if( ((now_ms - last_played_ms) > decay_lb_warning_ms_lower) && ((now_ms - last_played_ms) < decay_lb_warning_ms_upper) ) {
							ihs.send_decay_warning(player.discord_id,config.ih.max_leaderboard);
						}

						if((now_ms - last_played_ms) > decay_lb_buffer_ms){
							console.log("logged decaying for " + player.ign);
							var decayed_elo = player.elo - (config.ih.decay_amount) - (0.05 * player.elo);
							if(player.elo > config.ih.default_elo){
								if(decayed_elo < config.ih.default_elo){ decayed_elo = config.ih.default_elo; }
								decaying_players[player.ign] = "[" + player.elo + " -> " + Math.round(decayed_elo) + "]";
								db.dbquery("update players set elo=" + String(Math.round(decayed_elo)) + " where discord_id='" + player.discord_id + "';");
							}
						}
					}
				}

				// var updated_top_players = db.dbquery("select * from players p where (p.wins + p.losses) >=" + String(config.ih.num_placement_matches) + "order by p.elo desc,p.mmr desc, p.wins desc,p.losses asc,p.ign asc limit " + String(config.ih.max_leaderboard) + ";");
				// for(var p=0; p < updated_top_players.length;p++){
				// 	if(p < config.ih.max_top_players){
				// 		if( ((now_ms - last_played_ms) > decay_itp_warning_ms_lower)) {
				// 			ihs.send_decay_warning(player.discord_id,config.ih.max_top_players);
				// 		}
				// 	} else if(p < config.ih.max_leaderboard / 2){
				// 		if( ((now_ms - last_played_ms) > decay_halflb_warning_ms_lower)) {
				// 			ihs.send_decay_warning(player.discord_id, config.ih.max_leaderboard / 2);
				// 		}
				// 	} else {
				// 		// message if about to decay
				// 		if( ((now_ms - last_played_ms) > decay_lb_warning_ms_lower)) {
				// 			ihs.send_decay_warning(player.discord_id,config.ih.max_leaderboard);
				// 		}

				// 	}
				// }


				var decaying_players_string = Object.keys(decaying_players).map(function(t){ return t + " " + decaying_players[t]; }).join("\n");
				ih_channel.send({embed:{
					title: "Decayed Players",
					description: decaying_players_string.length === 0 ? "*None*" : decaying_players_string,
					timestamp : new Date()
				}});
				decaying_players = [];
				db.update_itp_tags(ihs);
			}
		});
	}

	send_decay_warning(discord_id, rank){
		var user = this.client.users.find("id", discord_id);
		if(user != null){
			// console.log("bye");
			user.send({embed: {
				color:3447003,
				author:{
					name:config.server.server_name + " In-House Decay Warning: " + user.tag,
					icon_url:this.client.guilds.find("id",config.server.server_id).iconURL()
				},
				description: "\u200b\nYou are currently one of the **__Top " + String(rank) + " players__** in **" + config.server.server_name + "** , but will start **__decaying in " + config.ih.decay_warning_before_days + " day(s)__** if you do not play. :frowning:\n\nAs a reminder, decay applies after __" + config.ih.decay_itp_buffer_days + " days of inactivity__ for Ranks 1-3, __" + config.ih.decay_halflb_buffer_days + " days of inactivity__ for Ranks 3-20, and after __" + config.ih.decay_lb_buffer_days + " days of inactivity__ for Ranks 21-40. You will lose 20 elo per day + 5% of your current elo.\n\n**We hope to see you back to protect your spot on the ladder! :smile: Best of luck!!**",
				// footer: {
				// 	icon_url: user.avatarURL,
				// 	text: user.username
				// },
			}}).then().catch(console.error);
		}
	}

	set_tbd_mid(name, mid){
		this.tbd_mids[name] = mid;
	}

	exists_open_lobby(){
		var lobby_names = Object.keys(this.lobbies)
		for(var ag = 0; ag < lobby_names.length; ag++){ 
			if(this.lobbies[lobby_names[ag]] != null){ 
				if(this.lobbies[lobby_names[ag]].ih_open === true){ 
					return true; 
				} 
			} 
		}
		return false;
	}

	get_open_lobby(){
		var lobby_names = Object.keys(this.lobbies)
		for(var ag = 0; ag < lobby_names.length; ag++){ 
			if(!(this.lobbies[lobby_names[ag]] == null)){ 
				if(this.lobbies[lobby_names[ag]].ih_open === true){ 
					return this.lobbies[lobby_names[ag]]; 
				} 
			} 
		}
		return null	
	}

	get_lobby(l){
		return this.lobbies[l];
	}

	undo_win(){
		var old_winner = db.dbquery("select * from games where game_id=" + this.last_game.game.id + ";")[0].winner;
		
		var winning_team = "";
		if(old_winner == 1){
			winning_team = "team1"
		} else {
			winning_team = "team2"
		}
	
		db.dbquery("delete from games where game_id=" + this.last_game.game.id + ";");
		for (var i = 0;i < this.last_game.t1_info.length; i++){
			var t1p = this.last_game.t1_info[i];
			var t2p = this.last_game.t2_info[i];
			db.dbquery("update players set elo=" + t1p.elo+",mmr=" + t1p.mmr + ",wins=" + t1p.wins + ",losses=" + t1p.losses + " where ign='" + db.dbify(t1p.ign) + "';");
			db.dbquery("update players set elo=" + t2p.elo+",mmr=" + t2p.mmr + ",wins=" + t2p.wins + ",losses=" + t2p.losses + " where ign='" + db.dbify(t2p.ign) + "';");
		}
	
		//fetch free lobby for undoing win
		//create new game and set to first free lobby
		var lobbies = this.lobbies;
		var free_lobbies = Object.keys(lobbies).filter(function(t){return lobbies[t] == null}).sort();
		if(free_lobbies.length != 0){
	
			this.lobbies[this.last_game.game.lobby] = new Game(this.last_game.game.host, free_lobbies[0]);
			this.lobbies[this.last_game.game.lobby].copy(this.last_game.game);
			this.cl = this.last_game.game.lobby;
	
			this.last_game.game.undo_award_currency(winning_team,this);
			this.last_game = null;

		} else {
			this.lobbies[this.last_game.game.lobby] = new Game(this.last_game.game.host, this.last_game.game.host);
			this.lobbies[this.last_game.game.lobby].copy(this.last_game.game);
			this.cl = this.last_game.game.lobby;
	
			this.last_game.game.undo_award_currency(winning_team,this);
			this.last_game = null;
		}
	}
}

module.exports = InHouseSystem;
var request = require("request");
var rp = require("request-promise");
var url = require("url");

var util = require("../lib/util.js");
var config = util.config;
var colors = require("colors");

class StatusCodeError {
	constructor(status_code, message){
		this.name = "StatusCodeError";
		this.statusCode = status_code || 0;
		this.message = message || "";
	}
}

class LolApi {
	constructor(api_key){
		this.api_key = api_key;
	}

	api_request(url){
		return new Promise(function(resolve,reject){
			request.get(url,function(error,response,body){
				if(error){
					reject(error)
				} else {
					if(response.statusCode == 200){
						var data = JSON.parse(body);
						resolve(data)
					} else {
						var sce = new StatusCodeError(response.statusCode, response.statusCode + " error.");
						reject(error)
					}
				}
	 		});
		});
	}

	generate_url(options){
		var url = "https://" + options.region + ".api.riotgames.com" + options.path + options.query + "?api_key=" + config.api.lol;
		return url;
	}

	async get_summoner_by_name(region,name){
		var url = this.generate_url({
			"region" : region,
			"path" : "/lol/summoner/v3/summoners/by-name/",
			"query" : encodeURIComponent(name)
		});

		return new Promise( (resolve,reject) => {
			this.api_request(url)
			.then( data => { resolve(data); })
			.catch( error => { reject(error); });
		});

	}

	async get_rank_by_summoner_id(region,id){
		var url = this.generate_url({
			"region" : region,
			"path" : "/lol/league/v3/positions/by-summoner/",
			"query" : id
		});

		return new Promise( (resolve,reject) => {
			this.api_request(url)
			.then( data => { resolve(data); })
			.catch( error => { reject(error); });
		});
	}

}

module.exports = LolApi;
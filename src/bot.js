// check that necessary external modules are installed
try {
	var discord = require("discord.js");
	var fs = require("fs");
	var ontime = require("ontime");
	var pgn = require("pg-native");
	var rt = require("rand-token");
	var request = require("request");
	var sqlite = require("sqlite3");
	var fetch = require("fetch").fetchUrl;
} catch(e) {
	console.log(e);
	console.log("Please run \"npm install\" before running the bot.");
	process.exit(1);
}

// import needed custom function modules
var util = require("./lib/util.js")
var config = util.config;
var db = require("./lib/db.js");

// import bot command directory, bot reaction directory
var cd = require("./lib/commands.js");
var rd = require("./lib/reactions.js");

// import needed custom class modules
var InHouseSystem = require("./lib/in_house_system.js");
var CommandHandler = require("./lib/command_handler.js");
var ReactionHandler = require("./lib/reaction_handler.js");
var GuildMemberAddHandler = require("./lib/guild_member_add_handler.js");

// create discord client
const client = new discord.Client({autoReconnect: true});

// initialize database if it doesn't exist
db.init_all();

// create handlers for reactions and messages, inhouse system
var ch = new CommandHandler(cd);
var rh = new ReactionHandler(rd);
var ihs = new InHouseSystem(client);
var gmah = new GuildMemberAddHandler();

// inhouse system functions - loads in active lobbies if they exist, checks for decaying players


// client functions
client.on("ready", () => { 
	console.log( new Date() + " - " + config.bot.username +"❤️  is now online."); 
	client.user.setActivity(config.bot.prefix + "helplfg 🐐");
});

client.on("message", message => {
	ch.process_message(message,ihs);
});

client.on("messageReactionAdd", (reaction,user) => {
	rh.process_reaction(reaction,user);
});

client.on("guildMemberAdd", member => {
	gmah.process_guild_member_add(member);
});

client.login(config.bot.token).then(function(){
	ihs.load_lobbies();
	ihs.cl_to_al();
	ihs.decay_check(ihs);
});

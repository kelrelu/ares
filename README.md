# ⚔ Ares 🤖  ⚔
Source code for Ares, a custom In-House Discord bot.

Ares organizes server in-house League of Legends games and tracks player statistics (elo/mmr/win/loss/kda) using a modified elo system.

Documentation can be found [here](https://google.com).

# Software Prerequisites
The following software must be installed **prior** to running Ares.

1. ``pm2`` 
2. ``node.js``
3. ``npm``

# Configuring Ares
## Create a Discord Bot Application 
Login to https://discordapp.com/developers/applications/me and create a custom bot application. 

1. Click **New App**
2. Click **Create App** 
3. Within your application page, find and click **Create Bot User**. *Important: After generating a bot user for your bot, you should be able to access **Client ID** and **Client Token** information for your bot application.*
5. To add your new bot to your server, visit 
https://discordapp.com/oauth2/authorize?client_id=CLIENTID&scope=bot&permissions=0

Make sure to replace ``CLIENTID`` with your bot's client id.

## Edit Configuration Settings for Ares
Edit ``config.json`` before running the bot. Most settings are self-explanatory.

Critical edits include:

1. Setting ``token`` to your bot's token (Can be found on the bot's application page).
2. Setting ``owner`` to your discord id (Can be found by typing ``\@your discord user`` in Discord).
3. Setting ``server_id`` to your server's server id (Can be found by turning on Developer Settings in Discord and right clicking the server).


# Running Ares

Run ``./run.sh`` in Terminal/Console. 

Note that you must have a persistent Internet connection - ideally, Ares should be hosted on a hosting server. 👍 
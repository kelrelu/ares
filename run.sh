#!/bin/bash
cd src
npm install

# stop if already running
pm2 stop king_bot
pm2 start bot.js --name king_bot
